import re
import time
import json
import requests
import ds_database
import mysql.connector
from bs4 import BeautifulSoup

start_time = time.time()
cloud_provider = '2'
storage_type = 'Object'
volume_type = 'NA'
lease_term = 'On Demand'

disk_space = ''
disk_space_uom = ''
min_iops = ''
max_iops = ''
min_throughput = ''
max_throughput = ''
iops_price = 0
throughput_price = 0

storage_name_list = ['Premium High Transaction Rate Blob Storage', 'Hot Standard (GPv2) Storage', 'Cold Standard (GPv2) Storage', 'Archive Standard (GPv2) Storage']
min_tier_list = [0, 51, 501]
max_tier_list = [50, 500, 9999]

# Initialize database
db = ds_database.Database(cloud_provider, "", storage_type)

url = 'https://azure.microsoft.com/en-us/pricing/details/storage/blobs/'
storage_pricing_list = []

try:
	response = requests.get(url)
except:
	print('Could not fetch data for ' + url)

soup = BeautifulSoup(response.text, 'html.parser')
root = soup.find_all('div', {'class': 'row row-size8 column'})

# root[0] - Pay as you go
adhoc_divs = root[0].find_all('div', {'class': 'storage-table'})
for div in adhoc_divs:
	redundancy = div['class'][1]
	file_structure = div['class'][2]

	# next_min_tier = 0
	disk_tier_index = 0
	table = div.find('table')
	rows = table.find('tbody').find_all('tr')
	for row in rows:

		cells = row.find_all('td')

		min_tier = min_tier_list[disk_tier_index]
		min_tier_uom = 'TB'

		max_tier = max_tier_list[disk_tier_index]
		max_tier_uom = 'TB'
		disk_tier_index = disk_tier_index + 1

		for i in range(1, 5):

			storage_name = storage_name_list[i-1]

			try:
				spans = cells[i].find_all('span', {'class': 'price-data'})
				ondemand = json.loads(spans[0]['data-amount'])['regional']

				for key in ondemand:
					region = key
					storage_price = ondemand[key]

					price_tuple = (cloud_provider, region, storage_type, volume_type, storage_name, redundancy, file_structure, lease_term, disk_space, disk_space_uom, min_iops, max_iops, min_throughput, max_throughput, min_tier, min_tier_uom, max_tier, max_tier_uom, storage_price, iops_price, throughput_price)
					# if region == 'us-central':
					# 	print(price_tuple)
					storage_pricing_list.append(price_tuple)

			except:
				print('Data not available or could not be parsed for ' + file_structure + ' - ' + redundancy)
				continue # move on to next row

# root[1] - Reserved price
reserved_divs = root[1].find_all('div', {'class': 'storage-table'})
for div in reserved_divs:
	redundancy = div['class'][1]
	file_structure = div['class'][2]

	row_counter = 0
	storage_name_list_index = 0
	table = div.find('table')
	rows = table.find('tbody').find_all('tr')
	for row in rows:

		row_counter = row_counter + 1
		if row_counter == 1:
			disk_space = '100'
			disk_space_uom = 'TB'

		else:
			disk_space = '1'
			disk_space_uom = 'PB'

		cells = row.find_all('td')

		for i in range(1, 7):

			if i < 4:
				lease_term = '1yr'
			else:
				lease_term = '3yr'

			storage_name_list_index = storage_name_list_index + 1
			if storage_name_list_index == 4:
				storage_name_list_index = 1
			storage_name = storage_name_list[storage_name_list_index]

			try:
				spans = cells[i].find_all('span', {'class': 'price-data'})
				reserved = json.loads(spans[0]['data-amount'])['regional']

				for key in reserved:
					region = key
					storage_price = reserved[key]

					price_tuple = (cloud_provider, region, storage_type, volume_type, storage_name, redundancy, file_structure, lease_term, disk_space, disk_space_uom, min_iops, max_iops, min_throughput, max_throughput, min_tier, min_tier_uom, max_tier, max_tier_uom, storage_price, iops_price, throughput_price)
					# if region == 'us-central':
					# 	print(price_tuple)
					storage_pricing_list.append(price_tuple)

			except:
				print('Data not available or could not be parsed for ' + file_structure + ' - ' + redundancy)
				continue # move on to next row

# For debugging
# file.write(str(storage_attributes_list))
# file.close()

try:
	db.cursor.executemany(db.storage_pricing_ins_sql, storage_pricing_list)
	db.connect.commit()
	print('Data inserted successfully.')
except (mysql.connector.Error, mysql.connector.Warning) as e:
	print(e)

db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
