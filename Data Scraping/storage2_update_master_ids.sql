UPDATE storage_pricing_stg a
SET a.region_mst_id = (
		SELECT b.region_mst_id
		FROM region_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.region = a.region
		);

UPDATE storage_pricing_stg a
SET a.storage_type_mst_id = (
		SELECT b.storage_type_mst_id
		FROM storage_type_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.storage_type = a.storage_type
		);

UPDATE storage_pricing_stg a
SET a.volume_type_mst_id = (
		SELECT b.volume_type_mst_id
		FROM volume_type_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.volume_type = a.volume_type
		);

UPDATE storage_pricing_stg a
SET a.storage_mst_id = (
		SELECT b.storage_mst_id
		FROM storage_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.storage_name = a.storage_name
		);

UPDATE storage_pricing_stg a
SET a.redundancy_mst_id = (
		SELECT b.redundancy_mst_id
		FROM redundancy_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.redundancy = a.redundancy
		);

UPDATE storage_pricing_stg a
SET a.file_structure_mst_id = (
		SELECT b.file_structure_mst_id
		FROM file_structure_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.file_structure = a.file_structure
		);

UPDATE storage_pricing_stg a
SET a.lease_term_mst_id = (
		SELECT b.lease_term_mst_id
		FROM lease_term_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.lease_term = a.lease_term
		);
