import time
import mysql.connector
import ds_database

def ExecuteSQLFromFile(file_name):
	fd = open(file_name, 'r')
	sql_file = fd.read()
	fd.close()

	sql_commands = sql_file.split(';')
	for command in sql_commands:
		try:
			db.cursor.execute(command)
		except (mysql.connector.Error, mysql.connector.Warning) as e:
			print(e)

def ExecuteStep(step_description, step_sql_file_name):
	step_start_time = time.time()
	ExecuteSQLFromFile(step_sql_file_name)
	elapsed_seconds = round((time.time() - step_start_time))
	print(step_description + '. Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
	print('====================================================================')

start_time = time.time()

# Initialize database
db = ds_database.Database("", "", "")


######### COMPUTE ##########
# Step 1 - Insert master tables
ExecuteStep('Compute Step 1 - Insert master tables', 'compute1_insert_master.sql')
db.connect.commit()

# Step 2 - Update master ids in staging tables
ExecuteStep('Compute Step 2 - Update master ids', 'compute2_update_master_ids.sql')
db.connect.commit()

# Step 3 - Create temp tables
ExecuteStep('Compute Step 3 - Create temp tables', 'compute3_create_tmp_tables.sql')

# Step 4 - Insert temp tables
ExecuteStep('Compute Step 4 - Insert temp tables', 'compute4_insert_tmp_tables.sql')
db.connect.commit()

# Step 5 - Rename temp tables
ExecuteStep('Compute Step 5 - Rename temp tables', 'compute5_rename_tmp_tables.sql')
db.connect.commit()


########## STORAGE ##########
# Step 1 - Insert master tables
ExecuteStep('Storage Step 1 - Insert master tables', 'storage1_insert_master.sql')
db.connect.commit()

# Step 2 - Update master ids in staging tables
ExecuteStep('Storage Step 2 - Update master ids', 'storage2_update_master_ids.sql')
db.connect.commit()

# Step 3 - Create temp tables
ExecuteStep('Storage Step 3 - Create temp tables', 'storage3_create_tmp_tables.sql')

# Step 4 - Insert temp tables
ExecuteStep('Storage Step 4 - Insert temp tables', 'storage4_insert_tmp_tables.sql')
db.connect.commit()

# Step 5 - Rename temp tables
ExecuteStep('Storage Step 5 - Rename temp tables', 'storage5_rename_tmp_tables.sql')
db.connect.commit()


# End program
db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Total Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')