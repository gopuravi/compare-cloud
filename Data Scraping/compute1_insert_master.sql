UPDATE server_attributes_stg
SET operating_system = 'NA'
WHERE operating_system = '';

UPDATE server_attributes_stg
SET region = 'NA'
WHERE region = '';

UPDATE server_attributes_stg
SET instance_name = 'NA'
WHERE instance_name = '';

UPDATE server_attributes_stg
SET instance_type = 'NA'
WHERE instance_type = '';

UPDATE server_attributes_stg
SET instance_family = 'NA'
WHERE instance_family = '';

UPDATE server_attributes_stg
SET processor = 'NA'
WHERE processor = '';

UPDATE server_pricing_stg
SET lease_term = 'NA'
WHERE lease_term = '';

UPDATE server_pricing_stg
SET offering_class = 'NA'
WHERE offering_class = '';

UPDATE server_pricing_stg
SET payment_option = 'NA'
WHERE payment_option = '';

INSERT INTO operating_system_mst (
	cloud_provider_id
	,operating_system
	)
SELECT DISTINCT cloud_provider_id
	,operating_system
FROM server_attributes_stg
WHERE (
		cloud_provider_id
		,operating_system
		) NOT IN (
		SELECT cloud_provider_id
			,operating_system
		FROM operating_system_mst
		);

INSERT INTO region_mst (
	cloud_provider_id
	,region
	)
SELECT DISTINCT cloud_provider_id
	,region
FROM server_attributes_stg
WHERE (
		cloud_provider_id
		,region
		) NOT IN (
		SELECT cloud_provider_id
			,region
		FROM region_mst
		);

INSERT INTO instance_mst (
	cloud_provider_id
	,instance_name
	)
SELECT DISTINCT cloud_provider_id
	,instance_name
FROM server_attributes_stg
WHERE (
		cloud_provider_id
		,instance_name
		) NOT IN (
		SELECT cloud_provider_id
			,instance_name
		FROM instance_mst
		);

INSERT INTO instance_type_mst (
	cloud_provider_id
	,instance_type
	)
SELECT DISTINCT cloud_provider_id
	,instance_type
FROM server_attributes_stg
WHERE (
		cloud_provider_id
		,instance_type
		) NOT IN (
		SELECT cloud_provider_id
			,instance_type
		FROM instance_type_mst
		);

INSERT INTO instance_family_mst (
	cloud_provider_id
	,instance_family
	)
SELECT DISTINCT cloud_provider_id
	,instance_family
FROM server_attributes_stg
WHERE (
		cloud_provider_id
		,instance_family
		) NOT IN (
		SELECT cloud_provider_id
			,instance_family
		FROM instance_family_mst
		);

INSERT INTO processor_mst (
	cloud_provider_id
	,processor
	)
SELECT DISTINCT cloud_provider_id
	,processor
FROM server_attributes_stg
WHERE (
		cloud_provider_id
		,processor
		) NOT IN (
		SELECT cloud_provider_id
			,processor
		FROM processor_mst
		);

INSERT INTO lease_term_mst (
	cloud_provider_id
	,lease_term
	)
SELECT DISTINCT cloud_provider_id
	,lease_term
FROM server_pricing_stg
WHERE (
		cloud_provider_id
		,lease_term
		) NOT IN (
		SELECT cloud_provider_id
			,lease_term
		FROM lease_term_mst
		);

INSERT INTO offering_class_mst (
	cloud_provider_id
	,offering_class
	)
SELECT DISTINCT cloud_provider_id
	,offering_class
FROM server_pricing_stg
WHERE (
		cloud_provider_id
		,offering_class
		) NOT IN (
		SELECT cloud_provider_id
			,offering_class
		FROM offering_class_mst
		);

INSERT INTO payment_option_mst (
	cloud_provider_id
	,payment_option
	)
SELECT DISTINCT cloud_provider_id
	,payment_option
FROM server_pricing_stg
WHERE (
		cloud_provider_id
		,payment_option
		) NOT IN (
		SELECT cloud_provider_id
			,payment_option
		FROM payment_option_mst
		);