import re
import time
import json
import requests
import ds_database
import mysql.connector
from bs4 import BeautifulSoup

start_time = time.time()
cloud_provider = '2'
storage_type = 'Block'
volume_type = 'NA'
file_structure = 'NA'
lease_term = 'On Demand'

disk_space = ''
disk_space_uom = ''
min_iops = ''
max_iops = ''
min_throughput = ''
max_throughput = ''
min_tier = ''
min_tier_uom = ''
max_tier = ''
max_tier_uom = ''
iops_price = 0
throughput_price = 0

# Initialize database
db = ds_database.Database(cloud_provider, "", storage_type)

url = 'https://azure.microsoft.com/en-us/pricing/details/managed-disks/'
storage_pricing_list = []

try:
	response = requests.get(url)
except:
	print('Could not fetch data for ' + url)

soup = BeautifulSoup(response.text, 'html.parser')
pricing_section = soup.find('section', {'id': 'pricing'})
divs = pricing_section.find_all('div')
for div in divs:

	try:
		lrsFound = 'lrs' in div['class']
		zrsFound = 'zrs' in div['class']

		if lrsFound or zrsFound:

			if lrsFound:
				redundancy = 'lrs'

			if zrsFound:
				redundancy = 'zrs'
			
			table = div.find('table')
			rows = table.find('tbody').find_all('tr')
			for row in rows:
				td = row.find_all('td')
				cell = td[0].get_text().strip()

				storage_name = td[0].get_text().strip()
				volume_type_raw = storage_name[0:1]

				if volume_type_raw == 'E' or volume_type_raw == 'P':
					volume_type = 'SSD'

				if volume_type_raw == 'S':
					volume_type = 'HDD'	

				disk_space_raw = td[1].get_text().strip().split()
				disk_space = disk_space_raw[0]
				disk_space_uom = disk_space_raw[1]

				spans = row.find_all('span', {'class': 'price-data'})
				ondemand = json.loads(spans[0]['data-amount'])['regional']
				if redundancy == 'zrs':
					print(redundancy + '-' + storage_name + '-' + str(ondemand))
				
				for key in ondemand:
					region = key
					storage_price = ondemand[key]
					price_tuple = (cloud_provider, region, storage_type, volume_type, storage_name, redundancy, file_structure, lease_term, disk_space, disk_space_uom, min_iops, max_iops, min_throughput, max_throughput, min_tier, min_tier_uom, max_tier, max_tier_uom, storage_price, iops_price, throughput_price)
					# if region == 'us-south-central':
						# print(price_tuple)
					storage_pricing_list.append(price_tuple)

	except Exception as e:
		print(str(e))

# For debugging
# file.write(str(storage_pricing_list))
# file.close()

try:
	db.cursor.executemany(db.storage_pricing_ins_sql, storage_pricing_list)
	db.connect.commit()
	print('Data inserted successfully.')
except (mysql.connector.Error, mysql.connector.Warning) as e:
	print(e)

db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
