UPDATE storage_pricing_stg
SET region = 'NA'
WHERE region = '';

UPDATE storage_pricing_stg
SET storage_type = 'NA'
WHERE storage_type = '';

UPDATE storage_pricing_stg
SET volume_type = 'NA'
WHERE volume_type = '';

UPDATE storage_pricing_stg
SET redundancy = 'NA'
WHERE redundancy = '';

UPDATE storage_pricing_stg
SET file_structure = 'NA'
WHERE file_structure = '';

UPDATE storage_pricing_stg
SET lease_term = 'NA'
WHERE lease_term = '';

INSERT INTO region_mst (
	cloud_provider_id
	,region
	)
SELECT DISTINCT cloud_provider_id
	,region
FROM storage_pricing_stg
WHERE (
		cloud_provider_id
		,region
		) NOT IN (
		SELECT cloud_provider_id
			,region
		FROM region_mst
		);

INSERT INTO storage_type_mst (
	cloud_provider_id
	,storage_type
	)
SELECT DISTINCT cloud_provider_id
	,storage_type
FROM storage_pricing_stg
WHERE (
		cloud_provider_id
		,storage_type
		) NOT IN (
		SELECT cloud_provider_id
			,storage_type
		FROM storage_type_mst
		);

INSERT INTO volume_type_mst (
	cloud_provider_id
	,volume_type
	)
SELECT DISTINCT cloud_provider_id
	,volume_type
FROM storage_pricing_stg
WHERE (
		cloud_provider_id
		,volume_type
		) NOT IN (
		SELECT cloud_provider_id
			,volume_type
		FROM volume_type_mst
		);

INSERT INTO storage_mst (
	cloud_provider_id
	,storage_name
	)
SELECT DISTINCT cloud_provider_id
	,storage_name
FROM storage_pricing_stg
WHERE (
		cloud_provider_id
		,storage_name
		) NOT IN (
		SELECT cloud_provider_id
			,storage_name
		FROM storage_mst
		);

INSERT INTO redundancy_mst (
	cloud_provider_id
	,redundancy
	)
SELECT DISTINCT cloud_provider_id
	,redundancy
FROM storage_pricing_stg
WHERE (
		cloud_provider_id
		,redundancy
		) NOT IN (
		SELECT cloud_provider_id
			,redundancy
		FROM redundancy_mst
		);

INSERT INTO file_structure_mst (
	cloud_provider_id
	,file_structure
	)
SELECT DISTINCT cloud_provider_id
	,file_structure
FROM storage_pricing_stg
WHERE (
		cloud_provider_id
		,file_structure
		) NOT IN (
		SELECT cloud_provider_id
			,file_structure
		FROM file_structure_mst
		);

INSERT INTO lease_term_mst (
	cloud_provider_id
	,lease_term
	)
SELECT DISTINCT cloud_provider_id
	,lease_term
FROM storage_pricing_stg
WHERE (
		cloud_provider_id
		,lease_term
		) NOT IN (
		SELECT cloud_provider_id
			,lease_term
		FROM lease_term_mst
		);
