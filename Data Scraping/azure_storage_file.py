import re
import time
import json
import requests
import ds_database
import mysql.connector
from bs4 import BeautifulSoup

start_time = time.time()
cloud_provider = '2'
storage_type = 'File'
volume_type = 'NA'
file_structure = 'NA'
lease_term = 'On Demand'

disk_space = ''
disk_space_uom = ''
min_iops = ''
max_iops = ''
min_throughput = ''
max_throughput = ''
min_tier = ''
min_tier_uom = ''
max_tier = ''
max_tier_uom = ''
iops_price = 0
throughput_price = 0
row_counter = 0

storage_name_list1 = ['Premium', 'Transaction Optimized', 'Hot', 'Cold']
storage_name_list2 = ['(data)', '(snapshots)', '(metadata)']
data_storage_name_list = ['Premium I/O Intensive High Performance SSD Storage', 'Transaction Optimized - Transaction Heavy File / Backend Storage', 'Hot - Storage Optimized General Purpose Storage', 'Cold - Storage Optimized Online Archive Storage']

# Initialize database
db = ds_database.Database(cloud_provider, "", storage_type)

url = 'https://azure.microsoft.com/en-us/pricing/details/storage/files/'
storage_pricing_list = []

try:
	response = requests.get(url)
except:
	print('Could not fetch data for ' + url)

soup = BeautifulSoup(response.text, 'html.parser')
root = soup.find_all('div', {'class': 'row row-size8 column'})

table = root[0].find('table')
rows = table.find('tbody').find_all('tr')
for row in rows:

	row_title = storage_name_list2[row_counter]
	row_counter = row_counter + 1
	if row_counter == 3:
		row_counter = 0

	redundancy = row['class'][1]
	cells = row.find_all('td')

	for i in range(1, 5):

		storage_name = storage_name_list1[i-1] + ' ' + row_title

		if row_counter == 1:
			storage_name = data_storage_name_list[i-1]

		try:
			spans = cells[i].find_all('span', {'class': 'price-data'})
			ondemand = json.loads(spans[0]['data-amount'])['regional']

			for key in ondemand:
				region = key
				storage_price = ondemand[key]

				price_tuple = (cloud_provider, region, storage_type, volume_type, storage_name, redundancy, file_structure, lease_term, disk_space, disk_space_uom, min_iops, max_iops, min_throughput, max_throughput, min_tier, min_tier_uom, max_tier, max_tier_uom, storage_price, iops_price, throughput_price)
				# if region == 'us-central':
				# 	print(price_tuple)
				storage_pricing_list.append(price_tuple)

		except:
			print('Data not available or could not be parsed for ' + redundancy + ' - ' + storage_name)
			continue # move on to next row			

# For debugging
# file.write(str(storage_attributes_list))
# file.close()

try:
	db.cursor.executemany(db.storage_pricing_ins_sql, storage_pricing_list)
	db.connect.commit()
	print('Data inserted successfully.')
except (mysql.connector.Error, mysql.connector.Warning) as e:
	print(e)

db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
