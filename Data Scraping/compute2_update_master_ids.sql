UPDATE server_attributes_stg a
SET a.operating_system_mst_id = (
		SELECT b.operating_system_mst_id
		FROM operating_system_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.operating_system = a.operating_system
		);

UPDATE server_attributes_stg a
SET a.region_mst_id = (
		SELECT b.region_mst_id
		FROM region_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.region = a.region
		);

UPDATE server_attributes_stg a
SET a.instance_mst_id = (
		SELECT b.instance_mst_id
		FROM instance_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.instance_name = a.instance_name
		);

UPDATE server_attributes_stg a
SET a.instance_type_mst_id = (
		SELECT b.instance_type_mst_id
		FROM instance_type_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.instance_type = a.instance_type
		);

UPDATE server_attributes_stg a
SET a.instance_family_mst_id = (
		SELECT b.instance_family_mst_id
		FROM instance_family_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.instance_family = a.instance_family
		);

UPDATE server_attributes_stg a
SET a.processor_mst_id = (
		SELECT b.processor_mst_id
		FROM processor_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.processor = a.processor
		);

UPDATE server_pricing_stg a
SET a.lease_term_mst_id = (
		SELECT b.lease_term_mst_id
		FROM lease_term_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.lease_term = a.lease_term
		);

UPDATE server_pricing_stg a
SET a.offering_class_mst_id = (
		SELECT b.offering_class_mst_id
		FROM offering_class_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.offering_class = a.offering_class
		);

UPDATE server_pricing_stg a
SET a.payment_option_mst_id = (
		SELECT b.payment_option_mst_id
		FROM payment_option_mst b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.payment_option = a.payment_option
		);

UPDATE server_pricing_stg a
SET a.server_attributes_id = (
		SELECT b.server_attributes_id
		FROM server_attributes_stg b
		WHERE b.cloud_provider_id = a.cloud_provider_id
			AND b.operating_system = a.operating_system
			AND b.region = a.region
			AND b.instance_name = a.instance_name
		);