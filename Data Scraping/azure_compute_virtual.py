import re
import time
import json
import requests
import ds_database
import mysql.connector
from bs4 import BeautifulSoup

start_time = time.time()
cloud_provider = '2'
instance_type = 'Virtual'
offering_class = 'Standard'
payment_option = 'No Upfront'

# For debugging
# file = open('azure_compute_virtual.csv', 'w')

# Initialize database
db = ds_database.Database(cloud_provider, instance_type, "")

base_url = 'https://azure.microsoft.com/en-us/pricing/details/virtual-machines/OPERATING_SYSTEM/'

def build_url(operating_system):
	url = base_url.replace('OPERATING_SYSTEM', operating_system)
	return url

os_dictionary = {
	'linux': 'CentOS or Ubuntu Linux',
	'red-hat': 'Red Hat Enterprise Linux',
	'rhel-ha': 'Red Hat Enterprise Linux with HA',
	'rhel-sap-business': 'RHEL for SAP Business Applications',
	'rhel-sap-hana': 'RHEL for SAP HANA',
	'rhel-sap-hana-ha': 'RHEL for SAP HANA with HA',
	'sles-basic': 'SUSE Linux Enterprise + Patching only',
	'sles-standard': 'SUSE Linux Enterprise + 24x7 Support',
	'sles-hpc-standard': 'SUSE Linux Enterprise for HPC + 24x7 Support',
	'sles-sap': 'SUSE Linux Enterprise for SAP Applications + 24x7 Support',
	'ubuntu-advantage-essential': 'Ubuntu Advantage Essential',
	'ubuntu-advantage-standard': 'Ubuntu Advantage Standard',
	'ubuntu-advantage-advanced': 'Ubuntu Advantage Advanced',
	'ml-server-rhel': 'Machine Learning Server on Red Hat Enterprise Linux',
	'ml-server-ubuntu': 'Machine Learning Server on Ubuntu or Centos Linux',
	'sql-server-enterprise-linux': 'SQL Server Enterprise Ubuntu Linux',
	'sql-server-standard-linux': 'SQL Server Standard Ubuntu Linux',
	'sql-server-web-linux': 'SQL Server Web Ubuntu Linux',
	'sql-server-enterprise-redhat': 'SQL Server Enterprise Red Hat Enterprise Linux',
	'sql-server-standard-redhat': 'SQL Server Standard Red Hat Enterprise Linux',
	'sql-server-web-redhat': 'SQL Server Web Red Hat Enterprise Linux',
	'sql-server-enterprise-sles': 'SQL Server Enterprise SUSE Priority',
	'sql-server-standard-sles': 'SQL Server Standard SUSE Priority',
	'sql-server-web-sles': 'SQL Server Web SUSE Priority',
	'windows': 'Windows OS',
	'biztalk-enterprise': 'BizTalk Enterprise',
	'biztalk-standard': 'BizTalk Standard',
	'ml-server-windows': 'Machine Learning Server',
	'sharepoint': 'SharePoint',
	'sql-server-enterprise': 'SQL Server Enterprise',
	'sql-server-standard': 'SQL Server Standard',
	'sql-server-web': 'SQL Server Web'
}

# os_dictionary = {
# 	'linux': 'CentOS or Ubuntu Linux',
# 	'red-hat': 'Red Hat Enterprise Linux',
# 	'rhel-ha': 'Red Hat Enterprise Linux with HA',
# 	'rhel-sap-business': 'RHEL for SAP Business Applications',
# 	'rhel-sap-hana': 'RHEL for SAP HANA',
# 	'rhel-sap-hana-ha': 'RHEL for SAP HANA with HA',
# 	'sles-basic': 'SUSE Linux Enterprise + Patching only',
# 	'sles-standard': 'SUSE Linux Enterprise + 24x7 Support',
# 	'sles-hpc-standard': 'SUSE Linux Enterprise for HPC + 24x7 Support',
# 	'sles-sap': 'SUSE Linux Enterprise for SAP Applications + 24x7 Support',
# 	'ubuntu-advantage-essential': 'Ubuntu Advantage Essential',
# 	'ubuntu-advantage-standard': 'Ubuntu Advantage Standard',
# 	'ubuntu-advantage-advanced': 'Ubuntu Advantage Advanced',
# 	'windows': 'Windows OS'
# }

# os_dictionary = {
# 	'linux': 'CentOS or Ubuntu Linux',
# }

for os_key in os_dictionary:

	server_attributes_list = []
	server_pricing_list = []

	operating_system = os_dictionary[os_key]
	url = build_url(os_key)
	print('Fetching data for URL: ' + url)

	try:
		response = requests.get(url)
	except:
		print('Could not fetch data for ' + os_key)
		continue # move on to next OS in the list

	soup = BeautifulSoup(response.text, 'html.parser')

	# categories = soup.find_all('div', {'class': 'f  tion'})
	categories = soup.find_all('div', {'class': 'category-section'})
	for category in categories:

		# category_header = category.select("div > div > h2")
		category_header = category.select("h2")
		instance_family = category_header[0].getText()

		tables = category.find_all('table')
		for table in tables:

			if table['aria-describedby'] != 'Constrained-vCPUs-capable-footnote':

				col_heading = table.select("thead > tr > th")[0].getText()
				if col_heading == 'Add to estimate':
					pos = 1
				else:
					pos = 0

				rows = table.find('tbody').find_all('tr')
				for row in rows:

					try:
						td = row.find_all('td')

						instance_name = td[pos].get_text().strip()
						virtual_cpu = td[pos+1].get_text().strip()

						memory_list = re.findall('\d+\.\d+', td[pos+2].get_text().strip())
						if not memory_list:
							memory_list = re.findall('\d', td[pos+2].get_text().strip())

						memory = ''.join(memory_list)
						default_storage = td[pos+3].get_text().strip()

					except:
						print('Data could not be parsed for ' + str(td))
						continue # move on to next row

					spans = row.find_all('span', {'class': 'price-data'})
					ondemand = json.loads(spans[0]['data-amount'])['regional']

					try:
						yr1_reserved = json.loads(spans[1]['data-amount'])['regional']
					except:
						yr1_reserved = {}
						# print('1 yr reserved price not available for ' + instance_name)

					try:
						yr3_reserved = json.loads(spans[2]['data-amount'])['regional']
					except:
						yr3_reserved = {}
						# print('3 yr reserved price not available for ' + instance_name)

					if instance_name != 'HB60rs' and instance_name != 'HC44rs':
						for key in ondemand:
							region = key

							lease_term = 'On Demand'
							ondemand_price = ondemand[key]
							price_tuple = (cloud_provider, operating_system, region, instance_name, lease_term, offering_class, payment_option, ondemand_price)
							server_pricing_list.append(price_tuple)

							if key in yr1_reserved:
								lease_term = '1yr'
								yr1_reserved_price = yr1_reserved[key]
								price_tuple = (cloud_provider, operating_system, region, instance_name, lease_term, offering_class, payment_option, yr1_reserved_price)
								server_pricing_list.append(price_tuple)

							if key in yr3_reserved:
								lease_term = '3yr'
								yr3_reserved_price = yr3_reserved[key]
								price_tuple = (cloud_provider, operating_system, region, instance_name, lease_term, offering_class, payment_option, yr3_reserved_price)
								server_pricing_list.append(price_tuple)

							attr_tuple = (cloud_provider, operating_system, region, instance_name, virtual_cpu, memory, default_storage, instance_type, instance_family, '')
							server_attributes_list.append(attr_tuple)

							# For debugging
							# file.write(str(attr_tuple) + "\n")

	try:
		db.cursor.executemany(db.server_attributes_ins_sql, server_attributes_list)
		db.cursor.executemany(db.server_pricing_ins_sql, server_pricing_list)
		db.connect.commit()
		print('Data inserted successfully.')
	except (mysql.connector.Error, mysql.connector.Warning) as e:
		print(e)

# For debugging
# file.close()

db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
