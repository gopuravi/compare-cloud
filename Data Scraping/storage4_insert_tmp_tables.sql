INSERT INTO storage_attributes_tmp (
	cloud_provider_id,
	region_mst_id,
	storage_type_mst_id,
	volume_type_mst_id,
	storage_mst_id,
	redundancy_mst_id,
	file_structure_mst_id
	)
SELECT DISTINCT
	cloud_provider_id,
	region_mst_id,
	storage_type_mst_id,
	volume_type_mst_id,
	storage_mst_id,
	redundancy_mst_id,
	file_structure_mst_id
FROM storage_pricing_stg
WHERE storage_price > 0;

INSERT INTO storage_pricing_tmp (
	storage_attributes_id,
	lease_term_mst_id,
	disk_space,
	disk_space_uom,
	min_iops,
	max_iops,
	min_throughput,
	max_throughput,
	min_tier,
	min_tier_uom,
	max_tier,
	max_tier_uom,
	storage_price,
	iops_price,
	throughput_price
)
SELECT 
	storage_attributes_id,
	lease_term_mst_id,
	disk_space,
	disk_space_uom,
	min_iops,
	max_iops,
	min_throughput,
	max_throughput,
	min_tier,
	min_tier_uom,
	max_tier,
	max_tier_uom,
	storage_price,
	iops_price,
	throughput_price
FROM storage_pricing_stg a, storage_attributes_tmp b
WHERE b.storage_attributes_id IS NOT NULL
AND b.cloud_provider_id = a.cloud_provider_id 
AND b.region_mst_id = a.region_mst_id
AND b.storage_type_mst_id = a.storage_type_mst_id
AND b.volume_type_mst_id = a.volume_type_mst_id
AND b.storage_mst_id = a.storage_mst_id
AND b.redundancy_mst_id = a.redundancy_mst_id
AND b.file_structure_mst_id = a.file_structure_mst_id
AND a.storage_price > 0;