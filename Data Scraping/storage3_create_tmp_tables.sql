DROP TABLE IF EXISTS storage_pricing_tmp;
DROP TABLE IF EXISTS storage_attributes_tmp;

CREATE TABLE `storage_attributes_tmp` (
  `cloud_provider_id` int(11) NOT NULL,
  `region_mst_id` int(11) NOT NULL,
  `storage_type_mst_id` int(11) NOT NULL,
  `volume_type_mst_id` int(11) NOT NULL,
  `storage_mst_id` int(11) NOT NULL,
  `redundancy_mst_id` int(11) NOT NULL,
  `file_structure_mst_id` int(11) NOT NULL,
  `storage_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`storage_attributes_id`),
  UNIQUE KEY `storage_attributes_uk1` (`cloud_provider_id`,`region_mst_id`,`storage_type_mst_id`,`volume_type_mst_id`,`storage_mst_id`,`redundancy_mst_id`,`file_structure_mst_id`),
  KEY `region_mst_id` (`region_mst_id`),
  KEY `storage_type_mst_id` (`storage_type_mst_id`),
  KEY `volume_type_mst_id` (`volume_type_mst_id`),
  KEY `storage_mst_id` (`storage_mst_id`),
  KEY `redundancy_mst_id` (`redundancy_mst_id`),
  KEY `file_structure_mst_id` (`file_structure_mst_id`),
  CONSTRAINT `storage_attributes_tmp_cp_fk1` FOREIGN KEY (`cloud_provider_id`)     REFERENCES `cloud_provider_mst` (`cloud_provider_id`),
  CONSTRAINT `storage_attributes_tmp_re_fk2` FOREIGN KEY (`region_mst_id`)         REFERENCES `region_mst` (`region_mst_id`),
  CONSTRAINT `storage_attributes_tmp_st_fk3` FOREIGN KEY (`storage_type_mst_id`)   REFERENCES `storage_type_mst` (`storage_type_mst_id`),
  CONSTRAINT `storage_attributes_tmp_vt_fk4` FOREIGN KEY (`volume_type_mst_id`)    REFERENCES `volume_type_mst` (`volume_type_mst_id`),
  CONSTRAINT `storage_attributes_tmp_sn_fk5` FOREIGN KEY (`storage_mst_id`)        REFERENCES `storage_mst` (`storage_mst_id`),
  CONSTRAINT `storage_attributes_tmp_re_fk6` FOREIGN KEY (`redundancy_mst_id`)     REFERENCES `redundancy_mst` (`redundancy_mst_id`),
  CONSTRAINT `storage_attributes_tmp_fs_fk7` FOREIGN KEY (`file_structure_mst_id`) REFERENCES `file_structure_mst` (`file_structure_mst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `storage_pricing_tmp` (
  `storage_attributes_id` int(11) NOT NULL,
  `lease_term_mst_id` int(11) NOT NULL,
  `disk_space` int(10) DEFAULT NULL,
  `disk_space_uom` char(10) DEFAULT NULL,
  `min_iops` int(10) DEFAULT NULL,
  `max_iops` int(10) DEFAULT NULL,
  `min_throughput` int(10) DEFAULT NULL,
  `max_throughput` int(10) DEFAULT NULL,
  `min_tier` int(10) DEFAULT NULL,
  `min_tier_uom` char(10) DEFAULT NULL,
  `max_tier` int(10) DEFAULT NULL,
  `max_tier_uom` char(10) DEFAULT NULL,
  `storage_price` float DEFAULT NULL,
  `iops_price` float DEFAULT NULL,
  `throughput_price` float DEFAULT NULL,
  `storage_pricing_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`storage_pricing_id`),
  UNIQUE KEY `storage_pricing_uk1` (`storage_attributes_id`,`lease_term_mst_id`,`disk_space`,`disk_space_uom`,`min_iops`,`max_iops`,`min_throughput`,`max_throughput`,`min_tier`,`min_tier_uom`,`max_tier`,`max_tier_uom`),
  KEY `lease_term_mst_id` (`lease_term_mst_id`),
  CONSTRAINT `storage_pricing_tmp_lt_fk1` FOREIGN KEY (`lease_term_mst_id`)         REFERENCES `lease_term_mst` (`lease_term_mst_id`),
  CONSTRAINT `storage_pricing_tmp_sa_fk2` FOREIGN KEY (`storage_attributes_id`)     REFERENCES `storage_attributes_tmp` (`storage_attributes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
