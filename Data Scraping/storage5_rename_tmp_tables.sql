ALTER TABLE storage_attributes_tmp DROP FOREIGN KEY storage_attributes_tmp_cp_fk1;
ALTER TABLE storage_attributes_tmp DROP FOREIGN KEY storage_attributes_tmp_re_fk2;
ALTER TABLE storage_attributes_tmp DROP FOREIGN KEY storage_attributes_tmp_st_fk3;
ALTER TABLE storage_attributes_tmp DROP FOREIGN KEY storage_attributes_tmp_vt_fk4;
ALTER TABLE storage_attributes_tmp DROP FOREIGN KEY storage_attributes_tmp_sn_fk5;
ALTER TABLE storage_attributes_tmp DROP FOREIGN KEY storage_attributes_tmp_re_fk6;
ALTER TABLE storage_attributes_tmp DROP FOREIGN KEY storage_attributes_tmp_fs_fk7;

ALTER TABLE storage_pricing_tmp DROP FOREIGN KEY storage_pricing_tmp_lt_fk1;
ALTER TABLE storage_pricing_tmp DROP FOREIGN KEY storage_pricing_tmp_sa_fk2;

DROP TABLE IF EXISTS storage_pricing;
DROP TABLE IF EXISTS storage_attributes;

RENAME TABLE storage_attributes_tmp TO storage_attributes;
RENAME TABLE storage_pricing_tmp TO storage_pricing;

ALTER TABLE storage_attributes ADD CONSTRAINT `storage_attributes_cp_fk1` FOREIGN KEY (`cloud_provider_id`)     REFERENCES `cloud_provider_mst` (`cloud_provider_id`);
ALTER TABLE storage_attributes ADD CONSTRAINT `storage_attributes_re_fk2` FOREIGN KEY (`region_mst_id`)         REFERENCES `region_mst` (`region_mst_id`);
ALTER TABLE storage_attributes ADD CONSTRAINT `storage_attributes_st_fk3` FOREIGN KEY (`storage_type_mst_id`)   REFERENCES `storage_type_mst` (`storage_type_mst_id`);
ALTER TABLE storage_attributes ADD CONSTRAINT `storage_attributes_vt_fk4` FOREIGN KEY (`volume_type_mst_id`)    REFERENCES `volume_type_mst` (`volume_type_mst_id`);
ALTER TABLE storage_attributes ADD CONSTRAINT `storage_attributes_sn_fk5` FOREIGN KEY (`storage_mst_id`)        REFERENCES `storage_mst` (`storage_mst_id`);
ALTER TABLE storage_attributes ADD CONSTRAINT `storage_attributes_re_fk6` FOREIGN KEY (`redundancy_mst_id`)     REFERENCES `redundancy_mst` (`redundancy_mst_id`);
ALTER TABLE storage_attributes ADD CONSTRAINT `storage_attributes_fs_fk7` FOREIGN KEY (`file_structure_mst_id`) REFERENCES `file_structure_mst` (`file_structure_mst_id`);
ALTER TABLE storage_attributes CHANGE storage_attributes_id storage_attributes_id INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE storage_pricing ADD CONSTRAINT `storage_pricing_lt_fk1` FOREIGN KEY (`lease_term_mst_id`)         REFERENCES `lease_term_mst` (`lease_term_mst_id`);
ALTER TABLE storage_pricing ADD CONSTRAINT `storage_pricing_sa_fk2` FOREIGN KEY (`storage_attributes_id`)     REFERENCES `storage_attributes` (`storage_attributes_id`);
ALTER TABLE storage_pricing CHANGE storage_pricing_id storage_pricing_id INT(11) NOT NULL AUTO_INCREMENT;
