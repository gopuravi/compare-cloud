import time
import requests
import mysql.connector
import ds_database

start_time = time.time()
cloud_provider = '1'
storage_type = 'File'
volume_type = 'NA'
redundancy = 'NA'
file_structure = 'NA'
lease_term = 'On Demand'

disk_space = ''
disk_space_uom = ''
min_iops = ''
max_iops = ''
min_throughput = ''
max_throughput = ''
min_tier = ''
min_tier_uom = ''
max_tier = ''
max_tier_uom = ''
iops_price = 0
throughput_price = 0

# Initialize database
db = ds_database.Database(cloud_provider, "", storage_type)

# For reference - https://aws.amazon.com/efs/pricing/
url = 'https://b0.p.awsstatic.com/pricing/2.0/meteredUnitMaps/efs/USD/current/efs.json'
storage_pricing_list = []

try:
	response = requests.get(url).json()
	for region, json_region in response['regions'].items():
		for storage_name, json_storage in json_region.items():
			storage_price = json_storage['price']
			price_tuple = (cloud_provider, region, storage_type, volume_type, storage_name, redundancy, file_structure, lease_term, disk_space, disk_space_uom, min_iops, max_iops, min_throughput, max_throughput, min_tier, min_tier_uom, max_tier, max_tier_uom, storage_price, iops_price, throughput_price)
			storage_pricing_list.append(price_tuple)
except:
	print('Could not fetch data for ' + url)

try:
	db.cursor.executemany(db.storage_pricing_ins_sql, storage_pricing_list)
	db.connect.commit()
	print('Data inserted successfully.')
except (mysql.connector.Error, mysql.connector.Warning) as e:
	print(e)

db.cursor.close()
db.connect.close()

elapsed_seconds = round((time.time() - start_time))
print('Execution Time: ' + time.strftime("%H:%M:%S", time.gmtime(elapsed_seconds)) + ' (hh:mm:ss)')
