package com.cloud.authService.dao;

import com.cloud.authService.model.User;
import com.cloud.authService.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserDaoImpl {
    @Autowired
    private UserRepository userRepository;

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User findUserById(int id) {
        return userRepository.findById(id);
    }

    public List<User> findUserByName(String  name) {
        return userRepository.findByName(name);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public boolean validateUser(String name,String password) {
        return userRepository.validateUSer(name,password);
    }

    public void deleteById(int id) {
         userRepository.deleteById(id);
    }

}