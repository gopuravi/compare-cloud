package com.cloud.compareService.model;

import com.cloud.compareService.enums.SearchOperation;
import com.cloud.compareService.util.Constant;
import lombok.*;
@Getter
@Setter
public class SearchCriteria {
    private String entity;
    private String key;
    private SearchOperation operation;
    private String value;
    private Float floatValue1;
    private Float floatValue2;
    private String[] valueArr;
    private String valueType;

    public SearchCriteria(String entity, String key, SearchOperation operation, String value,String valueType) {
        this.entity = entity;
        this.key = key;
        this.operation = operation;
        this.value = value;
        this.valueType = valueType;
    }

    public SearchCriteria(String entity, String key, SearchOperation operation, String[] valueArr,String valueType) {
        this.entity = entity;
        this.key = key;
        this.operation = operation;
        this.valueArr = valueArr;
        this.valueType = valueType;
        if(valueArr!=null && valueArr.length>=2 && Constant.SEARCH_TYPE_FLOAT_ARR.equalsIgnoreCase(valueType)) {
            this.floatValue1 = Float.valueOf(valueArr[0]);
            this.floatValue2 = Float.valueOf(valueArr[1]);
        }
    }

    public SearchCriteria(String entity, String key, SearchOperation operation,String value, String[] valueArr,String valueType) {
        this.entity = entity;
        this.key = key;
        this.operation = operation;
        this.valueArr = valueArr;
        this.value = value;
        this.valueType = valueType;
        if(valueArr!=null && valueArr.length>=2 && Constant.SEARCH_TYPE_FLOAT_ARR.equalsIgnoreCase(valueType))  {
            this.floatValue1 = Float.valueOf(valueArr[0]);
            this.floatValue2 = Float.valueOf(valueArr[1]);
        }
    }



    public SearchCriteria( String key, SearchOperation operation, String value) {
        this.entity = Constant.N_A;
        this.key = key;
        this.operation = operation;
        this.value = value;
    }


}