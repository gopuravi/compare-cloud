package com.cloud.compareService.util;

import com.cloud.compareService.exception.ResponseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;

public class Util {

    private static final Logger LOG = LoggerFactory.getLogger(Util.class);


    public static Object getVOfromJsonString(String jsonString, String className)
            throws IOException, ClassNotFoundException {

        ObjectMapper mapper = new ObjectMapper();
        Object obj=null;
        obj = mapper.readValue(jsonString, Class.forName(className));
        return obj;

    }

    public static boolean stringNotEmptyOrNull(String st) {
        return st != null && !st.trim().isEmpty();
    }

    public static long getDiskSpaceInGB (long diskSpace, String diskSpaceUnit) throws ResponseException{
        long result=0;

        if (diskSpace==0 || Objects.isNull(diskSpaceUnit)){
            LOG.error("Error in conversion, Invalid input... returning zero");
            return 0;
        }
        switch (diskSpaceUnit){
            case Constant.GB:
                return diskSpace;
            case Constant.TB:
                return diskSpace * 1000;
            case Constant.PB:
                return diskSpace * 1000 * 1000;
            case Constant.EB:
                return diskSpace * 1000 * 1000 * 1000;
            default:
                throw new ResponseException("Invalid input for getDiskSpaceInGB " + diskSpaceUnit);

        }

    }
}
