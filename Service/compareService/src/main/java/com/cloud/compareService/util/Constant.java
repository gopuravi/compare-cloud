package com.cloud.compareService.util;

public final class Constant {
    //Cloud provider
    public static final String GOOGLE = "Google";
    public static final String AZURE = "Azure";
    public static final String AWS = "AWS";

    // Storage Type
    public static final String BLOCK = "Block";
    public static final String DISK_SPACE="diskSpace";


    public static final String ADMIN ="admin";
    public static final String N_A ="Not Applicable";

    public static final String ServerAttributes="ServerAttributes";
    public static final String ServerPricing="ServerPricing";

    public static final String StorageAttributes="StorageAttributes";
    public static final String StoragePricing="StoragePricing";


    public static final String ERR_DATA_NOT_AVAILABLE = "Data Not Available";

    public static final String GB = "GB";
    public static final String TB = "TB";
    public static final String PB = "PB";
    public static final String EB = "EB";

    public static final String SEARCH_TYPE_DEFAULT = "string";
    public static final String SEARCH_TYPE_INT = "int";
    public static final String SEARCH_TYPE_INT_ARR = "intArr";
    public static final String SEARCH_TYPE_STRING = "string";
    public static final String SEARCH_TYPE_STRING_ARR = "stringArr";
    public static final String SEARCH_TYPE_DATE = "date";
    public static final String SEARCH_TYPE_DATE_ARR = "dateArr";
    public static final String SEARCH_TYPE_FLOAT = "float";
    public static final String SEARCH_TYPE_FLOAT_ARR = "floatArr";

}
