package com.cloud.compareService.vo;

import com.cloud.compareService.model.StoragePricing;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Id;
import java.util.Date;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class StoragePricingOutputVO {
  public int storageAttributesId;
  public int StoragePricingId;
  public float price;
  public long diskSpace;
  public int storagePricingId;
  public String diskSpaceUom;
  public Integer minIops;
  public Integer maxIops;
  public Integer minThroughput;
  public Integer maxThroughput;
  public Integer minTier;
  public String minTierUom;
  public Integer maxTier;
  public String maxTierUom;
  public Float storagePrice;
  public Float iopsPrice;
  public Float throughputPrice;
  public Float totalPrice;

  public StoragePricingOutputVO (StoragePricing sp){
    storageAttributesId = sp.getStorageAttributesId();
    StoragePricingId=sp.getStoragePricingId();
    price=sp.getStoragePrice();
    diskSpace=sp.getDiskSpace();
    storagePricingId=sp.getStoragePricingId();
    diskSpaceUom=sp.getDiskSpaceUom();
    minIops=sp.getMinIops();
    maxIops=sp.getMaxIops();
    minThroughput=sp.getMinThroughput();
    maxThroughput=sp.getMaxThroughput();
    minTier=sp.getMinTier();
    minTierUom=sp.getMinTierUom();
    maxTier=sp.getMaxTier();
    maxTierUom=sp.getMaxTierUom();
    storagePrice=sp.getStoragePrice();
    iopsPrice=sp.getIopsPrice();
    throughputPrice=sp.getThroughputPrice();


  }
}

