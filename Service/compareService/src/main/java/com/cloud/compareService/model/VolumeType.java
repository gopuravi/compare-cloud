package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "volume_type_mst")
public class VolumeType implements Serializable {
    @Id
    private int volumeTypeMstId;
    private String volumeType;
    private String volumeTypeMapped;
    @JsonIgnore
    private boolean active;
    @JsonIgnore
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cloud_provider_id", nullable=true)
    private CloudProvider cloudProvider ;

}


