package com.cloud.compareService.dao;

import com.cloud.compareService.model.ApplicationProperties;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.ApplicationPropertiesRepository;
import com.cloud.compareService.repository.CloudProviderRepository;
import com.cloud.compareService.repository.RegionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApplicationPropertiesDaoImpl extends BaseDao<ApplicationProperties> {

    @Autowired
    public ApplicationPropertiesDaoImpl(ApplicationPropertiesRepository applicationPropertiesRepository){
        super(applicationPropertiesRepository);
    }

    public ApplicationProperties save(ApplicationProperties applicationProperties) {
        return repository.save(applicationProperties);
    }

    public List<ApplicationProperties> findAll() {
        return repository.findAll();
    }

}




