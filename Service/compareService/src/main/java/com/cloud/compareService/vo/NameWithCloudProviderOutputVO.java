package com.cloud.compareService.vo;

import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.model.ServerPricing;
import java.util.List;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class NameWithCloudProviderOutputVO {
  private String name;
  private Set<String> cloudProviderIdList;


  @Override
  public boolean equals(Object obj) {
    boolean retVal = false;
    if (obj instanceof NameWithCloudProviderOutputVO) {
      retVal = ((NameWithCloudProviderOutputVO) obj).getName().equalsIgnoreCase(this.getName());
    }
    return retVal;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 17 * hash + (this.name != null ? this.name.hashCode() : 0);
    return hash;
  }
}

