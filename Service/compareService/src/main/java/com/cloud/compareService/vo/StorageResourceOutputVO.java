package com.cloud.compareService.vo;

import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.model.StorageAttributes;
import com.cloud.compareService.util.Constant;
import com.cloud.compareService.util.Util;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class StorageResourceOutputVO {
  private StorageAttributes storageAttributes;
  private List<StoragePricingOutputVO> storagePricingOutputVOList;
  private StorageResourceInputVO storageResourceInputVO;

  private float bestPrice =0;


  public StorageResourceOutputVO(StorageAttributes storageAttributes, List<StoragePricingOutputVO> storagePricingOutputVOList, StorageResourceInputVO storageResourceInputVO) {
    this.storageAttributes = storageAttributes;
    this.storagePricingOutputVOList = storagePricingOutputVOList;
    this.storageResourceInputVO = storageResourceInputVO;
  }

  public void calculateTotalPrice(){
    if (Objects.isNull(storagePricingOutputVOList) || Objects.isNull(storageAttributes)  ||  Objects.isNull(storageResourceInputVO)  )
        return ;

    int diskSpace = storageResourceInputVO.getIntValuebyKey("diskSpace");
    int noOfMonths = storageResourceInputVO.getIntValuebyKey("tcoTerm") * 12;

    String diskSpaceUnit = storageResourceInputVO.getStringValuebyKey("diskSpaceUnit");
    int diskSpaceUnitMultiplier = 1;

    // Assumption disk space unit can only be in GB ot TB
    if(Objects.nonNull(diskSpaceUnit) && Constant.TB.equalsIgnoreCase(diskSpaceUnit) ){
      diskSpaceUnitMultiplier = 1000;
    }


    for (StoragePricingOutputVO storagePricingOutputVO : storagePricingOutputVOList){

      if( storageAttributes.getCloudProvider().getName().equalsIgnoreCase(Constant.AZURE) &&
              storageAttributes.getStorageType().getStorageTypeMapped().equalsIgnoreCase(Constant.BLOCK)) {
        storagePricingOutputVO.totalPrice = storagePricingOutputVO.getPrice() *  noOfMonths ;
      }else {
        storagePricingOutputVO.totalPrice = storagePricingOutputVO.getPrice() *  noOfMonths * diskSpace * diskSpaceUnitMultiplier;
      }

      if(bestPrice == 0 || bestPrice > storagePricingOutputVO.totalPrice ){
        bestPrice=storagePricingOutputVO.totalPrice;
      }
    }
  }

}

