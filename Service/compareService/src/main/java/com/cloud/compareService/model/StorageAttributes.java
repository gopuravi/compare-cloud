package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "storage_attributes")
public class StorageAttributes implements Serializable {

    @Id
    private int storageAttributesId;
    @JsonIgnore
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cloud_provider_id", nullable=true)
    private CloudProvider cloudProvider;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="region_mst_id", nullable=true)
    private Region region ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="storage_type_mst_id", nullable=true)
    private StorageType storageType ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="volume_type_mst_id", nullable=true)
    private VolumeType volumeType ;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="file_structure_mst_id", nullable=true)
    private FileStructure fileStructure;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="storage_mst_id", nullable=true)
    private Storage storage;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="redundancyMstId", nullable=true)
    private Redundancy redundancy;

}



