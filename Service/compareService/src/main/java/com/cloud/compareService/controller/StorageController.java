package com.cloud.compareService.controller;

import com.cloud.compareService.exception.ResponseException;
import com.cloud.compareService.service.AuthService;
import com.cloud.compareService.service.CompareService;
import com.cloud.compareService.service.DataService;
import com.cloud.compareService.service.StorageService;
import com.cloud.compareService.vo.ComputeResourceOutputVO;
import com.cloud.compareService.vo.StorageResourceOutputVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/storageCompare")
public class StorageController {

    private static final Logger LOG = LoggerFactory.getLogger(StorageController.class);


    @Value("${spring.datasource.url}")
    private String databaseUrl;


    @Autowired
    private StorageService storageService;

    @Autowired
    private DataService dataService;

    @Autowired
    private AuthService authService;




    @RequestMapping(method = RequestMethod.POST, path = "/storageResource")
    @ResponseBody
    public List<StorageResourceOutputVO> computeStorage(@RequestBody String payload, @RequestParam Map<String, Object> requestParams)
            throws ResponseException {
        List<StorageResourceOutputVO> storageResourceOutputVOList;
        System.out.println("databaseUrl " + databaseUrl);

        try {
            storageResourceOutputVOList= storageService.computeStorage(payload ,requestParams);
        } catch (Exception e) {
            System.out.println("Inside Exception" + e.getMessage());
            throw new ResponseException(e);

        }
    return storageResourceOutputVOList;

    }
}
