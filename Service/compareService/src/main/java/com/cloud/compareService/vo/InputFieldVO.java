package com.cloud.compareService.vo;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Arrays;

@AllArgsConstructor
@Getter
@Setter
@NoArgsConstructor
public class InputFieldVO implements Serializable {
    public String[] value;
    public String search;
    public String key;
    public String type;

    @Override
    public String toString() {
        return "ComputeResourceInputField{" +
            "value=" + Arrays.toString(value) +
            ", search='" + search + '\'' +
            ", key='" + key + '\'' +
            '}';
    }
}
