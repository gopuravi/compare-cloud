package com.cloud.compareService.dao;

import com.cloud.compareService.model.Instance;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.ApplicationPropertiesRepository;
import com.cloud.compareService.repository.InstanceRepository;
import com.cloud.compareService.repository.RegionRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InstanceDaoImpl  extends BaseDao<Instance>{


    @Autowired
    public InstanceDaoImpl(InstanceRepository repository){
        super(repository);
    }

    public Instance save(Instance instance) {
        return repository.save(instance);
    }

    public List<Instance> findAll() {
        return repository.findAll();
    }

}




