package com.cloud.compareService.dao;

import com.cloud.compareService.model.Region;
import com.cloud.compareService.repository.BaseRepository;
import java.util.Collection;
import java.util.List;
import org.springframework.data.jpa.domain.Specification;

public  abstract class BaseDao<E>  {

  protected final BaseRepository<E> repository;
  public List<E> findAll(Specification spec){
    return repository.findAll(spec);
  }

  protected  BaseDao(BaseRepository<E> repository){
    this.repository = repository;
  }


}
