package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "storage_type_mst")
public class StorageType implements Serializable {
    @Id
    private int storageTypeMstId;
    private String storageType;
    private String storageTypeMapped;
    @JsonIgnore
    private boolean active;
    @JsonIgnore
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cloud_provider_id", nullable=true)
    private CloudProvider cloudProvider ;

}


