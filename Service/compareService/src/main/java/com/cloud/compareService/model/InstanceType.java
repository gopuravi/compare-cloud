package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "instance_type_mst")
public class InstanceType implements Serializable {
    @Id
    private int instanceTypeMstId;
    private String instanceType;
    private String instanceTypeMapped;
    @JsonIgnore
    private boolean active;
    @JsonIgnore
    private Date createdDate;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="cloud_provider_id", nullable=true)
    private CloudProvider cloudProvider ;

}


