package com.cloud.compareService.repository;

import com.cloud.compareService.model.ServerPricing;
import com.cloud.compareService.model.StoragePricing;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface StoragePricingRepository extends JpaRepository<StoragePricing, Integer>
    , JpaSpecificationExecutor<StoragePricing>,BaseRepository<StoragePricing> {

    public StoragePricing findById(int Id);
}
