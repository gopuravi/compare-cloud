package com.cloud.compareService.repository;

import com.cloud.compareService.model.PaymentOption;
import com.cloud.compareService.model.Processor;
import com.cloud.compareService.model.ServerPricing;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface ProcessorRepository extends JpaRepository<Processor, Integer>
    , JpaSpecificationExecutor<Processor>,BaseRepository<Processor>{

    public Processor findById(int Id);
    
    public List<Processor> findAll();

}
