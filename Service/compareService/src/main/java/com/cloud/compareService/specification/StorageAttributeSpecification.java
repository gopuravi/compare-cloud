package com.cloud.compareService.specification;


import com.cloud.compareService.model.SearchCriteria;
import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.util.Constant;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class StorageAttributeSpecification extends BaseSpecification<ServerAttributes> {


    public StorageAttributeSpecification(SearchCriteria searchCriteria) {
        super(searchCriteria);
    }


}
