package com.cloud.compareService.service;

import com.cloud.compareService.dao.ApplicationPropertiesDaoImpl;
import com.cloud.compareService.dao.BaseDao;
import com.cloud.compareService.dao.CloudProvidersDaoImpl;
import com.cloud.compareService.dao.InstanceDaoImpl;
import com.cloud.compareService.dao.InstanceFamilyDaoImpl;
import com.cloud.compareService.dao.InstanceTypeDaoImpl;
import com.cloud.compareService.dao.LeaseTermDaoImpl;
import com.cloud.compareService.dao.OfferingClassDaoImpl;
import com.cloud.compareService.dao.OperatingSystemDaoImpl;
import com.cloud.compareService.dao.PaymentOptionDaoImpl;
import com.cloud.compareService.dao.ProcessorDaoImpl;
import com.cloud.compareService.dao.RegionDaoImpl;
import com.cloud.compareService.dao.ServerAttributesDaoImpl;
import com.cloud.compareService.dao.ServerPricingDaoImpl;
import com.cloud.compareService.enums.SearchOperation;
import com.cloud.compareService.exception.ResponseException;
import com.cloud.compareService.model.ApplicationProperties;
import com.cloud.compareService.model.CloudProvider;
import com.cloud.compareService.model.Instance;
import com.cloud.compareService.model.InstanceFamily;
import com.cloud.compareService.model.LeaseTerm;
import com.cloud.compareService.model.OfferingClass;
import com.cloud.compareService.model.OperatingSystem;
import com.cloud.compareService.model.PaymentOption;
import com.cloud.compareService.model.Processor;
import com.cloud.compareService.model.Region;
import com.cloud.compareService.model.SearchCriteria;
import com.cloud.compareService.model.ServerAttributes;
import com.cloud.compareService.model.ServerPricing;
import com.cloud.compareService.repository.RegionRepository;
import com.cloud.compareService.specification.BaseSpecification;
import com.cloud.compareService.util.*;
import com.cloud.compareService.vo.NameWithCloudProviderOutputVO;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DataService {

  private static final Logger LOG = LoggerFactory.getLogger(DataService.class);

  private List<CloudProvider> cloudProviderList;

  @Autowired
  ApplicationPropertiesDaoImpl applicationPropertiesDao;

  @Autowired
  CloudProvidersDaoImpl cloudProvidersDao;

  @Autowired
  InstanceDaoImpl instanceDao;

  @Autowired
  InstanceFamilyDaoImpl instanceFamilyDao;

  @Autowired
  InstanceTypeDaoImpl instanceTypeDao;

  @Autowired
  LeaseTermDaoImpl leaseTermDao;

  @Autowired
  OfferingClassDaoImpl offeringClassDao;

  @Autowired
  OperatingSystemDaoImpl operatingSystemDao;

  @Autowired
  PaymentOptionDaoImpl paymentOptionDao;

  @Autowired
  ProcessorDaoImpl processorDao;

  @Autowired
  RegionRepository regionRepository;

  @Autowired
  ServerAttributesDaoImpl serverAttributesDao;

  @Autowired
  ServerPricingDaoImpl serverPricingDao;

  @Autowired
  RegionDaoImpl regionDao;

  @Autowired
  FactoryUtil factoryUtil;





  public List<ApplicationProperties> getApplicationProperties() {
    return applicationPropertiesDao.findAll();
  }

  public List<CloudProvider> getCloudProvider() {
    return cloudProvidersDao.findAll();
  }

  public List<Instance> getInstance() { return instanceDao.findAll();  }

  public List<InstanceFamily> getInstanceFamily() { return instanceFamilyDao.findAll();  }

  public List<LeaseTerm> getLeaseTerm() { return leaseTermDao.findAll();  }

  public List<OfferingClass> getOfferingClass() { return offeringClassDao.findAll();  }

  public List<OperatingSystem> getOperatingSystem() { return operatingSystemDao.findAll();  }

  public List<PaymentOption> getPaymentOption() { return paymentOptionDao.findAll();  }


  public List<Processor> getProcessor() { return processorDao.findAll();  }

  public List<Region> getRegion() {
    return regionRepository.findAll();
  }

  public List<NameWithCloudProviderOutputVO> getUniqueNameByCloudProvider( Map<String, String> requestParams)
  throws ResponseException{
    try {
      String cloudProviderIds = requestParams.get("cloudProviderId");
      String attribute = requestParams.get("attribute");
      String attributeField = requestParams.get("attributeField");

      Specification specs = Specification.where(null);
      List<NameWithCloudProviderOutputVO> outputList = new ArrayList<>();
      SearchMapping searchMapping = SearchMapping.valueOf(attribute+"Mst");
      InputAttribute inputAttribute = InputAttribute.valueOf(attribute);

      if(!Util.stringNotEmptyOrNull(attributeField) ){
        attributeField = inputAttribute.attribute;
      }

      if (cloudProviderIds != null && Util.stringNotEmptyOrNull(cloudProviderIds)) {
        cloudProviderIds = cloudProviderIds.replaceAll("[{[}]]", "").trim();
        String[] cloudProviderArr = cloudProviderIds != null ? cloudProviderIds.split(",") : null;

        BaseSpecification spec = new BaseSpecification(
            new SearchCriteria(searchMapping.entity, searchMapping.attribute, SearchOperation.IN,
                cloudProviderArr, Constant.SEARCH_TYPE_STRING_ARR));
        specs = specs.and(spec);
      }

      BaseDao baseDao = factoryUtil.getDaoByName(attribute) ;

        List<Object> objList = baseDao.findAll(specs);

      for (Object obj : objList) {
        boolean isPresent = false;
        Field field = obj.getClass().getDeclaredField(attributeField);
        Field cloudProviderField = obj.getClass().getDeclaredField("cloudProvider");
        field.setAccessible(true); cloudProviderField.setAccessible(true);
        CloudProvider cloudProviderObj = (CloudProvider) cloudProviderField.get(obj);

        for (NameWithCloudProviderOutputVO outputVO : outputList) {
          if (outputVO.getName().equalsIgnoreCase( (String) field.get(obj))) {
            outputVO.getCloudProviderIdList().add(
                cloudProviderObj.getCloudProviderId());
               // region.getCloudProvider().getCloudProviderId());
            isPresent=true;
            continue;
          }
        }
        if (!isPresent) {



          outputList.add(new NameWithCloudProviderOutputVO( (String) field.get(obj),
              new HashSet<String>() {{
                add(cloudProviderObj.getCloudProviderId());
              }}
          ));
        }
      }
      return outputList;
    }catch(Exception e){
      LOG.error(e.getMessage(), e);
      throw new ResponseException(e);
    }

  }


  public List<ServerAttributes> getServerAttributes() { return serverAttributesDao.findAll();  }

  public List<ServerPricing> getServerPricing() { return serverPricingDao.findAll();  }

  public Set<String> getUniqueAttributeList(String field) throws ResponseException {
    Set<String> result=null;
    try {
      if (!Util.stringNotEmptyOrNull(field))
        throw new IllegalArgumentException("Input Parameter not valid");
      InputAttribute inputAttribute = InputAttribute.valueOf(field);

      Method method = this.getClass().getMethod(inputAttribute.entity);

      switch (inputAttribute) {
        case region:
          result = null;
          break;
        case cloudProvider:
          result = null;
          break;
        default:
          throw new ResponseException("Mapping not available for given input field " + field);
      }
    }catch(Exception e){
      throw new ResponseException(e.getMessage());
    }
    return result;
  }







}
