package com.cloud.compareService.repository;

import com.cloud.compareService.model.OperatingSystem;
import com.cloud.compareService.model.PaymentOption;
import com.cloud.compareService.model.Processor;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;


@Repository
public interface PaymentOptionRepository extends JpaRepository<PaymentOption, Integer>
    , JpaSpecificationExecutor<PaymentOption>,BaseRepository<PaymentOption>{

    public PaymentOption findById(int Id);
    
    public List<PaymentOption> findAll();

}
