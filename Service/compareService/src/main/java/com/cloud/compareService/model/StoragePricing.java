package com.cloud.compareService.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;


@AllArgsConstructor
@Builder
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "storage_pricing")
public class StoragePricing implements Serializable {

    @Id
    private int storagePricingId;
    @JsonIgnore
    private Date createdDate;
    private Integer diskSpace;
    private String diskSpaceUom;
    private Integer minIops;
    private Integer maxIops;
    private Integer minThroughput;
    private Integer maxThroughput;
    private Integer minTier;
    private String minTierUom;
    private Integer maxTier;
    private String maxTierUom;
    private Float storagePrice;
    private Float iopsPrice;
    private Float throughputPrice;
    private int storageAttributesId;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="lease_term_mst_id", nullable=true)
    private LeaseTerm leaseTerm;

}



