package com.cloud.compareService.service;

import com.cloud.compareService.dao.CloudProvidersDaoImpl;
import com.cloud.compareService.dao.RegionDaoImpl;
import com.cloud.compareService.dao.ServerAttributesDaoImpl;
import com.cloud.compareService.enums.SearchOperation;
import com.cloud.compareService.exception.ResponseException;
import com.cloud.compareService.model.*;
import com.cloud.compareService.repository.ServerAttributesRepository;
import com.cloud.compareService.repository.ServerPricingRepository;
import com.cloud.compareService.repository.StorageAttributesRepository;
import com.cloud.compareService.repository.StoragePricingRepository;
import com.cloud.compareService.specification.BaseSpecification;
import com.cloud.compareService.specification.ServerAttributeSpecification;
import com.cloud.compareService.specification.ServerPricingSpecification;
import com.cloud.compareService.specification.StoragePricingSpecification;
import com.cloud.compareService.util.Constant;
import com.cloud.compareService.util.OutputUtil;
import com.cloud.compareService.util.SearchMapping;
import com.cloud.compareService.util.Util;
import com.cloud.compareService.vo.*;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.*;

@Slf4j
@Service
public class StorageService {

  private static final Logger LOG = LoggerFactory.getLogger(StorageService.class);

  private List<CloudProvider> cloudProviderList;

  @Autowired
  CloudProvidersDaoImpl cloudProvidersDao;

  @Autowired
  RegionDaoImpl RegionDao;


  @Autowired
  StorageAttributesRepository storageAttributesRepository;

  @Autowired
  StoragePricingRepository storagePricingRepository;

  public List<CloudProvider> getCloudProvider() {
    return cloudProvidersDao.findAll();
  }

  public List<Region> getRegion() {
    return RegionDao.findAll();
  }

  public List<StorageResourceOutputVO> computeStorage(String payload, Map<String, Object> requestParams)
      throws ResponseException {
    List<StorageResourceOutputVO> storageResourceOutputVOList = new ArrayList<>();
    List<StorageResourceOutputVO> storageResourceOutputVOListTemp = new ArrayList<>();
    int diskSpace=0;
    String diskSpaceUnit=Constant.N_A;
    List<StorageAttributes> storageAttributesList = new ArrayList<>();
    List<StoragePricing> storagePricingList = new ArrayList<>();

    try {
      ObjectMapper mapper = new ObjectMapper();
      mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

      StorageResourceInputVO[] storageResourceInputVOList = mapper.readValue(payload, StorageResourceInputVO[].class);

      for (StorageResourceInputVO storageResourceInput : storageResourceInputVOList) {
        LOG.info("computeResourceInput" + storageResourceInput);

         diskSpace = storageResourceInput.getIntValuebyKey("diskSpace");
         diskSpaceUnit = storageResourceInput.getStringValuebyKey("diskSpaceUnit");

        storageAttributesList=findStorageAttribute(storageResourceInput);

        for(StorageAttributes storageAttribute: storageAttributesList) {
          storagePricingList = findStoragePricing(storageAttribute.getStorageAttributesId(),storageResourceInput);
          LOG.info("Size is " + storagePricingList.size());


          List<StoragePricingOutputVO> storagePricingOutputVOList = OutputUtil.ConvertStoragePricingListToStoragePricingVOList(
                  (ArrayList<StoragePricing>) storagePricingList, storageResourceInput);



          if(storagePricingOutputVOList.size() >0 ) {
            StorageResourceOutputVO storageResourceOutputVO = new StorageResourceOutputVO(storageAttribute, storagePricingOutputVOList, storageResourceInput);
            storageResourceOutputVO.calculateTotalPrice();
            if( storageAttribute.getCloudProvider().getName().equalsIgnoreCase(Constant.AZURE) &&
                    storageAttribute.getStorageType().getStorageTypeMapped().equalsIgnoreCase(Constant.BLOCK)){
              storageResourceOutputVOListTemp.add(storageResourceOutputVO);
            }else{
              storageResourceOutputVOList.add(storageResourceOutputVO);
            }
          }


        }
      }

      //filtering for azure and block. need to select matching or next higher value
      storageResourceOutputVOListTemp = filterOutputForAzureBlock(storageResourceOutputVOListTemp , storageResourceInputVOList);

      storageResourceOutputVOList.addAll(storageResourceOutputVOListTemp);

    } catch (Exception e) {
      LOG.error("Error in Compute" , e);
      throw new ResponseException (e.getMessage() );
    }
    return storageResourceOutputVOList;
  }

  private  List<StorageResourceOutputVO> filterOutputForAzureBlock(List<StorageResourceOutputVO> storageResourceOutputVOListTemp, StorageResourceInputVO[] storageResourceInputVOList)
    throws ResponseException{
    List<StorageResourceOutputVO> result = new ArrayList<StorageResourceOutputVO>();


    for (StorageResourceInputVO storageResourceInput : storageResourceInputVOList) {
      long bestMatch=Long.MAX_VALUE;
      StorageResourceOutputVO selected = null;
      int diskSpace = storageResourceInput.getIntValuebyKey("diskSpace");
      String diskSpaceUnit = storageResourceInput.getStringValuebyKey("diskSpaceUnit");
      String[] cloudProviderArr =  (String[]) storageResourceInput.getValuebyKey("cloudProvider");
      if( Arrays.asList(cloudProviderArr).contains(Constant.AZURE) &&
              storageResourceInput.getStringValuebyKey("storageType").equalsIgnoreCase(Constant.BLOCK)){
        long diskSpaceinGB= Util.getDiskSpaceInGB(diskSpace, diskSpaceUnit);
        for (StorageResourceOutputVO storageResourceOutputVO : storageResourceOutputVOListTemp) {
          String inputWorkLoadId=storageResourceInput.getStringValuebyKey("workloadId");
          String storageResourceOutputVOWorkloadId = storageResourceOutputVO.getStorageResourceInputVO().getStringValuebyKey("workloadId");
          if(inputWorkLoadId.equalsIgnoreCase(storageResourceOutputVOWorkloadId)) {

            for (StoragePricingOutputVO storagePricingOutputVO : storageResourceOutputVO.getStoragePricingOutputVOList()) {
              if (Util.getDiskSpaceInGB(storagePricingOutputVO.getDiskSpace(), storagePricingOutputVO.getDiskSpaceUom()) > diskSpaceinGB
                      && Util.getDiskSpaceInGB(storagePricingOutputVO.getDiskSpace(), storagePricingOutputVO.getDiskSpaceUom()) < bestMatch
              ) {
                bestMatch = Util.getDiskSpaceInGB(storagePricingOutputVO.getDiskSpace(), storagePricingOutputVO.getDiskSpaceUom());
                selected = storageResourceOutputVO;
              }
            }
          }
        }

        if(Objects.nonNull(selected)) {
          result.add(selected);
        }
      }
    }

    result= getAllMatchingNames(storageResourceOutputVOListTemp , result);
    return result;
  }

  private  List<StorageResourceOutputVO> getAllMatchingNames(  List<StorageResourceOutputVO> storageResourceOutputVOListTemp, List<StorageResourceOutputVO> selectedList){
    String inputWorkLoadIdSel,storageNameSel,workloadId,storageName;

    List<StorageResourceOutputVO> result = new ArrayList<>();
    for (StorageResourceOutputVO storageResourceOutputVOSel : selectedList) {
       inputWorkLoadIdSel=storageResourceOutputVOSel.getStorageResourceInputVO().getStringValuebyKey("workloadId");
       storageNameSel=storageResourceOutputVOSel.getStorageAttributes().getStorage().getStorageName();
      if(Objects.nonNull(inputWorkLoadIdSel) && Objects.nonNull(storageNameSel)) {
        for (StorageResourceOutputVO storageResourceOutputVO : storageResourceOutputVOListTemp) {
          workloadId = storageResourceOutputVO.getStorageResourceInputVO().getStringValuebyKey("workloadId");
          storageName = storageResourceOutputVO.getStorageAttributes().getStorage().getStorageName();

          if (inputWorkLoadIdSel.equalsIgnoreCase(workloadId) && storageNameSel.equalsIgnoreCase(storageName)){
            result.add(storageResourceOutputVO);
          }
        }
      }
    }
    return result;
  }


  private List<StoragePricing> findStoragePricing ( int storageAttributesId ,StorageResourceInputVO storageResourceInput)
          throws IllegalAccessException{
    List<BaseSpecification> baseSpecificationList = new ArrayList<>();


    for(InputFieldVO field: storageResourceInput.getWorkload()){
      if(Util.stringNotEmptyOrNull(field.getKey())
              && Arrays.asList(SearchMapping.valueOf(field.getKey()).parent).contains(Constant.StoragePricing)
              && field.getValue()!=null && field.getValue().length>0 && Util.stringNotEmptyOrNull(field.getValue()[0])
              && Util.stringNotEmptyOrNull(field.getSearch())

      ){
        SearchMapping searchMapping = SearchMapping.valueOf(field.getKey());
        BaseSpecification spec;

          if(!field.getKey().equalsIgnoreCase(Constant.DISK_SPACE)) {
            spec = new StoragePricingSpecification(
                    new SearchCriteria(searchMapping.entity, searchMapping.attribute, SearchOperation.valueOf(field.getSearch()), field.getValue()[0], field.getValue(), field.getType()));
            baseSpecificationList.add(spec);
          }

      }
    }


    SearchMapping searchMapping = SearchMapping.valueOf("storageAttribute");
    Specification specs = Specification.where(new StoragePricingSpecification(
            new SearchCriteria(searchMapping.entity, searchMapping.attribute,SearchOperation.EQUALITY, Integer.toString(storageAttributesId),Constant.SEARCH_TYPE_INT))
    );
    for (Specification spec : baseSpecificationList) {
      specs = specs.and(spec);
    }

    List<StoragePricing> storagePricingList = storagePricingRepository.findAll(specs);
    return storagePricingList;
  }







  private List<StorageAttributes> findStorageAttribute(StorageResourceInputVO storageResourceInputVO)
      throws IllegalAccessException {

    List<BaseSpecification> baseSpecificationList = new ArrayList<>();

    for(InputFieldVO field: storageResourceInputVO.getWorkload()){
      if(Util.stringNotEmptyOrNull(field.getKey())
              && Arrays.asList(SearchMapping.valueOf(field.getKey()).parent).contains(Constant.StorageAttributes)
          && field.getValue()!=null && field.getValue().length>0 && Util.stringNotEmptyOrNull(field.getValue()[0])
            && Util.stringNotEmptyOrNull(field.getSearch())
      ){
        SearchMapping searchMapping = SearchMapping.valueOf(field.getKey());

        BaseSpecification spec = new BaseSpecification(
            new SearchCriteria(searchMapping.entity, searchMapping.attribute,SearchOperation.valueOf(field.getSearch()), field.getValue()[0] , field.getValue() ,field.getType()));
        baseSpecificationList.add(spec);

      }
    }

    Specification specs = Specification.where(null);
    for (Specification spec : baseSpecificationList) {
      specs = specs.and(spec);
    }

    List<StorageAttributes> storageAttributesList = storageAttributesRepository.findAll(specs);
    return storageAttributesList;
  }

}
