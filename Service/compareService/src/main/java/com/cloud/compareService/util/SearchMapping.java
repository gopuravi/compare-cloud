package com.cloud.compareService.util;

import lombok.Getter;
import lombok.Setter;


public enum


SearchMapping {

  noOfServers(new String[]{Constant.N_A},Constant.N_A, "noOfServers"),
  workloadId(new String[]{Constant.N_A},Constant.N_A, "workloadId"),
  usagePerMonth(new String[]{Constant.N_A},Constant.N_A, "usagePerMonth"),
  tcoTerm(new String[]{Constant.N_A},Constant.N_A, "tcoTerm"),
  storageId(new String[]{Constant.N_A},Constant.N_A, "storageId"),

  cloudProviderMst(new String[]{"cloudProvider"},"cloudProvider", "cloudProviderId"),
  regionMst(new String[]{"region"},"cloudProvider", "cloudProviderId"),
  processorMst(new String[]{"processor"},"cloudProvider", "cloudProviderId"),
  instanceMst(new String[]{"instance"},"cloudProvider", "cloudProviderId"),
  instanceTypeMst(new String[]{"instanceType"},"cloudProvider", "cloudProviderId"),
  instanceFamilyMst(new String[]{"instanceFamily"},"cloudProvider", "cloudProviderId"),
  operatingSystemMst(new String[]{"operatingSystem"},"cloudProvider", "cloudProviderId"),
  leaseTermMst(new String[]{"leaseTerm"},"cloudProvider", "cloudProviderId"),
  offeringClassMst(new String[]{"offeringClass"},"cloudProvider", "cloudProviderId"),
  paymentOptionMst(new String[]{"paymentOption"},"cloudProvider", "cloudProviderId"),


  memory(new String[]{Constant.ServerAttributes},Constant.N_A, "memory"),
  defaultStorage(new String[]{Constant.ServerAttributes},Constant.N_A, "defaultStorage"),
  region(new String[]{Constant.ServerAttributes},"region", "regionGroupMapped"),
  regionGroup(new String[]{Constant.ServerAttributes,Constant.StorageAttributes} ,"region", "regionGroupMapped"),
  cloudProvider(new String[]{Constant.ServerAttributes,Constant.StorageAttributes},"cloudProvider", "name"),
  instance(new String[]{Constant.ServerAttributes},"instance", "instanceName"),
  instanceType(new String[]{Constant.ServerAttributes},"instanceType","instanceTypeMapped"),
  instanceFamily(new String[]{Constant.ServerAttributes},"instanceFamily","instanceFamilyMapped"),
  processor(new String[]{Constant.ServerAttributes},"processor","processorFamilyMapped"),
  operatingSystem(new String[]{Constant.ServerAttributes},"operatingSystem","operatingSystemMapped"),
  operatingSystemFamily(new String[]{Constant.ServerAttributes},"operatingSystem","operatingSystemFamily"),
  virtualCpu(new String[]{Constant.ServerAttributes},Constant.N_A, "virtualCpu"),

  leaseTerm(new String[]{Constant.ServerPricing},"leaseTerm", "leaseTermMapped"),
  offeringClass(new String[]{Constant.ServerPricing},"offeringClass", "offeringClassMapped"),
  paymentOption(new String[]{Constant.ServerPricing},"paymentOption", "paymentOptionMapped"),
  serverAttribute(new String[]{Constant.ServerPricing},Constant.N_A, "serverAttributesId"),



  /*  for storage attribute */
  storageType(new String[]{Constant.StorageAttributes},"storageType","storageTypeMapped"),
  volumeType(new String[]{Constant.StorageAttributes},"volumeType","volumeTypeMapped"),
  storageAttribute(new String[]{Constant.StorageAttributes},Constant.N_A, "storageAttributesId"),

  /*  for storage pricing */
  diskSpace(new String[]{Constant.StoragePricing},Constant.N_A, "diskSpace"),
  storagePricing(new String[]{Constant.StoragePricing},Constant.N_A, "storagePricingId"),
  diskSpaceUnit(new String[]{Constant.N_A},Constant.N_A, "diskSpaceUom"),
  storagePricingAttribute(new String[]{Constant.ServerPricing},Constant.N_A, "storageAttributesId")


  ;


  public String entity;
  public String attribute;
  public String[] parent;
  SearchMapping(String[] parent,String entity,String attribute) {
    this.entity = entity;
    this.attribute=attribute;
    this.parent=parent;
  }

}


