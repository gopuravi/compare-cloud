import React, { useEffect, useState } from 'react';
import { Container, Accordion, Card, Modal, Button } from 'react-bootstrap';
import TableGrid from '@maze/shared-components/TableGrid/TableGrid.component';
import { ChevronRight, ChevronDown, BoxArrowUp } from 'react-bootstrap-icons';
import ReactECharts from 'echarts-for-react';

import ModifyWorkloadItemsService from './../ModifyWorkload/services/ModifyWorkload.items.services';
import CompareOptimizeItemsService from './services/CompareOptimize.transform.services';
import { workloadTypes } from '@maze/constants';
import html2canvas from 'html2canvas';
import { jsPDF } from 'jspdf';
import logo from './../../assets/logo_pdf.png';

// Language
import i18next from 'i18next';

const CompareOptimize = (props) => {
  const [stateOptimizedData, setStateOptimizedData] = useState([]);
  const [accordionState, setAccordionState] = useState([]);
  const [hasDataState, setHasDataState] = useState(false);
  const [stateChartOptions, setStateChartOptions] = useState({});
  const [stateTotalAmountByCSP, setStateTotalAmountByCSP] = useState({});

  const columnDef = {
    computeResource: [
      {
        headerName: i18next.t('workloadName'),
        field: 'workloadName',
        headerTooltip: i18next.t('workloadName'),
        tooltipField: 'workloadName',
        width: '20%',
      },
      {
        headerName: i18next.t('instanceName'),
        field: 'instanceName',
        headerTooltip: i18next.t('instanceName'),
        tooltipField: 'instanceName',
        width: '20%',
      },
      {
        headerName: i18next.t('#OfServers'),
        field: 'selectedNoOfServer',
        headerTooltip: i18next.t('noOfServers'),
        width: '10%',
      },
      {
        headerName: i18next.t('vCPU'),
        field: 'selectedVCpu',
        headerTooltip: i18next.t('vCPU'),
        headerSecondaryText: i18next.t('common:onlyPerServer'),
        width: '15%',
      },
      {
        headerName: i18next.t('memory'),
        field: 'selectedMemory',
        headerTooltip: i18next.t('memory'),
        headerSecondaryText: i18next.t('common:perServer'),
        width: '15%',
      },
      {
        headerName: i18next.t('cost'),
        field: 'costDisplayFormat',
        headerTooltip: i18next.t('cost'),
        headerSecondaryText: i18next.t('common:unitDollar'),
        width: '10%',
      },
      {
        headerName: i18next.t('avgCost'),
        field: 'avgCost',
        headerTooltip: i18next.t('avgConst'),
        headerSecondaryText: i18next.t('common:unitDollar'),
        width: '10%',
      },
    ],
    storage: [
      {
        headerName: i18next.t('workloadName'),
        field: 'workloadName',
        headerTooltip: i18next.t('workloadName'),
        tooltipField: 'workloadName',
        width: '20%',
      },
      {
        headerName: i18next.t('storageName'),
        field: 'storageName',
        headerTooltip: i18next.t('storageName'),
        tooltipField: 'storageName',
        width: '20%',
      },
      {
        headerName: i18next.t('diskSpace'),
        field: 'diskSpace',
        headerTooltip: i18next.t('diskSpace'),
        headerSecondaryText: i18next.t('common:unitGB'),
        width: '10%',
      },
      {
        headerName: i18next.t('storageType'),
        field: 'storageType',
        headerTooltip: i18next.t('storageType'),
        width: '20%',
      },
      {
        headerName: i18next.t('volumeType'),
        field: 'volumeType',
        headerTooltip: i18next.t('volumeType'),
        width: '20%',
      },
      {
        headerName: i18next.t('totalCost'),
        field: 'costDisplayFormat',
        headerTooltip: i18next.t('totalCost'),
        headerSecondaryText: i18next.t('common:unitDollar'),
        width: '10%',
      },
    ],
  };

  useEffect(() => {
    const optimizedData = ModifyWorkloadItemsService.getModifiedWorkloadData();
    const resultData = CompareOptimizeItemsService.loadItems(optimizedData);
    if (resultData.length) {
      const chartOption = CompareOptimizeItemsService.loadChart(optimizedData);
      setStateChartOptions(chartOption);
    }

    const accordionExpand = resultData.map(() =>
      Object.keys(workloadTypes).map(() => true)
    );

    const dataByCSP = resultData.map((item) => {
      return {
        [item.cloudProvider.name]: {
          ...item,
        },
      };
    });

    const totalAmountByCSP = calculateTotalAmountByCSP(dataByCSP);

    setStateTotalAmountByCSP(totalAmountByCSP);

    const hasData = Object.keys(workloadTypes).some((keys) =>
      resultData.some((result) => result[`${keys}Results`])
    );

    setHasDataState(hasData);
    setStateOptimizedData(resultData);
    setAccordionState(accordionExpand);
  }, []);

  useEffect(() => {
    downloadPdfDocument(); //children function of interest
  }, [props.downloadReportState]);

  const calculateTotalAmountByCSP = (data) => {
    const dataToObject = toObject(data);

    const totalAmountInArray = Object.keys(dataToObject).map((item) => {
      const computeResourceCost =
        dataToObject[item]?.computeResourceResults?.totalCost ?? 0;
      const storageCost = dataToObject[item]?.storageResults?.totalCost ?? 0;
      return {
        [item]: computeResourceCost + storageCost,
      };
    });

    return toObject(totalAmountInArray);
  };

  const toObject = (data) => {
    let tempObject = {};
    for (let amount of data) {
      let [first] = Object.keys(amount);
      tempObject[first] = amount[first];
    }

    return tempObject;
  };

  const activeOnClick = (id, keyId) => {
    const newActiveData = accordionState.map((value, index) => {
      return index === id
        ? value.map((status, index) => (index === keyId ? !status : status))
        : value;
    });

    setAccordionState(newActiveData);
  };

  const downloadPdfDocument = () => {
    if (stateOptimizedData.length) {
      const tcoTerm = stateOptimizedData[0].computeResource[0].tcoTerm;
      const cloudProviders = stateOptimizedData
        .map((optimizedData) => optimizedData.cloudProvider.name)
        .join(', ');
      const input = document.querySelector('#forPDF');
      const inputTableData = document.querySelector('#forPDFTableData');
      if (input && inputTableData) {
        const pdf = new jsPDF();
        pdf.addImage(logo, 'PNG', 15, 10);
        pdf.setFontSize(20);
        pdf.setFont('helvetica', 'bold');
        pdf.text('Compare Cloud Report', 105, 45, null, null, 'center');
        pdf.setFontSize(14);
        pdf.setFont('helvetica', 'normal');
        pdf.text(
          `Total cost of compute, storage, and network resources between \n ${cloudProviders} for ${tcoTerm} years`,
          105,
          55,
          null,
          null,
          'center'
        );
        const promise1 = html2canvas(input);
        const promise2 = html2canvas(inputTableData);

        const promises = [promise1, promise2];
        Promise.allSettled(promises)
          .then((results) => {
            results.forEach((result, index) => {
              if (result.status === 'fulfilled') {
                const canvas = result.value;
                const imgData = canvas.toDataURL('image/png');
                if (index > 0) {
                  pdf.addPage();
                  pdf.addImage(imgData, 'JPEG', 5, 20, 200, 100);
                } else {
                  pdf.addImage(imgData, 'JPEG', 5, 75, 200, 100);
                }
              }
            });

            const dateTime = new Date().getTime();
            const fileName = `compare_cloud_${dateTime}.pdf`;
            pdf.save(fileName);
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
  };

  return (
    <>
      <Container fluid className="m-0 p-0">
        <div>
          {hasDataState && (
            <div>
              <h5 className="px-3">Compare Cloud Report</h5>
              <div id="forPDF">
                <ReactECharts
                  option={{ ...stateChartOptions }}
                  style={{ height: '500px' }}
                />
              </div>
            </div>
          )}
          <div id="forPDFTableData" className="m-3">
            {hasDataState &&
              stateOptimizedData.map((data, dataIndex) => {
                return (
                  <Card key={`${dataIndex}_card`} className={`border-1 mt-3`}>
                    <Card.Header
                      key={`${dataIndex}_card_header`}
                      className={'py-1 px-3 bg-white border-bottom-0'}
                    >
                      <h5 className="mb-0">
                        {`${i18next.t(data.cloudProvider.name)} - ${i18next.t(
                          'common:unitDollar'
                        )}${stateTotalAmountByCSP[data.cloudProvider.name]}`}
                      </h5>
                    </Card.Header>
                    <Card.Body key={`${dataIndex}_card_body`} className={'p-0'}>
                      {Object.keys(workloadTypes).map((item, keyItemIndex) => {
                        const hasResults = data[`${item}Results`] ?? false;
                        if (hasResults) {
                          return (
                            <Accordion
                              flush="true"
                              key={`${item}_${data.cloudProvider.name}_${dataIndex}_accordion`}
                            >
                              {hasResults && (
                                <React.Fragment key={`${dataIndex}_fragment`}>
                                  <Accordion.Toggle
                                    as={Card.Header}
                                    className={'cursor-pointer px-3'}
                                    onClick={() =>
                                      activeOnClick(dataIndex, keyItemIndex)
                                    }
                                  >
                                    {accordionState[dataIndex][keyItemIndex] ? (
                                      <ChevronDown className="text-primary" />
                                    ) : (
                                      <ChevronRight className="text-primary" />
                                    )}
                                    <span className="ml-2">
                                      {`${hasResults.title} - ${i18next.t(
                                        'common:unitDollar'
                                      )}${hasResults.totalCostDisplayFormat}`}
                                    </span>
                                  </Accordion.Toggle>

                                  <Accordion.Collapse
                                    className={`${
                                      accordionState[dataIndex][keyItemIndex]
                                        ? 'collapse show'
                                        : ''
                                    } m-0`}
                                  >
                                    <TableGrid
                                      showSearch={false}
                                      columnDef={columnDef[item]}
                                      data={hasResults.results}
                                    ></TableGrid>
                                  </Accordion.Collapse>
                                </React.Fragment>
                              )}
                            </Accordion>
                          );
                        }

                        return [];
                      })}
                    </Card.Body>
                  </Card>
                );
              })}

            {!hasDataState && (
              <div className="px-3 pb-3">{i18next.t('common:noDataFound')}</div>
            )}
          </div>
        </div>
      </Container>
    </>
  );
};

export default CompareOptimize;
