// Constants
import { workloadTypes } from '@maze/constants';
import { isEmpty } from '@maze/utilities/Object.utilities';
import i18next from 'i18next';

const CompareOptimizeItemsService = {
  loadItems: (optimizedData) => {
    if (Array.isArray(optimizedData) && optimizedData.length) {
      for (const data of optimizedData) {
        for (const dataKey in data) {
          const isValidWorkload = workloadTypes[dataKey] ?? false;
          const isFunction =
            typeof CompareOptimizeItemsService[
              `${dataKey}OptimizeDataTransformForUI`
            ] === 'function' ?? false;

          if (isValidWorkload && isFunction) {
            const results = CompareOptimizeItemsService[
              `${dataKey}OptimizeDataTransformForUI`
            ](data[dataKey]);

            if (!isEmpty(results)) {
              data[`${dataKey}Results`] = results;
            }
          }
        }
      }

      return optimizedData;
    }

    return [];
  },
  computeResourceOptimizeDataTransformForUI: (computeResourceData) => {
    const filteredOptimizedData = computeResourceData.filter(
      (cr) =>
        cr.vmInstance.selectedInstance && cr.vmInstance.selectedInstance?.cost
    );

    if (Array.isArray(filteredOptimizedData) && filteredOptimizedData.length) {
      const selectedVMInstance =
        filteredOptimizedData[0].vmInstance.selectedInstance;
      const selectedInstance = Array.isArray(
        selectedVMInstance.selectedInstance
      )
        ? selectedVMInstance[0]
        : selectedVMInstance;

      const totalCost =
        filteredOptimizedData.length > 1
          ? filteredOptimizedData.reduce(CompareOptimizeItemsService.reducer)
          : selectedInstance.cost * parseInt(selectedInstance.noOfServers);

      const tcoTerm = computeResourceData[0].tcoTerm;
      const hours =
        computeResourceData[0].payAsYouGo === 'true'
          ? parseInt(computeResourceData[0].usagePerMonth)
          : 730;
      const noOfServers = computeResourceData[0].noOfServers;

      return {
        title: i18next.t('computeCost'),
        totalCost,
        totalCostDisplayFormat: totalCost.toLocaleString('en-US'),
        results: filteredOptimizedData.map((result) => {
          const filteredSelectedInstance = result.vmInstance.selectedInstance;
          const selectedItem = Array.isArray(filteredSelectedInstance)
            ? filteredSelectedInstance[0]
            : filteredSelectedInstance;
          const cost = selectedItem.cost * parseInt(selectedItem.noOfServers);
          const avgCost = (cost / tcoTerm) * 12 * hours * noOfServers;
          return {
            ...result,
            cost,
            costDisplayFormat: cost.toLocaleString('en-US'),
            instanceName: selectedItem?.instanceName?.value,
            selectedNoOfServer: selectedItem.noOfServers,
            selectedMemory: selectedItem.memory,
            selectedVCpu: selectedItem.vCPU,
            workloadName: selectedItem.workloadId,
            avgCost: avgCost.toLocaleString('en-US'),
          };
        }),
      };
    }

    return {};
  },
  storageOptimizeDataTransformForUI: (storageData) => {
    const filteredOptimizedData = storageData.filter(
      (cr) =>
        cr.vmInstance.selectedInstance && cr.vmInstance.selectedInstance?.cost
    );

    if (Array.isArray(filteredOptimizedData) && filteredOptimizedData.length) {
      const selectedVMInstance =
        filteredOptimizedData[0].vmInstance.selectedInstance;
      const selectedInstance = Array.isArray(
        selectedVMInstance.selectedInstance
      )
        ? selectedVMInstance[0]
        : selectedVMInstance;

      const totalCost =
        filteredOptimizedData.length > 1
          ? filteredOptimizedData.reduce(
              CompareOptimizeItemsService.storageReducer
            )
          : selectedInstance.cost;

      return {
        title: i18next.t('storageCost'),
        totalCost,
        totalCostDisplayFormat: totalCost.toLocaleString('en-US'),
        results: filteredOptimizedData.map((result) => {
          const filteredSelectedInstance = result.vmInstance.selectedInstance;
          const selectedItem = Array.isArray(filteredSelectedInstance)
            ? filteredSelectedInstance[0]
            : filteredSelectedInstance;
          const cost = selectedItem.cost;
          return {
            ...result,
            cost,
            costDisplayFormat: cost.toLocaleString('en-US'),
            storageName: selectedItem?.storageName?.value,
            diskSpace: selectedItem.diskSpace,
            storageType: selectedItem.storageType,
            volumeType: selectedItem.volumeType,
            workloadName: selectedItem.workloadId,
          };
        }),
      };
    }

    return {};
  },

  storageReducer: (accumulator, currentValue) => {
    return (
      accumulator.vmInstance.selectedInstance.cost +
      currentValue.vmInstance.selectedInstance.cost
    );
  },

  reducer: (accumulator, currentValue) => {
    return (
      accumulator.vmInstance.selectedInstance.cost *
        parseInt(accumulator.vmInstance.selectedInstance.noOfServers) +
      currentValue.vmInstance.selectedInstance.cost *
        parseInt(currentValue.vmInstance.selectedInstance.noOfServers)
    );
  },

  loadChart: (optimizedData) => {
    const series = [];
    const hasComputeResults = optimizedData.some(
      (data) => data.computeResourceResults
    );
    const hasStorageResults = optimizedData.some((data) => data.storageResults);

    if (hasComputeResults) {
      const computeChartSeries = {
        name: i18next.t('computeResource'),
        type: 'bar',
        stack: 'compareCloud',
        data: optimizedData.map(
          (data) => data?.computeResourceResults?.totalCost
        ),
        label: {
          verticalAlign: 'top',
          show: true,
          formatter: '${c}',
        },
        barWidth: '30%',
      };

      series.push(computeChartSeries);
    }

    if (hasStorageResults) {
      const storageChartSeries = {
        name: i18next.t('storage'),
        type: 'bar',
        stack: 'compareCloud',
        data: optimizedData.map((data) => data?.storageResults?.totalCost),
        label: {
          verticalAlign: 'top',
          show: true,
          formatter: '${c}',
        },
        barWidth: '30%',
      };
      series.push(storageChartSeries);
    }

    const option = {
      color: ['#003f5c', '#ef5675', '#7a5195', '#ffa600'],
      tooltip: {},
      legend: {
        bottom: '0%',
      },
      xAxis: {
        data: optimizedData
          .filter(
            (data) => data?.computeResourceResults || data?.storageResults
          )
          .map((data) => i18next.t(data.cloudProvider.name)),
      },
      yAxis: {
        axisLabel: {
          formatter: '${value}',
        },
      },
      series,
    };

    return option;
  },
};

export default CompareOptimizeItemsService;
