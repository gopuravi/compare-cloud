import React from 'react';
import { Navbar, Nav, Button } from 'react-bootstrap';
import { Link, useHistory, useLocation } from 'react-router-dom';

// For Translation
import i18next from 'i18next';

import logo from '../../assets/logo.png';
import './Header.component.scss';

const Header = () => {
  let history = useHistory();
  const { pathname } = useLocation();

  const redirect = () => {
    history.push('/compare');
  };

  const isComparePage = (pathname) => {
    return pathname === '/compare';
  };

  return (
    <Navbar
      collapseOnSelect
      expand="lg"
      bg="white"
      variant="light"
      fixed="top"
      className="shadow-sm"
    >
      <div className={'container-fluid px-0'}>
        <Navbar.Brand as={Link} to="/">
          {/* {i18next.t('logoHeader').toUpperCase()} */}
          <img
            src={logo}
            className="d-inline-block header-logo-image"
            alt={i18next.t('logoHeader').toUpperCase()}
          />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            {/* <Nav.Link as={Link} to="/compare">
              {i18next.t('common:compare')}
            </Nav.Link> */}
            {/* <Nav.Link as={Link} to="/about">
              {i18next.t('common:about')}
            </Nav.Link>
            <Nav.Link as={Link} to="/contact">
              {i18next.t('common:contact')}
            </Nav.Link> */}
          </Nav>
          {!isComparePage(pathname) && (
            <Nav>
              <Nav.Item>
                <Button variant="primary" onClick={redirect}>
                  {i18next.t('common:getStarted')}
                </Button>
              </Nav.Item>
            </Nav>
          )}
        </Navbar.Collapse>
      </div>
    </Navbar>
  );
};

export default Header;
