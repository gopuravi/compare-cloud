// Language
import i18next from 'i18next';

const AlternateColumnDef = () => ({
  computeResource: [
    {
      headerName: i18next.t('instanceName'),
      field: 'instanceName',
      headerTooltip: i18next.t('instanceName'),
      tooltipField: 'instanceName',
      width: '40%',
    },
    {
      headerName: i18next.t('vCPU'),
      field: 'vCPU',
      headerTooltip: i18next.t('vCPU'),
      headerSecondaryText: i18next.t('common:onlyPerServer'),
      width: '20%',
    },
    {
      headerName: i18next.t('memory'),
      field: 'memory',
      headerTooltip: i18next.t('memory'),
      headerSecondaryText: i18next.t('common:perServer'),
      width: '20%',
    },
    {
      headerName: i18next.t('cost'),
      field: 'constDisplayFormat',
      headerTooltip: i18next.t('cost'),
      headerSecondaryText: i18next.t('common:dollarPerServer'),
      width: '20%',
    },
  ],
  storage: [
    {
      headerName: i18next.t('storageName'),
      field: 'storageName',
      headerTooltip: i18next.t('storageName'),
      tooltipField: 'storageName',
      width: '40%',
    },
    {
      headerName: i18next.t('diskSpace'),
      field: 'diskSpace',
      headerTooltip: i18next.t('diskSpace'),
      tooltipField: 'diskSpace',
      headerSecondaryText: i18next.t('common:unitGB'),
      width: '20%',
    },
    {
      headerName: i18next.t('cost'),
      field: 'constDisplayFormat',
      headerTooltip: i18next.t('cost'),
      width: '20%',
    },
    {
      headerName: i18next.t('storagePrice'),
      field: 'storagePrice',
      headerTooltip: i18next.t('storagePrice'),
      width: '20%',
    },
  ],
});

export default AlternateColumnDef;
