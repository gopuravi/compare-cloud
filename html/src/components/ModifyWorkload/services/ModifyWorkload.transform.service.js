import { sortByNumericValue, sort_by } from '@maze/utilities/Array.utilities';

const ModifyWorkloadTransformService = {
  transformComputeResource: (data) => {
    let transformedData = [];
    if (Array.isArray(data) && data.length) {
      const transData = data.map((item) => {
        const totalPrice = Math.round(item?.totalPrice);
        return {
          instanceNameValue: item?.serverAttributes?.instance?.instanceName,
          instanceName: {
            customEditor: false,
            value: item?.serverAttributes?.instance?.instanceName,
            type: 'select',
            customEditorConfig: {
              id: 'select_instance_name',
              className: 'dropdown__instanceName',
              options: [],
              value: {
                value: item?.serverAttributes?.instance?.instanceName,
                label: item?.serverAttributes?.instance?.instanceName,
              },
            },
          },
          cloudProvider: item.serverAttributes.cloudProvider.name,
          workloadId: ModifyWorkloadTransformService.getValuesFromWorkload(
            item?.computeResourceInputVO,
            'workloadId'
          )[0],
          operatingSystem:
            item?.serverAttributes?.operatingSystem?.operatingSystem,
          region: item?.serverAttributes?.region?.region,
          noOfServers: ModifyWorkloadTransformService.getValuesFromWorkload(
            item?.computeResourceInputVO,
            'noOfServers'
          )[0],
          vCPU: item?.serverAttributes?.virtualCpu,
          memory: item?.serverAttributes?.memory,
          cost: totalPrice,
          constDisplayFormat: totalPrice.toLocaleString('en-US'),
        };
      });
      transformedData = sortByNumericValue(transData, 'cost');
    }
    return transformedData;
  },

  getValuesFromWorkload: (item, key) => {
    return item?.workload?.find((workload) => key && workload.key === key)
      .value;
  },

  getDiskSpace: (item) => {
    const storageAttributes = item.storageAttributes;
    const selectedCloudProvider =
      storageAttributes.cloudProvider.name === 'Azure';
    const selectedStorageType =
      storageAttributes.storageType.storageType === 'Block';
    const diskType =
      selectedCloudProvider && selectedStorageType
        ? Array.isArray(item?.storagePricingOutputVOList) &&
          item?.storagePricingOutputVOList.length > 1
          ? item?.storagePricingOutputVOList
              .map((disk) => disk.diskSpace)
              .reduce(
                (previousValue, currentValue) => previousValue + currentValue
              )
          : Array.isArray(item?.storagePricingOutputVOList) &&
            item?.storagePricingOutputVOList.length === 1
          ? `${item?.storagePricingOutputVOList[0].diskSpace}###${item?.storagePricingOutputVOList[0].diskSpaceUom}`
          : ''
        : ModifyWorkloadTransformService.getValuesFromWorkload(
            item?.storageResourceInputVO,
            'diskSpace'
          )[0];
    return diskType;
  },

  transformStorage: (data) => {
    if (Array.isArray(data) && data.length) {
      const transData = data
        .filter(
          (item) =>
            Array.isArray(item?.storagePricingOutputVOList) &&
            item?.storagePricingOutputVOList.length
        )
        .map((item) => {
          const totalPrice = Math.round(item?.bestPrice);
          const storageAttributes = item.storageAttributes;
          let storagePrice =
            Array.isArray(item?.storagePricingOutputVOList) &&
            item?.storagePricingOutputVOList.length
              ? item?.storagePricingOutputVOList
                  .map((price) => price.storagePrice)
                  .reduce(
                    (previousValue, currentValue) =>
                      previousValue + currentValue
                  )
              : '';
          storagePrice = parseFloat(storagePrice).toFixed(4);

          let storageObj = {
            storageNameValue: storageAttributes.storage.storageName,
            storageName: {
              customEditor: false,
              value: storageAttributes.storage.storageName,
              type: 'select',
              customEditorConfig: {
                id: 'select_storage_name',
                className: 'dropdown__storage_name',
                options: [],
                value: {
                  value: storageAttributes.storage.storageName,
                  label: storageAttributes.storage.storageName,
                },
              },
            },
            cloudProvider: storageAttributes.cloudProvider.name,
            workloadId: ModifyWorkloadTransformService.getValuesFromWorkload(
              item?.storageResourceInputVO,
              'workloadId'
            )[0],
            region: storageAttributes?.region?.region,
            cost: totalPrice,
            constDisplayFormat: totalPrice.toLocaleString('en-US'),
            storageType: storageAttributes.storageType.storageType,
            volumeType: storageAttributes.volumeType.volumeType,
            storagePrice,
            diskSpace: ModifyWorkloadTransformService.getDiskSpace(item),
            defaultItem: storageAttributes.storage.default,
          };

          if (
            storageAttributes?.redundancy?.redundancy &&
            storageAttributes?.redundancy?.redundancy !== 'NA'
          ) {
            storageObj = {
              ...storageObj,
              redundancy: storageAttributes?.redundancy?.redundancy,
            };
          }

          return storageObj;
        });

      transData.sort(
        sort_by(
          {
            name: 'defaultItem',
            reverse: true,
          },
          {
            name: 'storagePrice',
            primer: parseFloat,
            reverse: false,
          }
        )
      );

      return transData;
    }

    return [];
  },

  filterComputeResource: (
    data,
    workloadName = null,
    cloudProvider = null,
    region = null,
    operatingSystem = null,
    redundancy = null
  ) => {
    if (Array.isArray(data)) {
      const cpName = cloudProvider.name ?? cloudProvider;
      const transData = data
        .filter((item) => item.workloadId === workloadName)
        .filter((item) => item.cloudProvider === cpName)
        .filter((item) => {
          const filterCondition = region?.value && item.region === region.value;
          return filterCondition ?? true;
        })
        .filter((item) => {
          const filterCondition =
            operatingSystem?.value &&
            item.operatingSystem === operatingSystem.value;
          return filterCondition ?? true;
        })
        .filter((item) => {
          const filterCondition =
            redundancy?.value && item.redundancy === redundancy.value;
          return filterCondition ?? true;
        });

      return transData;
    }

    return [];
  },
};

export default ModifyWorkloadTransformService;
