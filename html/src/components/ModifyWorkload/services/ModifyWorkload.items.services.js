import { errorMessage } from '@maze/utilities/Error.utilities';
import { toast } from 'react-toastify';
import { isEmpty } from '@maze/utilities/Object.utilities';
import CombinedResultsServices from '@maze/shared/services/combined-results/combined-results.services';

import i18next from 'i18next';

// Constants
import {
  Condition,
  CustomAttributes,
  SearchTypes,
  workloadTypes,
  Status,
} from '@maze/constants';

// services
import {
  postComputeResourceHTTPService,
  postStorageHTTPService,
} from '@maze/http-post-services/compute-resource-http.services';
import ModifyWorkloadTransformService from './ModifyWorkload.transform.service';

const ModifyWorkloadItemsService = {
  filteredCloudProviders: [],
  workloadData: [],
  cloudProviders: [],
  computeResourceData$: Promise,
  transformedComputeResourceData$: Promise,
  modifiedWorkloadData: [],

  setModifiedWorkloadData: (data) => {
    ModifyWorkloadItemsService.modifiedWorkloadData = data;
  },

  getModifiedWorkloadData: () =>
    ModifyWorkloadItemsService.modifiedWorkloadData,

  setCloudProviders: (cloudProviders) => {
    ModifyWorkloadItemsService.cloudProviders = cloudProviders;
  },

  getCloudProviders: () => ModifyWorkloadItemsService.cloudProviders,

  setFilteredCloudProviders: (cloudProviders) => {
    ModifyWorkloadItemsService.filteredCloudProviders = cloudProviders;
  },

  getFilteredCloudProviders: () =>
    ModifyWorkloadItemsService.filteredCloudProviders,

  setWorkloadData: (workloadData) => {
    ModifyWorkloadItemsService.workloadData = workloadData;
  },

  getWorkloadData: () => ModifyWorkloadItemsService.workloadData,

  setComputeResourceData: (data) => {
    ModifyWorkloadItemsService.computeResourceData$ = data;
  },

  getComputeResourceData$: () =>
    ModifyWorkloadItemsService.computeResourceData$,

  setTransformedData: (data) => {
    ModifyWorkloadItemsService.transformedComputeResourceData$ = data;
  },

  getTransformedData$: () =>
    ModifyWorkloadItemsService.transformedComputeResourceData$,

  getSelectedCloudProviders: () => {
    const workload = ModifyWorkloadItemsService.getWorkloadData();
    const cloudProviders = ModifyWorkloadItemsService.getCloudProviders();
    const selectedCloudProviders = cloudProviders
      .filter((cp) => workload[0].cloudProviders.includes(cp.cloudProviderId))
      .map((cp) => ({ name: cp.name, value: cp.cloudProviderId }));

    ModifyWorkloadItemsService.setFilteredCloudProviders(
      selectedCloudProviders
    );

    return selectedCloudProviders;
  },

  loadItems$: (payload) => {
    const promises = [];
    for (const payloadKey in payload) {
      const isValidWorkload = workloadTypes[payloadKey] ?? false;
      const isFunction =
        typeof ModifyWorkloadItemsService[`${payloadKey}Service`] ===
          'function' ?? false;

      if (isValidWorkload) {
        const hasPayload = payload[payloadKey]?.length ?? false;
        if (isFunction && hasPayload) {
          promises.push(
            ModifyWorkloadItemsService[`${payloadKey}Service`](
              payload[payloadKey]
            )
          );
        }
      }
    }
    const combinedResult$ = Promise.allSettled(promises)
      .then((values) => {
        const payloadKeys = Object.values(workloadTypes).filter((item) =>
          Object.keys(payload)
            .filter((pl) => payload[pl].length)
            .includes(item)
        );

        const responseData = payloadKeys.reduce((a, v, currentIndex) => {
          const responseValue =
            values[currentIndex]?.status === Status.fulfilled
              ? values[currentIndex]?.value
              : [];

          // Catch Error
          if (values[currentIndex]?.status === Status.rejected) {
            toast.error(`${errorMessage(values[currentIndex]?.reason)}`);
          }

          return { ...a, [v]: responseValue };
        }, {});

        const copyWorkloadResultData = JSON.parse(JSON.stringify(responseData));

        for (const resultKey in copyWorkloadResultData) {
          const functionKey =
            resultKey.charAt(0).toUpperCase() + resultKey.slice(1);
          const isFunction =
            typeof ModifyWorkloadTransformService[`transform${functionKey}`] ===
              'function' ?? false;
          if (isFunction) {
            copyWorkloadResultData[resultKey] = ModifyWorkloadTransformService[
              `transform${functionKey}`
            ](copyWorkloadResultData[resultKey]);
          }
        }

        return new Promise((resolve) => {
          setTimeout(() => {
            resolve(copyWorkloadResultData);
          }, 1);
        });
      })
      .catch((error) => toast.error(`${errorMessage(error)}`));

    ModifyWorkloadItemsService.setTransformedData(combinedResult$);
  },

  computeResourceService: (payload) => postComputeResourceHTTPService(payload),

  storageService: (payload) => postStorageHTTPService(payload),

  getItems$: () => {
    const transformedData$ =
      ModifyWorkloadItemsService.getTransformedComputeResourceData$();
    return transformedData$.then((transData) => {
      const items = JSON.parse(JSON.stringify(transData));
      const combinedResults = CombinedResultsServices.getCombinedResultData();
      const transformedItems =
        ModifyWorkloadItemsService.transformDataForUIWithFilterOptions(
          combinedResults,
          items
        );

      return new Promise((resolve) => {
        setTimeout(() => {
          resolve(transformedItems);
        }, 1);
      });
    });
  },

  transformDataForUIWithFilterOptions: (combinedResults, transData) => {
    const copyCombinedResults = JSON.parse(JSON.stringify(combinedResults));
    for (const combinedResult of copyCombinedResults) {
      for (const combinedResultKey in combinedResult) {
        const isValidWorkload = workloadTypes[combinedResultKey] ?? false;
        const isFunction =
          typeof ModifyWorkloadItemsService[
            `${combinedResultKey}DataForUIWithFilterOption`
          ] === 'function' ?? false;

        if (isValidWorkload && isFunction) {
          combinedResult[combinedResultKey] = ModifyWorkloadItemsService[
            `${combinedResultKey}DataForUIWithFilterOption`
          ](combinedResult, transData, combinedResultKey);
        }
      }
    }

    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(copyCombinedResults);
      }, 1);
    });
  },

  computeResourceDataForUIWithFilterOption: (result, transData, key) => {
    const data = transData[key];
    const computeResource = result[key].map((cr) => {
      let filteredData = [],
        endFilteredData = [],
        region = [],
        operatingSystem = [];
      let selectedRegion = {},
        selectedOperatingSystem = {},
        vmInstance = {};
      if (Array.isArray(data) && data.length) {
        filteredData = ModifyWorkloadTransformService.filterComputeResource(
          data,
          cr.workloadId,
          result.cloudProvider
        );

        region = [
          ...new Map(
            filteredData.map((item) => [
              item.region,
              {
                value: item.region,
                label: item.region,
              },
            ])
          ).values(),
        ];

        selectedRegion = region[0];
        const filteredOperatingSystem =
          ModifyWorkloadTransformService.filterComputeResource(
            filteredData,
            cr.workloadId,
            result.cloudProvider,
            selectedRegion
          );

        operatingSystem = [
          ...new Map(
            filteredOperatingSystem.map((item) => [
              item.operatingSystem,
              {
                value: item.operatingSystem,
                label: item.operatingSystem,
              },
            ])
          ).values(),
        ];
        selectedOperatingSystem = operatingSystem[0];
        endFilteredData = ModifyWorkloadTransformService.filterComputeResource(
          filteredOperatingSystem,
          cr.workloadId,
          result.cloudProvider,
          selectedRegion,
          selectedOperatingSystem
        );

        // Selected & Alternate VM Options
        const filterVMInstance = JSON.parse(JSON.stringify(endFilteredData));
        const selectedVMInstance = filterVMInstance.shift();
        const options = filterVMInstance.map((item) => ({
          label: item?.instanceName?.value,
          value: item?.instanceName?.value,
        }));
        vmInstance = {
          selectedInstance: {
            ...selectedVMInstance,
            instanceName: {
              ...selectedVMInstance?.instanceName,
              customEditor:
                Array.isArray(filterVMInstance) && filterVMInstance.length > 1
                  ? true
                  : false,
              customEditorConfig: {
                ...selectedVMInstance?.instanceName?.customEditorConfig,
                options,
              },
            },
          },
          alternateVMInstance: filterVMInstance,
        };
        // Selected & Alternate VM Options
      }

      const crData = {
        ...cr,
        filteredData,
        endFilteredData,
        vmInstance,
        region,
        selectedRegion,
        operatingSystem,
        selectedOperatingSystem,
        data,
      };

      return crData;
    });

    return computeResource;
  },

  storageDataForUIWithFilterOption: (result, transData, key) => {
    const data = transData[key];
    const storageDataForUI = result[key].map((storage) => {
      let filteredData = [],
        endFilteredData = [],
        region = [],
        redundancy = [];

      let selectedRegion = {},
        vmInstance = {},
        selectedRedundancy = {};
      if (Array.isArray(data) && data.length) {
        filteredData = ModifyWorkloadTransformService.filterComputeResource(
          data,
          storage.workloadId,
          result.cloudProvider
        );

        region = [
          ...new Map(
            filteredData.map((item) => [
              item.region,
              {
                value: item.region,
                label: item.region,
              },
            ])
          ).values(),
        ];

        selectedRegion = region[0];
        endFilteredData = ModifyWorkloadTransformService.filterComputeResource(
          filteredData,
          storage.workloadId,
          result.cloudProvider,
          selectedRegion
        );

        const hasRedundancy = endFilteredData.some((item) => item.redundancy);
        if (hasRedundancy) {
          redundancy = [
            ...new Map(
              endFilteredData.map((item) => [
                item.redundancy,
                {
                  value: item.redundancy,
                  label: item.redundancy,
                },
              ])
            ).values(),
          ];

          selectedRedundancy = redundancy[0];
          endFilteredData =
            ModifyWorkloadTransformService.filterComputeResource(
              filteredData,
              storage.workloadId,
              result.cloudProvider,
              selectedRegion,
              null,
              selectedRedundancy
            );
        }

        // Selected & Alternate VM Options
        const filteredStorage = JSON.parse(JSON.stringify(endFilteredData));
        if (Array.isArray(filteredStorage) && filteredStorage.length === 1) {
          vmInstance = {
            selectedInstance: isEmpty(filteredStorage[0])
              ? {}
              : {
                  ...filteredStorage[0],
                },
            alternateVMInstance: [],
          };
        } else {
          const selectedStorageItem = filteredStorage.find(
            (item) => item.defaultItem
          );
          const options = filteredStorage
            .filter((item) => !item.defaultItem)
            .map((item) => ({
              label: item?.storageName?.value,
              value: item?.storageName?.value,
            }));

          const alternateVMInstance = filteredStorage.filter(
            (item) => !item.defaultItem
          );
          vmInstance = {
            selectedInstance: isEmpty(selectedStorageItem)
              ? {}
              : {
                  ...selectedStorageItem,
                  storageName: {
                    ...selectedStorageItem?.storageName,
                    customEditor: true,
                    customEditorConfig: {
                      ...selectedStorageItem?.storageName?.customEditorConfig,
                      options,
                    },
                  },
                },
            alternateVMInstance,
          };
        }
        // Selected & Alternate VM Options
      }

      const storageData = {
        ...storage,
        region,
        redundancy,
        filteredData,
        endFilteredData,
        vmInstance,
      };

      return storageData;
    });

    return storageDataForUI;
  },

  updateModifiedWorkloadData: (param) => {
    const workloadData = ModifyWorkloadItemsService.getModifiedWorkloadData();
    workloadData.forEach((wl) => {
      if (
        wl.cloudProvider.name === param.filterItems.leftSelection.selectedItem
      ) {
        // TODO: find alternate looping to update data. it may cause performance issue.
        wl[param.filterItems.leftSelection.selectedKeyValue].forEach(
          (cr, index) => {
            if (cr.workloadId === param.filterItems.workloadId) {
              const filteredItem =
                wl[param.filterItems.leftSelection.selectedKeyValue][index];

              const isFunction =
                typeof ModifyWorkloadItemsService[
                  `${param.filterItems.leftSelection.selectedKeyValue}UpdateData`
                ] === 'function' ?? false;

              if (isFunction) {
                const crData = ModifyWorkloadItemsService[
                  `${param.filterItems.leftSelection.selectedKeyValue}UpdateData`
                ](filteredItem, param);

                wl[param.filterItems.leftSelection.selectedKeyValue][index] = {
                  ...wl[param.filterItems.leftSelection.selectedKeyValue][
                    index
                  ],
                  ...param.optionToBeUpdate,
                  ...crData,
                };
              }
            }
          }
        );
      }
    });

    ModifyWorkloadItemsService.setModifiedWorkloadData(workloadData);
  },

  computeResourceUpdateData: (filteredItem, param) => {
    const filteredData = filteredItem.filteredData;
    const selectedRegionItem =
      param.optionToBeUpdate.selectedRegion ?? filteredItem.selectedRegion;
    const filteredOperatingSystem =
      ModifyWorkloadTransformService.filterComputeResource(
        filteredData,
        param.filterItems.workloadId,
        param.filterItems.leftSelection.selectedItem,
        selectedRegionItem
      );

    const operatingSystem = [
      ...new Map(
        filteredOperatingSystem.map((item) => [
          item.operatingSystem,
          {
            value: item.operatingSystem,
            label: item.operatingSystem,
          },
        ])
      ).values(),
    ];
    const selectedOperatingSystem =
      param.optionToBeUpdate.selectedOperatingSystem ?? operatingSystem[0];
    const endFilteredData =
      ModifyWorkloadTransformService.filterComputeResource(
        filteredOperatingSystem,
        param.filterItems.workloadId,
        param.filterItems.leftSelection.selectedItem,
        selectedRegionItem,
        selectedOperatingSystem
      );
    const filterVMInstance = JSON.parse(JSON.stringify(endFilteredData));
    const selectedVMInstance = filterVMInstance.shift();
    const options = filterVMInstance.map((item) => ({
      label: item?.instanceName?.value,
      value: item?.instanceName?.value,
    }));

    const crData = {
      endFilteredData,
      vmInstance: {
        selectedInstance: {
          ...selectedVMInstance,
          instanceName: {
            ...selectedVMInstance?.instanceName,
            customEditor:
              Array.isArray(filterVMInstance) && filterVMInstance.length > 1
                ? true
                : false,
            customEditorConfig: {
              ...selectedVMInstance?.instanceName?.customEditorConfig,
              options,
            },
          },
        },
        alternateVMInstance: filterVMInstance,
      },
      operatingSystem: param.optionToBeUpdate.selectedRegion
        ? operatingSystem
        : filteredItem.operatingSystem,
      selectedOperatingSystem,
    };

    return crData;
  },

  storageUpdateData: (filteredItem, param) => {
    const filteredData = filteredItem.filteredData;
    const selectedRegionItem =
      param.optionToBeUpdate.selectedRegion ?? filteredItem.selectedRegion;
    let endFilteredData = ModifyWorkloadTransformService.filterComputeResource(
      filteredData,
      param.filterItems.workloadId,
      param.filterItems.leftSelection.selectedItem,
      selectedRegionItem
    );

    let redundancy = [];
    let selectedRedundancy = {};
    const hasRedundancy = endFilteredData.some((item) => item.redundancy);
    if (hasRedundancy) {
      redundancy = [
        ...new Map(
          endFilteredData.map((item) => [
            item.redundancy,
            {
              value: item.redundancy,
              label: item.redundancy,
            },
          ])
        ).values(),
      ];

      selectedRedundancy =
        param.optionToBeUpdate.selectedRedundancy ?? redundancy[0];
      endFilteredData = ModifyWorkloadTransformService.filterComputeResource(
        filteredData,
        param.filterItems.workloadId,
        param.filterItems.leftSelection.selectedItem,
        selectedRegionItem,
        null,
        selectedRedundancy
      );
    }

    const filterVMInstance = JSON.parse(JSON.stringify(endFilteredData));
    let crData = {
      endFilteredData,
      redundancy: param.optionToBeUpdate.selectedRegion
        ? redundancy
        : filteredItem.redundancy,
      selectedRedundancy,
    };
    if (Array.isArray(filterVMInstance) && filterVMInstance.length === 1) {
      crData = {
        ...crData,
        vmInstance: {
          selectedInstance: isEmpty(filterVMInstance[0])
            ? {}
            : {
                ...filterVMInstance[0],
              },
          alternateVMInstance: [],
        },
      };
    } else {
      const selectedStorageItem = filterVMInstance.find(
        (item) => item.defaultItem
      );
      const options = filterVMInstance
        .filter((item) => !item.defaultItem)
        .map((item) => ({
          label: item?.storageName?.value,
          value: item?.storageName?.value,
        }));
      const alternateVMInstance = filterVMInstance.filter(
        (item) => !item.defaultItem
      );

      crData = {
        ...crData,
        vmInstance: {
          selectedInstance: isEmpty(selectedStorageItem)
            ? {}
            : {
                ...selectedStorageItem,
                storageName: {
                  ...selectedStorageItem?.storageName,
                  customEditor: true,
                  customEditorConfig: {
                    ...selectedStorageItem?.storageName?.customEditorConfig,
                    options,
                  },
                },
              },
          alternateVMInstance,
        },
      };
    }

    return crData;
  },

  updateSelectedVMInstances: (param, workloadName, vmInstance) => {
    // TODO: find alternate looping to update data. it may cause performance issue.
    const workloadData = ModifyWorkloadItemsService.getModifiedWorkloadData();
    workloadData.forEach((wl) => {
      if (wl.cloudProvider.name === param.selectedItem) {
        wl[param.selectedKeyValue].forEach((cr, index) => {
          if (cr.workloadId === workloadName) {
            wl[param.selectedKeyValue][index] = {
              ...wl[param.selectedKeyValue][index],
              vmInstance: {
                alternateVMInstance: vmInstance.vmInstanceData,
                selectedInstance: Array.isArray(vmInstance.selectedVMData)
                  ? vmInstance.selectedVMData[0]
                  : vmInstance.selectedVMData,
              },
            };
          }
        });
      }
    });

    ModifyWorkloadItemsService.setModifiedWorkloadData(workloadData);
  },

  getTransformedComputeResourceData$: () => {
    const transformedComputeResourceData$ =
      ModifyWorkloadItemsService.getTransformedData$();
    return transformedComputeResourceData$;
  },

  getMenuItems: () => {
    const cloudProviders =
      ModifyWorkloadItemsService.getFilteredCloudProviders();
    const workload = ModifyWorkloadItemsService.getWorkloadData();
    const payload = workload[0];
    const menuNames = Object.values(workloadTypes)
      .filter((item) =>
        Object.keys(payload)
          .filter((pl) => payload[pl].length)
          .includes(item)
      )
      .map((pk) => {
        return {
          name: i18next.t(pk),
          className: 'child',
          keyValue: pk,
        };
      });

    const transformedMenuItems = cloudProviders.map((item) => {
      return {
        displayName: i18next.t(item.name),
        name: item.name,
        value: item.value,
        className: 'parent',
        children: [...menuNames],
      };
    });

    return transformedMenuItems;
  },

  transformComputeResourceDataForBackend: () => {
    const workload = ModifyWorkloadItemsService.getWorkloadData();
    const workloadData = workload[0];
    for (const wl in workloadData) {
      const isValidWorkload = workloadTypes[wl] === wl ?? false;
      if (
        isValidWorkload &&
        Array.isArray(workloadData[wl]) &&
        workloadData[wl].length
      ) {
        workloadData[wl] = workloadData[wl].map((wlItems) =>
          ModifyWorkloadItemsService[wl](workloadData, wlItems)
        );
      }
    }

    const transformDefinedWorkload = workload[0];

    return transformDefinedWorkload;
  },

  computeResource(wl, data) {
    const computeResource = {
      ...data,
      tcoTerm: wl.tcoTerm.toString(),
      cloudProvider: ModifyWorkloadItemsService.getFilteredCloudProviders().map(
        (cp) => cp.name
      ),
      leaseTerm: data.payAsYouGo
        ? CustomAttributes.ON_DEMAND
        : CustomAttributes.RESERVED,
      usagePerMonth: data.payAsYouGo ? data.usagePerMonth : 730,
      payAsYouGo: data.payAsYouGo.toString(),
    };
    return computeResource;
  },

  storage: (wl, data) => {
    return {
      ...data,
      tcoTerm: wl.tcoTerm.toString(),
      cloudProvider: ModifyWorkloadItemsService.getFilteredCloudProviders().map(
        (cp) => cp.name
      ),
    };
  },

  getComputeResourcePayload: (transformedData) => {
    const copyTransformedData = JSON.parse(JSON.stringify(transformedData));

    for (const transKey in copyTransformedData) {
      const isValidWorkload = workloadTypes[transKey] ?? false;
      const isFunction =
        typeof ModifyWorkloadItemsService[`${transKey}TransformData`] ===
          'function' ?? false;
      if (isValidWorkload) {
        if (isFunction) {
          copyTransformedData[transKey] = copyTransformedData[transKey].map(
            (item) =>
              ModifyWorkloadItemsService[`${transKey}TransformData`](item)
          );
        } else {
          copyTransformedData[transKey] = [];
        }
      }
    }

    return copyTransformedData;
  },

  computeResourceTransformData(twl) {
    const workloadKeys = Object.keys(twl).filter(
      (key) => !['payAsYouGo'].includes(key)
    );

    const workloadCombinedPayload = workloadKeys.map((wlKey) => {
      const value = Array.isArray(twl[wlKey]) ? twl[wlKey] : [twl[wlKey]];
      const searchCondition = ModifyWorkloadItemsService.isMemoryOptimized(twl)
        ? Condition.GREATER_THAN
        : Condition.BETWEEN;
      const searchKeyword = ModifyWorkloadItemsService.getSearchKeyWord(
        wlKey,
        searchCondition
      );

      const workloadPayload = {
        key: wlKey,
        value,
        search: searchKeyword
          ? searchKeyword
          : value.length > 1
          ? Condition.IN
          : Condition.EQUALITY,
      };

      const type = ModifyWorkloadItemsService.getType(wlKey);
      let updatedValuesAsString = [];
      if (type) {
        if (ModifyWorkloadItemsService.isMemoryOptimized(twl)) {
          updatedValuesAsString = [value.toString()];
        } else {
          const updatedValues = ModifyWorkloadItemsService.updateValues(value);
          updatedValuesAsString = updatedValues.map((value) =>
            value.toString()
          );
        }
      }
      return type
        ? { ...workloadPayload, type, value: updatedValuesAsString }
        : { ...workloadPayload };
    });

    return {
      workload: [
        ...workloadCombinedPayload,
        ModifyWorkloadItemsService.getPaymentOption(),
      ],
    };
  },

  storageTransformData: (twl) => {
    const workloadKeys = Object.keys(twl).filter(
      (key) => !['payAsYouGo'].includes(key)
    );

    const workloadCombinedPayload = workloadKeys.map((wlKey) => {
      const value = Array.isArray(twl[wlKey]) ? twl[wlKey] : [twl[wlKey]];
      const searchKeyword = ModifyWorkloadItemsService.getSearchKeyWord(wlKey);

      const workloadPayload = {
        key: wlKey,
        value,
        search: searchKeyword
          ? searchKeyword
          : value.length > 1
          ? Condition.IN
          : Condition.EQUALITY,
      };

      const type = ModifyWorkloadItemsService.getType(wlKey);
      let updatedValuesAsString = [];
      if (type) {
        const updatedValues = ModifyWorkloadItemsService.updateValues(value);
        updatedValuesAsString = updatedValues.map((value) => value.toString());
      }
      return type
        ? { ...workloadPayload, type, value: updatedValuesAsString }
        : { ...workloadPayload };
    });

    return {
      workload: [...workloadCombinedPayload],
    };
  },

  getPaymentOption() {
    return { key: 'paymentOption', value: ['No Upfront'], search: 'EQUALITY' };
  },

  updateValues(value) {
    let updatedValue = [];
    for (let i = 0; i < value.length + 1; i++) {
      if (i - 1 > -1) {
        updatedValue[i] = parseInt(value[i - 1]) * i * (i + 2);
      } else {
        updatedValue[i] = value[i];
      }
    }

    return updatedValue;
  },

  getType: (type) => {
    const searchType = {
      virtualCpu: SearchTypes.floatArr,
      memory: SearchTypes.floatArr,
    };

    return searchType[type] ?? false;
  },

  getSearchKeyWord: (searchInput, condition = Condition.BETWEEN) => {
    const searchKey = {
      virtualCpu: condition,
      memory: condition,
    };
    return searchKey[searchInput] ?? false;
  },

  isMemoryOptimized(twl) {
    return (
      twl &&
      twl['instanceFamily'].toLowerCase() === 'Memory Optimized'.toLowerCase()
    );
  },
};

export default ModifyWorkloadItemsService;
