import React, { useState, useEffect, useRef } from 'react';
import { Row, Col, Form } from 'react-bootstrap';
import Loader from 'react-loader';
import { ChevronRight, ChevronLeft } from 'react-bootstrap-icons';

// Services
import ModifyWorkloadItemsService from './services/ModifyWorkload.items.services';
import { isEmpty } from '@maze/utilities/Object.utilities';

// Components
import ListItems from '@maze/shared-components/ListItems/ListItems.component';
import SectionWorkload from './SectionWorkload/SectionWorkload.component';
import SectionVMInstances from './SectionVMInstances/SectionVMInstances.component';

// SCSS
import './ModifyWorkload.component.scss';

const ModifyWorkload = (props) => {
  const { cloudProviders, definedData, reloadData } = props;
  const vmInstanceMap = new Map();
  const modifyWorkloadFormRef = useRef();

  const [stateSelectedVMInstanceMap, setStateSelectedVMInstanceMap] =
    useState(vmInstanceMap);
  const [stateCloudProviders, setStateCloudProviders] = useState([]);
  const [stateMenuItems, setStateMenuItems] = useState([]);
  const [stateWorkload, setStateWorkload] = useState([]);
  const [stateFilteredData, setStateFilteredData] = useState([]);
  const [stateSelectedLeftSection, setStateSelectedLeftSection] =
    useState(null);
  const [stateSelectedWorkloadName, setStateSelectedWorkloadName] =
    useState(null);
  const [stateIsLoading, setStateIsLoading] = useState(false);
  const [stateSelectedRegion, setStateSelectedRegion] = useState(null);
  const [stateSelectedOperatingSystem, setStateSelectedOperatingSystem] =
    useState(null);
  const [stateSelectedInstance, setStateSelectedInstance] = useState({});
  const [stateSelectedRedundancy, setStateSelectedRedundancy] = useState(null);

  const leftPanelStorage = localStorage.getItem('showLeftPanel') ?? 'true';
  const [stateShowLeftPanel, setStateShowLeftPanel] =
    useState(leftPanelStorage);

  useEffect(() => {
    let unmounted = false;
    const { workload } = definedData;

    // Setter methods
    ModifyWorkloadItemsService.setCloudProviders(cloudProviders);
    ModifyWorkloadItemsService.setWorkloadData(workload);

    const selectedCloudProviders =
      ModifyWorkloadItemsService.getSelectedCloudProviders();
    setStateCloudProviders(cloudProviders);
    setStateWorkload(workload);

    if (selectedCloudProviders.length > 0) {
      const menuItems = ModifyWorkloadItemsService.getMenuItems();
      setStateSelectedLeftSection({
        selectedItem: menuItems[0].name,
        selectedSection: menuItems[0].children[0].name,
        selectedKeyValue: menuItems[0].children[0].keyValue,
      });
      setStateMenuItems(menuItems);
    }

    if (!unmounted) {
      const transformedComputeResourceData =
        ModifyWorkloadItemsService.transformComputeResourceDataForBackend();
      const computeResourcePayload =
        ModifyWorkloadItemsService.getComputeResourcePayload(
          transformedComputeResourceData
        );

      /**
       * Load compute resource data
       */
      if (reloadData) {
        ModifyWorkloadItemsService.setModifiedWorkloadData([]);
        ModifyWorkloadItemsService.loadItems$(computeResourcePayload);
        const items$ = ModifyWorkloadItemsService.getItems$();
        items$
          .then((items) => {
            ModifyWorkloadItemsService.setModifiedWorkloadData(items);
          })
          .finally(() => setStateIsLoading(true));
      }
    }

    return () => {
      unmounted = true;
    };
  }, [cloudProviders, definedData, reloadData]);

  const onSelectMenuItem = (selectedItem) => {
    setStateSelectedLeftSection(selectedItem);
  };

  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const prevFilterState = usePrevious({
    stateSelectedWorkloadName,
    stateSelectedLeftSection,
    stateSelectedRegion,
    stateSelectedOperatingSystem,
    stateSelectedInstance,
    stateSelectedRedundancy,
  });

  useEffect(() => {
    let unmounted = false;

    if (!unmounted) {
      if (
        prevFilterState?.stateSelectedWorkloadName !==
          stateSelectedWorkloadName ||
        prevFilterState?.stateSelectedLeftSection?.selectedItem !==
          stateSelectedLeftSection?.selectedItem ||
        prevFilterState?.stateSelectedRegion?.value !==
          stateSelectedRegion?.value ||
        prevFilterState?.stateSelectedOperatingSystem?.value !==
          stateSelectedOperatingSystem?.value ||
        prevFilterState?.stateSelectedInstance?.selectedItem?.value !==
          stateSelectedInstance?.selectedItem?.value ||
        prevFilterState?.stateSelectedRedundancy?.value !==
          stateSelectedRedundancy?.value
      ) {
        vmInstanceMap.clear();
        stateSelectedVMInstanceMap.clear();
        const items = ModifyWorkloadItemsService.getModifiedWorkloadData();
        if (items.length) {
          setStateIsLoading(true);
          const filteredItem = items.filter(
            (item) =>
              item.cloudProvider.name === stateSelectedLeftSection?.selectedItem
          );

          if (filteredItem.length) {
            setStateFilteredData(filteredItem);
            const filteredData =
              filteredItem[0][stateSelectedLeftSection?.selectedKeyValue] ??
              false;
            if (filteredData) {
              if (stateSelectedWorkloadName) {
                const selectedVMInstance = filteredData.find(
                  (item) => item.workloadId === stateSelectedWorkloadName
                );
                if (!isEmpty(selectedVMInstance)) {
                  items.forEach((item) => {
                    item[stateSelectedLeftSection.selectedKeyValue].forEach(
                      (cr) => {
                        const keyName = `${item.cloudProvider.name}_${stateSelectedLeftSection.selectedKeyValue}_${cr.workloadId}_selectedInstance`;
                        vmInstanceMap.set(keyName, cr);
                      }
                    );
                  });

                  setStateSelectedVMInstanceMap(vmInstanceMap);
                }
              }
            }
          }
        }
      }
    }

    return () => {
      unmounted = true;
    };
  }, [
    stateSelectedWorkloadName,
    stateSelectedLeftSection,
    stateSelectedRegion,
    stateSelectedOperatingSystem,
    stateSelectedInstance,
    stateSelectedRedundancy,
    prevFilterState?.stateSelectedWorkloadName,
    prevFilterState?.stateSelectedLeftSection,
    prevFilterState?.stateSelectedRegion,
    prevFilterState?.stateSelectedOperatingSystem,
    prevFilterState?.stateSelectedInstance,
    prevFilterState?.stateSelectedRedundancy,
  ]);

  const onChangeWorkload = (workload) => {
    setStateSelectedWorkloadName(workload);
  };

  const onChangeRegion = (region) => {
    setStateSelectedRegion(region);
  };

  const onChangeOperatingSystem = (operatingSystem) => {
    setStateSelectedOperatingSystem(operatingSystem);
  };

  const onUpdateSelectedInstance = (selectedInstance) => {
    setStateSelectedInstance(selectedInstance);
  };

  const onChangeRedundancy = (selectedRedundancy) => {
    setStateSelectedRedundancy(selectedRedundancy);
  };

  const onControlPanels = () => {
    const isLeftPanelEnabled = stateShowLeftPanel === 'true' ? 'false' : 'true';
    localStorage.setItem('showLeftPanel', isLeftPanelEnabled);
    setStateShowLeftPanel(isLeftPanelEnabled);
  };

  return (
    <>
      <div className={'modify-workload-outer-box'}>
        <Row className={'m-0 h-100'}>
          <Col
            xs={3}
            className={`p-0 m-0 light-green-200 ${
              stateShowLeftPanel === 'true' ? 'd-block' : 'd-none'
            }`}
          >
            {stateIsLoading && (
              <ListItems
                items={stateMenuItems}
                {...{ onSelectMenuItem }}
              ></ListItems>
            )}
          </Col>
          <Col
            xs={`${stateShowLeftPanel === 'true' ? 9 : 12}`}
            className={'p-0'}
          >
            <Loader loaded={stateIsLoading} className="spinner">
              <div
                className={'position-absolute leftPaneControl cursor-pointer'}
                onClick={() => onControlPanels()}
              >
                {stateShowLeftPanel === 'true' ? (
                  <ChevronLeft />
                ) : (
                  <ChevronRight />
                )}
              </div>
              <Form ref={modifyWorkloadFormRef}>
                <SectionWorkload
                  filteredData={stateFilteredData}
                  data={stateWorkload}
                  cloudProviders={stateCloudProviders}
                  selectedLeftSection={stateSelectedLeftSection}
                  onChangeWorkload={onChangeWorkload}
                  onChangeRegion={onChangeRegion}
                  onChangeOperatingSystem={onChangeOperatingSystem}
                  onChangeRedundancy={onChangeRedundancy}
                ></SectionWorkload>
                {!isEmpty(
                  stateSelectedVMInstanceMap.get(
                    `${stateSelectedLeftSection?.selectedItem}_${stateSelectedLeftSection?.selectedKeyValue}_${stateSelectedWorkloadName}_selectedInstance`
                  )
                ) && (
                  <SectionVMInstances
                    selectedWorkloadName={stateSelectedWorkloadName}
                    selectedLeftSection={stateSelectedLeftSection}
                    data={stateSelectedVMInstanceMap.get(
                      `${stateSelectedLeftSection?.selectedItem}_${stateSelectedLeftSection?.selectedKeyValue}_${stateSelectedWorkloadName}_selectedInstance`
                    )}
                    onUpdateSelectedInstance={onUpdateSelectedInstance}
                  ></SectionVMInstances>
                )}
              </Form>
            </Loader>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default ModifyWorkload;
