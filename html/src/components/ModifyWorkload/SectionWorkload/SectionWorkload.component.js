import React, { useEffect, useState } from 'react';
import { Row, Col, Form } from 'react-bootstrap';

// For Translation
import i18next from 'i18next';

// SCSS
import './SectionWorkload.component.scss';

import ModifyWorkloadItemsService from './../services/ModifyWorkload.items.services';

const SectionWorkload = (props) => {
  const map1 = new Map();
  const { data, onChangeWorkload, filteredData, selectedLeftSection } = props;

  const [stateSelectedData, setStateSelectedData] = useState({});
  const [hasData, setHasData] = useState(false);
  const [stateWorkloadItems, setStateWorkloadItems] = useState([]);
  const [stateDefaultWorkloadName, setStateDefaultWorkloadName] = useState('');
  const [stateDynamicValues, setStateDynamicValues] = useState(map1);

  useEffect(() => {
    if (filteredData.length) {
      map1.clear();
      stateDynamicValues.clear();
      setHasData(false);
      const validData =
        filteredData[0][selectedLeftSection.selectedKeyValue] ?? false;
      if (validData) {
        const data = validData;
        if (data.length) {
          setHasData(true);
          const selectedData = data.find(
            (item) => item.workloadId === stateDefaultWorkloadName
          );
          setStateSelectedData(selectedData);
          const dynamicRegion = `${selectedLeftSection?.selectedItem}_${selectedLeftSection?.selectedKeyValue}_${selectedData?.workloadId}_region`;
          const dynamicOperatingSystem = `${selectedLeftSection?.selectedItem}_${selectedLeftSection?.selectedKeyValue}_${selectedData?.workloadId}_operatingSystem`;

          map1.set(dynamicRegion, selectedData?.selectedRegion);
          map1.set(
            dynamicOperatingSystem,
            selectedData?.selectedOperatingSystem
          );

          setStateDynamicValues(map1);
        }
      }
    }
  }, [filteredData, selectedLeftSection, stateDefaultWorkloadName]);

  useEffect(() => {
    const workload = data[0];
    if (workload && selectedLeftSection) {
      setStateDefaultWorkloadName('');
      const selectedWorkloadName = workload[
        selectedLeftSection.selectedKeyValue
      ].length
        ? workload[selectedLeftSection.selectedKeyValue][0].workloadId
        : ''; // For default option

      setStateDefaultWorkloadName(selectedWorkloadName);
      onChangeWorkload(selectedWorkloadName);
      const workloadNames = getWorkloadNames(workload);
      setStateWorkloadItems(workloadNames);
    }
  }, [data, selectedLeftSection]);

  const onWorkloadChange = (event) => {
    const selectedWorkloadName = event.target.value;
    setStateDefaultWorkloadName(selectedWorkloadName);
    onChangeWorkload(selectedWorkloadName);
  };

  const onRegionChange = (event) => {
    const selectedRegion = {
      value: event.target.value,
      label: event.target.value,
    };

    const param = {
      filterItems: {
        workloadId: stateDefaultWorkloadName,
        leftSelection: selectedLeftSection,
      },
      optionToBeUpdate: {
        selectedRegion: selectedRegion,
      },
    };

    ModifyWorkloadItemsService.updateModifiedWorkloadData(param);

    props.onChangeRegion(selectedRegion);
  };

  const onRedundancyChange = (event) => {
    const selectedRedundancy = {
      value: event.target.value,
      label: event.target.value,
    };

    const param = {
      filterItems: {
        workloadId: stateDefaultWorkloadName,
        leftSelection: selectedLeftSection,
      },
      optionToBeUpdate: {
        selectedRedundancy,
      },
    };

    ModifyWorkloadItemsService.updateModifiedWorkloadData(param);

    props.onChangeRedundancy(selectedRedundancy);
  };

  const onOperatingSystemChange = (event) => {
    const selectedOS = {
      value: event.target.value,
      label: event.target.value,
    };

    const param = {
      filterItems: {
        workloadId: stateDefaultWorkloadName,
        leftSelection: selectedLeftSection,
      },
      optionToBeUpdate: {
        selectedOperatingSystem: selectedOS,
      },
    };

    ModifyWorkloadItemsService.updateModifiedWorkloadData(param);

    props.onChangeOperatingSystem(selectedOS);
  };

  const getWorkloadNames = (workloadData) => {
    const workload = workloadData[selectedLeftSection.selectedKeyValue];

    return workload.map((cr) => ({
      label: cr.workloadId,
      value: cr.workloadId,
    }));
  };

  const computeResourceWorkload = () => {
    return (
      <>
        <Row className={'workload-details'}>
          <Col md={6}>
            <div>
              <label className={'mb-0 text-black-50'}>
                {i18next.t('operatingSystemFamily')}
              </label>
              <span className={'w-150-px'}>
                {stateSelectedData.operatingSystemFamily}
              </span>
            </div>
          </Col>
          <Col md={6}>
            <Form.Group
              controlId={`${selectedLeftSection.selectedItem}_${stateDefaultWorkloadName.value}_${selectedLeftSection.selectedKeyValue}_formOperatingSystem`}
              className={'workload-details'}
            >
              <Form.Label className={'text-black-50'}>
                {i18next.t('operatingSystem')}
              </Form.Label>
              <Form.Control
                size={'sm'}
                as="select"
                custom
                className={'d-inline w-auto'}
                value={
                  stateDynamicValues?.get(
                    `${selectedLeftSection.selectedItem}_${selectedLeftSection.selectedKeyValue}_${stateSelectedData.workloadId}_operatingSystem`
                  )?.value
                }
                onChange={onOperatingSystemChange}
              >
                {stateSelectedData?.operatingSystem &&
                  stateSelectedData.operatingSystem.map((wn, wnIndex) => {
                    return <option key={wnIndex}>{wn.label}</option>;
                  })}

                {stateSelectedData?.operatingSystem?.length === 0 && (
                  <option key={'noData'}>
                    {i18next.t('common:noDataFound')}
                  </option>
                )}
              </Form.Control>
            </Form.Group>
          </Col>
        </Row>

        <Row className={'workload-details mt-3'}>
          <Col md={6}>
            <div>
              <label className={'text-black-50'}>
                {i18next.t('instanceType')}
              </label>
              <span className={'w-150-px'}>
                {stateSelectedData.instanceType}
              </span>
            </div>
          </Col>
          <Col md={6}>
            <div>
              <label className={'text-black-50'}>
                {i18next.t('instanceFamily')}
              </label>
              <span className={'w-150-px'}>
                {stateSelectedData.instanceFamily}
              </span>
            </div>
          </Col>
        </Row>

        <Row className={'workload-details mt-3'}>
          <Col md={6}>
            <div>
              <label className={'text-black-50'}>
                {i18next.t('noOfServers')}
              </label>
              <span className={'w-150-px'}>
                {stateSelectedData.noOfServers}
              </span>
            </div>
          </Col>
          <Col md={6}>
            <div>
              <label className={'text-black-50'}>
                {i18next.t('vCPUsPerServer')}
              </label>
              <span className={'w-150-px'}>{stateSelectedData.virtualCpu}</span>
            </div>
          </Col>
        </Row>

        <Row className={'workload-details mt-3'}>
          <Col md={6}>
            <div>
              <label className={'text-black-50'}>
                {i18next.t('memoryPerServer')}
              </label>
              <span className={'w-150-px'}>{stateSelectedData.memory}</span>
            </div>
          </Col>
          <Col md={6}>
            <div>
              <label className={'mb-0 text-black-50'}>
                {i18next.t('pricingOption')}
              </label>
              <span className={'w-150-px'}>
                {stateSelectedData.payAsYouGo ? 'On Demand' : 'Reserved'}
              </span>
            </div>
          </Col>
        </Row>
      </>
    );
  };

  const storageWorkload = () => {
    return (
      <>
        <Row className={'workload-details'}>
          <Col md={6}>
            <div>
              <label className={'mb-0 text-black-50'}>
                {i18next.t('storageType')}
              </label>
              <span className={'w-150-px'}>
                {stateSelectedData.storageType}
              </span>
            </div>
          </Col>
          <Col md={6}>
            <div>
              <label className={'text-black-50'}>{i18next.t('capacity')}</label>
              <span className={'w-150-px'}>
                {stateSelectedData.diskSpace}
                <span className="ml-1 elem--text_round_bracket_small_size text-muted">
                  {stateSelectedData.diskSpaceUnit}
                </span>
              </span>
            </div>
          </Col>
        </Row>

        <Row className={'workload-details mt-3'}>
          {stateSelectedData.volumeType && (
            <Col md={6}>
              <div>
                <label className={'text-black-50'}>
                  {i18next.t('volumeType')}
                </label>
                <span className={'w-150-px'}>
                  {stateSelectedData.volumeType}
                </span>
              </div>
            </Col>
          )}
          {stateSelectedData?.redundancy?.length > 0 && (
            <Col md={6}>
              <Form.Group
                controlId={`${selectedLeftSection.selectedItem}_${stateDefaultWorkloadName.value}_${selectedLeftSection.selectedKeyValue}_formRedundancy`}
                className={'workload-details'}
              >
                <Form.Label className={'text-black-50'}>
                  {i18next.t('redundancy')}
                </Form.Label>
                <Form.Control
                  size={'sm'}
                  as="select"
                  custom
                  className={'d-inline w-auto'}
                  value={
                    stateDynamicValues?.get(
                      `${selectedLeftSection.selectedItem}_${selectedLeftSection.selectedKeyValue}_${stateSelectedData.workloadId}_redundancy`
                    )?.value
                  }
                  onChange={onRedundancyChange}
                >
                  {stateSelectedData?.redundancy &&
                    stateSelectedData.redundancy.map((wn, wnIndex) => {
                      return <option key={wnIndex}>{wn.label}</option>;
                    })}

                  {stateSelectedData?.redundancy?.length === 0 && (
                    <option key={'noData'}>
                      {i18next.t('common:noDataFound')}
                    </option>
                  )}
                </Form.Control>
              </Form.Group>
            </Col>
          )}
        </Row>
      </>
    );
  };

  return (
    <>
      {stateSelectedData && hasData && (
        <>
          <div className={'p-3 border-bottom'}>
            <Form.Group
              controlId={`${selectedLeftSection.selectedItem}_${selectedLeftSection.selectedKeyValue}_formTCOTermInYears`}
              className={'workload-details'}
            >
              <Form.Label className={'text-black-50'}>
                {i18next.t('labelTCOTermYear')}
              </Form.Label>
              <Form.Control
                size={'sm'}
                type="number"
                placeholder={i18next.t('labelTCOTermYear')}
                className={'d-inline w-auto '}
                min={0}
                max={5}
                readOnly
                defaultValue={stateSelectedData.tcoTerm}
              />
            </Form.Group>
            <Form.Group
              controlId={`${selectedLeftSection.selectedItem}_${selectedLeftSection.selectedKeyValue}_formWorkload`}
              className={'workload-details'}
            >
              <Form.Label className={'text-black-50'}>
                {i18next.t('labelWorkload')}
              </Form.Label>
              <Form.Control
                size={'sm'}
                as="select"
                custom
                className={'d-inline w-auto'}
                value={stateDefaultWorkloadName}
                onChange={onWorkloadChange}
              >
                {stateWorkloadItems.map((wn, wnIndex) => {
                  return <option key={wnIndex}>{wn.label}</option>;
                })}
              </Form.Control>
            </Form.Group>

            <Row className={'workload-details'}>
              <Col md={6}>
                <div>
                  <label className={'text-black-50'}>
                    {i18next.t('regionGroup')}
                  </label>
                  <span className={'w-150-px'}>
                    {stateSelectedData.regionGroup}
                  </span>
                </div>
              </Col>
              <Col md={6}>
                <Form.Group
                  controlId={`${selectedLeftSection.selectedItem}_${stateDefaultWorkloadName.value}_${selectedLeftSection.selectedKeyValue}_formRegion`}
                  className={'workload-details'}
                >
                  <Form.Label className={'text-black-50'}>
                    {i18next.t('region')}
                  </Form.Label>
                  <Form.Control
                    size={'sm'}
                    as="select"
                    custom
                    className={'d-inline w-auto'}
                    value={
                      stateDynamicValues?.get(
                        `${selectedLeftSection.selectedItem}_${selectedLeftSection.selectedKeyValue}_${stateSelectedData.workloadId}_region`
                      )?.value
                    }
                    onChange={onRegionChange}
                  >
                    {stateSelectedData?.region &&
                      stateSelectedData.region.map((wn, wnIndex) => {
                        return <option key={wnIndex}>{wn.label}</option>;
                      })}

                    {stateSelectedData?.region?.length === 0 && (
                      <option key={'noData'}>
                        {i18next.t('common:noDataFound')}
                      </option>
                    )}
                  </Form.Control>
                </Form.Group>
              </Col>
            </Row>
            {selectedLeftSection.selectedKeyValue === 'computeResource'
              ? computeResourceWorkload()
              : storageWorkload()}
          </div>
        </>
      )}

      {!hasData && <div className="p-3">{i18next.t('common:noDataFound')}</div>}
    </>
  );
};

export default SectionWorkload;
