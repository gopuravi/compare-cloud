import React, { useState, useEffect } from 'react';
import TableGrid from '@maze/shared-components/TableGrid/TableGrid.component';
import { sortByNumericValue } from '@maze/utilities/Array.utilities';
import { isEmpty } from '@maze/utilities/Object.utilities';

// Table Grid Column Definitions
import SelectedColumnDef from './../GridDataModel/SelectedInstanceColumnDef.model';
import AlternateColumnDef from './../GridDataModel/AlternateInstanceColumnDef.model';

import ModifyWorkloadItemsService from './../services/ModifyWorkload.items.services';

// Language
import i18next from 'i18next';

const SectionVMInstances = (props) => {
  const { data, onUpdateSelectedInstance, selectedLeftSection } = props;
  const selectedColumnDef = SelectedColumnDef();
  const alternateColumnDef = AlternateColumnDef();

  const selectedData = JSON.parse(JSON.stringify(data));
  const [stateData, setStateData] = useState({
    selectedVMData: [],
    vmInstanceData: selectedData?.vmInstance?.alternateVMInstance ?? [],
  });

  useEffect(() => {
    if (selectedData?.endFilteredData?.length) {
      const selectedInstance = selectedData?.vmInstance?.selectedInstance ?? [];
      const optionalInstance =
        selectedData?.vmInstance?.alternateVMInstance ?? [];

      setStateData({
        selectedVMData: isEmpty(selectedInstance) ? [] : [selectedInstance],
        vmInstanceData: optionalInstance,
      });
      onUpdateSelectedInstance(selectedInstance);
    } else {
      setStateData({
        selectedVMData: [],
        vmInstanceData: [],
      });
    }
  }, [data]);

  const onSelectionChange = (e) => {
    onUpdateSelectedInstance(e);

    const selectedItem = selectedData.vmInstance.alternateVMInstance.filter(
      (item) => {
        if (props.selectedLeftSection.selectedKeyValue === 'storage') {
          return e.selectedItem.value === item?.storageName?.value;
        }
        return e.selectedItem.value === item?.instanceName?.value;
      }
    );

    if (stateData?.selectedVMData && stateData?.selectedVMData.length) {
      const selectedVMData = Object.assign([], stateData.selectedVMData);

      const replaceIndex =
        selectedData.vmInstance.alternateVMInstance.findIndex((item) => {
          if (props.selectedLeftSection.selectedKeyValue === 'storage') {
            return (
              item?.storageName?.value === selectedItem[0]?.storageName?.value
            );
          }
          return (
            item?.instanceName?.value === selectedItem[0]?.instanceName?.value
          );
        });
      if (props.selectedLeftSection.selectedKeyValue === 'storage') {
        stateData.vmInstanceData[replaceIndex] = {
          ...selectedVMData[e.index],
          storageName: {
            ...selectedVMData[e.index].storageName,
            customEditor: false,
          },
        };
      } else {
        stateData.vmInstanceData[replaceIndex] = {
          ...selectedVMData[e.index],
          instanceName: {
            ...selectedVMData[e.index].instanceName,
            customEditor: false,
          },
        };
      }

      let options = [];
      if (
        Array.isArray(stateData?.vmInstanceData) &&
        stateData?.vmInstanceData?.length
      ) {
        options = sortByNumericValue(stateData.vmInstanceData, 'cost').map(
          (item) => {
            if (props.selectedLeftSection.selectedKeyValue === 'storage') {
              return {
                label: item?.storageName?.value,
                value: item?.storageName?.value,
              };
            }
            return {
              label: item?.instanceName?.value,
              value: item?.instanceName?.value,
            };
          }
        );
      }

      if (props.selectedLeftSection.selectedKeyValue === 'storage') {
        stateData.selectedVMData[e.index] = {
          ...selectedItem[0],
          storageName: {
            ...selectedItem[0].storageName,
            customEditor: true,
            customEditorConfig: {
              ...selectedItem[0].storageName.customEditorConfig,
              options,
            },
          },
        };
      } else {
        stateData.selectedVMData[e.index] = {
          ...selectedItem[0],
          instanceName: {
            ...selectedItem[0].instanceName,
            customEditor: true,
            customEditorConfig: {
              ...selectedItem[0].instanceName.customEditorConfig,
              options,
            },
          },
        };
      }

      const vmInstance = {
        selectedVMData: stateData.selectedVMData,
        vmInstanceData: sortByNumericValue(stateData.vmInstanceData, 'cost'),
      };

      setStateData(vmInstance);

      ModifyWorkloadItemsService.updateSelectedVMInstances(
        props.selectedLeftSection,
        props.selectedWorkloadName,
        vmInstance
      );
    }
  };

  const loadTemplate = (key, selectedTitle, optionalTitle) => {
    return (
      <>
        <div className={'border-bottom'}>
          <TableGrid
            title={i18next.t(selectedTitle)}
            columnDef={selectedColumnDef[key]}
            data={stateData.selectedVMData}
            onSelectionChange={onSelectionChange}
            showSearch={false}
          ></TableGrid>
        </div>
        <div className={'mt-2'}>
          <TableGrid
            title={i18next.t(optionalTitle)}
            columnDef={alternateColumnDef[key]}
            data={stateData.vmInstanceData}
          ></TableGrid>
        </div>
      </>
    );
  };

  return (
    <>
      {selectedLeftSection.selectedKeyValue === 'computeResource'
        ? loadTemplate(
            selectedLeftSection.selectedKeyValue,
            'selectedVMInstances',
            'alternateVMOptions'
          )
        : loadTemplate(
            selectedLeftSection.selectedKeyValue,
            'selectedStorage',
            'alternateStorage'
          )}
    </>
  );
};

export default SectionVMInstances;
