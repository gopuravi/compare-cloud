import React, { useState, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import { Container, Row, Col, Form, Table } from 'react-bootstrap';
import { Search, XLg } from 'react-bootstrap-icons';

// Language
import i18next from 'i18next';

// Component
import TableGridHead from './TableGridHead/TableGridHead.component';
import TableGridBody from './TableGridBody/TableGridBody.component';

import './TableGrid.component.scss';

const TableGrid = (props) => {
  const { data, columnDef, title, onSelectionChange, showSearch } = props;
  const [stateData, setStateData] = useState([]);
  const [stateShowSearch, setStateShowSearch] = useState(false);
  const [stateKeyString, setStateKeyString] = useState('');
  useEffect(() => {
    let unmounted = false;
    if (!unmounted) {
      setStateData(data);
    }

    return () => {
      unmounted = true;
    };
  }, [data]);
  const handleQuickFilter = (event) => {
    const keyString = event ? event.target.value : '';
    setStateKeyString(keyString);
    const filteredArray = filterByValue(data, keyString);
    setStateData(filteredArray);
  };

  // TODO: should move to common file - Text / String Search
  const filterByValue = (array, string) => {
    return array.filter((o) =>
      Object.keys(o).some((k) => {
        return o[k].toString().toLowerCase().includes(string.toLowerCase());
      })
    );
  };

  const toggleSearchBox = (event) => {
    setStateShowSearch(!stateShowSearch);
  };

  return (
    <>
      {/* // TODO: Have to create seperate Component for TITLE <Below>Title with search input</Below> */}
      {(title || showSearch) && (
        <div className="m-3">
          <Container className={'p-0'} fluid>
            <Row className={'m-0 p-0'}>
              {title && (
                <Col className={'m-0 p-0'} md={6}>
                  <h5>{title}</h5>
                </Col>
              )}
              {showSearch && (
                <Col
                  className={'text-md-right m-0 p-0 d-flex justify-content-end'}
                  md={6}
                >
                  <div className="search-box-with-icon">
                    {stateShowSearch && (
                      <Form.Control
                        size={'sm'}
                        type="text"
                        placeholder={i18next.t('common:search')}
                        onChange={handleQuickFilter}
                        className={'mr-2 d-block'}
                        value={stateKeyString}
                      />
                    )}
                    {!stateShowSearch ? (
                      <Search
                        size={16}
                        className={
                          stateKeyString
                            ? 'text-danger cursor-pointer'
                            : 'cursor-pointer'
                        }
                        onClick={toggleSearchBox}
                      />
                    ) : (
                      <XLg
                        size={16}
                        className={'cursor-pointer'}
                        onClick={toggleSearchBox}
                      />
                    )}
                  </div>
                </Col>
              )}
            </Row>
          </Container>
        </div>
      )}
      <div className={'table-grid-outer'}>
        <div className={'table-grid-outer-inner'}>
          <Table striped bordered hover size="sm">
            <TableGridHead columnData={columnDef}></TableGridHead>
            {stateData.length > 0 && (
              <TableGridBody
                rowData={stateData}
                columnDef={columnDef}
                onSelectionChange={onSelectionChange}
              ></TableGridBody>
            )}

            {stateData.length === 0 && (
              <tbody>
                <tr>
                  <td colSpan={columnDef.length}>
                    {i18next.t('common:noDataFound')}
                  </td>
                </tr>
              </tbody>
            )}
          </Table>
        </div>
      </div>
    </>
  );
};

TableGrid.propTypes = {
  showSearch: PropTypes.bool,
  title: PropTypes.string,
  data: PropTypes.array,
  columnDef: PropTypes.arrayOf(
    PropTypes.exact({
      headerName: PropTypes.string.isRequired,
      field: PropTypes.string.isRequired,
      headerTooltip: PropTypes.string,
      tooltipField: PropTypes.string,
      headerSecondaryText: PropTypes.string,
      width: PropTypes.string,
    })
  ),
};

TableGrid.defaultProps = {
  showSearch: true,
};

export default TableGrid;
