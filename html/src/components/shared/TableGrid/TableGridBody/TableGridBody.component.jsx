import React from 'react';

import CustomDropdown from './../../CustomDropdown/CustomDropdown.component';

import './TableGridBody.component.scss';

const TableGridBody = (props) => {
  const { rowData, columnDef, onSelectionChange } = props;
  const randomNumber = Date();
  return (
    <>
      <tbody>
        {rowData.map((row, rowIndex) => (
          <tr key={`${rowIndex}_row_${randomNumber}`}>
            {columnDef.map((cell, cellIndex) => {
              return (
                <td key={`${cellIndex}_cell_${randomNumber}`}>
                  {row[cell.field]?.customEditor ? (
                    <CustomDropdown
                      key={`${cellIndex}_customDropdown_${randomNumber}`}
                      config={{ ...row[cell.field]?.customEditorConfig }}
                      onSelectionChange={(e) =>
                        onSelectionChange({
                          selectedItem: e,
                          index: rowIndex,
                        })
                      }
                    ></CustomDropdown>
                  ) : row[cell.field]?.value ? (
                    row[cell.field].value
                  ) : row[cell.field] &&
                    row[cell.field].toString().split('###').length > 1 ? (
                    <span>
                      {row[cell.field].toString().split('###')[0]}{' '}
                      <span className="elem--text_round_bracket_small_size text-muted ml-1">
                        {row[cell.field].toString().split('###')[1]}
                      </span>
                    </span>
                  ) : (
                    row[cell.field]
                  )}
                </td>
              );
            })}
          </tr>
        ))}
      </tbody>
    </>
  );
};

export default TableGridBody;
