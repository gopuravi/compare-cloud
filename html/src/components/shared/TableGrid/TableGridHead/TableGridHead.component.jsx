import React from 'react';

const TableGridHead = (props) => {
  const { columnData } = props;
  return (
    <>
      <thead className="border-top">
        <tr>
          {columnData.map((column, columnIndex) => {
            const styleCss = {
              width: column?.width ? column.width : '',
            };
            return (
              <th key={columnIndex} style={styleCss}>
                {column.headerName}
                {column?.headerSecondaryText && (
                  <span
                    className={'text-muted elem--text_round_bracket_small_size'}
                  >
                    {column.headerSecondaryText}
                  </span>
                )}
              </th>
            );
          })}
        </tr>
      </thead>
    </>
  );
};

export default TableGridHead;
