import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  ChevronDown,
  ChevronRight,
  Server,
  CpuFill,
} from 'react-bootstrap-icons';

// SCSS
import './ListItems.component.scss';

import amazon from './../../../assets/images/icons/amazon.png';
import google from './../../../assets/images/icons/google.png';
import azure from './../../../assets/images/icons/azure.png';

const ListItems = (props) => {
  const { items, onSelectMenuItem } = props;
  const [stateActive, setStateActive] = useState({});
  const [stateSelectionStatus, setSelectionStatus] = useState([]);

  const cloudProviderIcons = { AWS: amazon, Azure: azure, Google: google };

  useEffect(() => {
    if (items.length) {
      setStateActive({
        selectedItem: items[0].name,
        selectedSection: items[0].children[0].name,
        selectedKeyValue: items[0].children[0].keyValue,
      });

      const listSelectStatus = items.map(() => true);
      setSelectionStatus(listSelectStatus);
    }
  }, [items]);

  const onClickListItem = (event) => {
    const selectedMenuItem = {
      selectedItem: event.currentTarget.getAttribute('data-value'),
      selectedSection: event.currentTarget.getAttribute('data-section'),
      selectedKeyValue: event.currentTarget.getAttribute('data-key'),
    };
    onSelectMenuItem(selectedMenuItem);
    setStateActive(selectedMenuItem);
  };

  const onClickParentItem = (event) => {
    const newActiveData = stateSelectionStatus.map((value, index) => {
      return index === event ? !value : value;
    });
    setSelectionStatus(newActiveData);
  };

  return (
    <>
      <ul className={`ul-lists-items m-0 p-0`}>
        {items.map((item, listItemIndex) => {
          return (
            <React.Fragment key={listItemIndex}>
              <li
                key={`parent-${listItemIndex}`}
                className={`border-bottom-white cursor-pointer px-3 py-1 ${item.className}`}
                onClick={() => onClickParentItem(listItemIndex)}
              >
                {stateSelectionStatus[listItemIndex] ? (
                  <ChevronDown key={`icon-${listItemIndex}`} size={12} />
                ) : (
                  <ChevronRight key={`icon-${listItemIndex}`} size={12} />
                )}

                <span
                  key={`name-${listItemIndex}`}
                  title={item.name}
                  className={'ml-2'}
                >
                  {cloudProviderIcons[item.name] ? (
                    <img
                      src={cloudProviderIcons[item.name]}
                      alt={item.name}
                      className="size-16"
                    />
                  ) : (
                    ''
                  )}
                  <span className="ml-2 list-title vertical-align-middle">
                    {item.displayName ? item.displayName : item.name}
                  </span>
                </span>
              </li>
              {item.children.length &&
                item.children.map((child, childIndex) => {
                  return (
                    <li
                      key={`child-${childIndex}`}
                      className={`border-bottom-white cursor-pointer px-3 py-1 ${
                        child.className
                      } ${
                        stateActive.selectedItem === item.name &&
                        stateActive.selectedSection === child.name
                          ? 'active'
                          : ''
                      } ${
                        stateSelectionStatus[listItemIndex]
                          ? 'd-block'
                          : 'd-none'
                      }`}
                      data-value={item.name}
                      data-section={child.name}
                      data-key={child.keyValue}
                      onClick={onClickListItem}
                    >
                      <span>
                        {child.keyValue === 'storage' ? (
                          <Server size={16} />
                        ) : (
                          <CpuFill size={16} />
                        )}
                        <span
                          key={`child-name-${childIndex}`}
                          title={child.name}
                          className="ml-2 list-title vertical-align-middle"
                        >
                          {child.name}
                        </span>
                      </span>
                    </li>
                  );
                })}
            </React.Fragment>
          );
        })}
      </ul>
    </>
  );
};

ListItems.propTypes = {
  items: PropTypes.array.isRequired,
};

export default ListItems;
