import React, { useState, useRef, useEffect } from 'react';
import { PropTypes } from 'prop-types';
import Select from 'react-select';

// CSS
import './CustomDropdown.component.scss';

const CustomDropdown = (props) => {
  const { config } = props;
  const [selectedValue, setSelectedValue] = useState(config.value);
  const onChangeOption = (e) => {
    setSelectedValue(e);
    props.onSelectionChange(e);
  };

  const usePrevious = (value) => {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  };

  const prevFilterState = usePrevious({
    selectedValue,
  });

  useEffect(() => {
    let unmounted = false;

    if (!unmounted) {
      if (selectedValue?.value !== config?.value?.value) {
        setSelectedValue(config.value);
      }
    }

    return () => {
      unmounted = true;
    };
  }, [config.value, selectedValue, prevFilterState?.selectedValue]);

  return (
    <>
      <Select
        {...config}
        value={selectedValue}
        onChange={onChangeOption}
        theme={(theme) => ({
          ...theme,
          borderRadius: 0,
        })}
        classNamePrefix="maze-select"
      />
    </>
  );
};

CustomDropdown.propTypes = {
  options: PropTypes.array,
  // value: PropTypes.oneOfType(
  //   PropTypes.exact({ value: PropTypes.string, label: PropTypes.string }),
  //   PropTypes.arrayOf(
  //     PropTypes.exact({ value: PropTypes.string, label: PropTypes.string })
  //   )
  // ),
  value: PropTypes.any,
  onChange: PropTypes.func,
};

export default CustomDropdown;
