// Libraries
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import { ToastContainer } from 'react-toastify';
import { useLocation } from 'react-router-dom';

// Components, pages and views
import Header from './components/Header/Header.component';
import Home from './pages/Home/Home.page';
import Compare from './pages/Compare/Compare.page';
// import About from './pages/About/About.page';
// import Contact from './pages/Contact/Contact.page';

// import logo from './assets/logo.png';

// Language
import i18next from 'i18next';

// Css, SCSS
import './App.scss';

const App = (props) => {
  const { pathname } = useLocation();
  const isEnableFooter = (pathname) => {
    // return pathname === '/' || pathname === '/about' || pathname === '/contact';
    return true;
  };
  return (
    <>
      <Header />
      <Container className={'h-100 p-0 pt-59 pb-59'} fluid>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/compare" component={Compare} exact />
          {/* <Route path="/about" component={About} exact />
          <Route path="/contact" component={Contact} exact /> */}
        </Switch>
      </Container>
      {isEnableFooter(pathname) && (
        <div className="bg-dark p-3 fixed-bottom">
          <div className="container-fluid">
            <footer className="mt-auto text-dark-50">
              <div className="row">
                <div className="col-12 col-md d-flex align-items-center">
                  <small className="d-block text-muted">
                    {i18next.t('common:copyRightText')}
                  </small>
                </div>
                <div className="col-6 col-md">
                  <div
                    className={
                      'd-flex flex-column flex-md-row justify-content-start justify-content-md-end'
                    }
                  >
                    <div className={'text-muted'}>
                      {i18next.t('common:termsAndConditions')}
                    </div>
                    <div className={'ml-0 ml-md-3 text-muted'}>
                      Privacy Policy
                    </div>
                    <div className={'ml-0 ml-md-3 text-muted'}>Contact</div>
                  </div>
                </div>
              </div>
            </footer>
          </div>
        </div>
      )}
      <ToastContainer />
    </>
  );
};

export default App;
