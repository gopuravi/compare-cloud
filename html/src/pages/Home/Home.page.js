// Libraries
import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import './Home.page.scss';

import placeholder from '@maze/assets/images/home/place.png';
// import homeImage from '@maze/assets/images/home/first.jpg';
import homeImage from '@maze/assets/images/home/banner.png';
import workload from '@maze/assets/images/home/slide1.jpg';
import modifyWorkload from '@maze/assets/images/home/slide4.jpg';
import report from '@maze/assets/images/home/report.jpg';

export default class Home extends Component {
  componentDidMount() {}

  routeChange = () => {
    this.props.history.push(`/compare`);
  };

  render() {
    return (
      <>
        <Container fluid className="p-0 bg-white">
          <Container fluid className="p-0">
            <div className="d-flex justify-content-center align-items-center pt-5">
              <main className="px-3 text-center">
                <h1 className="mb-0">
                  A powerful tool to compare Cloud Infrastructure resources
                </h1>
                <div className="d-flex justify-content-center mt-3">
                  <div className="showTwoLines h5 text-muted  mb-0">
                    Simple User Interface to define your workload and tailor the
                    options based on your needs and estimate the best optimized
                    cost between major Cloud Service Providers
                  </div>
                </div>
                <div className="h4 text-black-50 mb-0 mt-3">
                  Do you like to save costs by comparing resources between Cloud
                  Service Providers?
                </div>
                <div className="m-3">
                  <Button variant="primary" onClick={this.routeChange}>
                    Get Started
                  </Button>
                </div>

                <div className="my-5 w-100">
                  <img className="w-100" src={homeImage} alt="place holder" />
                </div>
              </main>
            </div>
          </Container>
          <Container fluid className="p-0 full-block">
            <Container fluid className="p-0">
              <Container className="p-0 py-50">
                <div className="p-3">
                  <div className="d-md-flex justify-content-around">
                    <div className="text-center align-items-center justify-content-center d-flex">
                      <img className="w-75" src={workload} alt="place holder" />
                    </div>
                    <div className="box-content align-items-center justify-content-center d-flex">
                      <div className="p-3">
                        <h2>Define your cloud workload</h2>
                        <div className="text-muted">
                          Define your compute, Storage and Network requirements
                          for your different workloads
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Container>
            </Container>
            <Container fluid className="p-0">
              <Container className="p-0 py-50">
                <div className="p-3">
                  <div className="d-md-flex justify-content-around flex-row flex-row-reverse">
                    <div className="text-center align-items-center justify-content-center d-flex">
                      <img
                        className="w-75"
                        src={modifyWorkload}
                        alt="place holder"
                      />
                    </div>
                    <div className="box-content align-items-center justify-content-center d-flex">
                      <div className="p-3">
                        <h2>Modify your workload</h2>
                        <div className="text-muted">
                          Tweak your workload options based on different options
                          available from Cloud Service Providers
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Container>
            </Container>
            <Container fluid className="p-0">
              <Container className="p-0 py-50">
                <div className="p-3">
                  <div className="d-md-flex justify-content-around">
                    <div className="text-center align-items-center justify-content-center d-flex">
                      <img className="w-75" src={report} alt="place holder" />
                    </div>
                    <div className="box-content align-items-center justify-content-center d-flex">
                      <div className="p-3">
                        <h2>Compare costs for your workload</h2>
                        <div className="text-muted">
                          Compare costs between different Cloud Service
                          Providers for your workload
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </Container>
            </Container>
          </Container>
        </Container>
      </>
    );
  }
}
