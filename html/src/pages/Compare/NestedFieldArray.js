import React, { useState, useEffect } from 'react';
import { useFieldArray } from 'react-hook-form';
import {
  Row,
  Col,
  Form,
  Button,
  Accordion,
  Card,
  OverlayTrigger,
  Tooltip,
  Modal,
} from 'react-bootstrap';
import {
  PlusCircleFill,
  PlusSquareFill,
  XCircleFill,
  ChevronDown,
  ChevronRight,
  Server,
  CpuFill,
} from 'react-bootstrap-icons';
import { isEmpty } from '@maze/utilities/Object.utilities';

// Constants
import { MemoryUnit } from '@maze/constants';

// Components
import Message from '@maze/shared-components/Message/Message.component';

// For Translation
import i18next from 'i18next';

const NestedArray = ({
  nestIndex,
  control,
  register,
  errors,
  stateMsgCreateNewWorkload,
  instanceTypeOptions,
  operatingSystemOptions,
  instanceFamilyOptions,
  regionOptions,
  unregister,
  getValues,
  setValue,
  setError,
  cloudProviders,
  formState,
}) => {
  const { fields, remove, append } = useFieldArray({
    control,
    name: `workload[${nestIndex}].computeResource`,
  });

  const fieldArray = useFieldArray({
    control,
    name: `workload[${nestIndex}].storage`,
  });

  const storageFields = fieldArray.fields;
  const [payAsYouGoState, setPayAsYouGo] = useState({});
  const [instanceTypeState, setInstanceType] = useState({});
  const [storageTypeState, setStorageType] = useState({});
  const [accordionState, setAccordionState] = useState([true, true]);

  const { workload } = errors;
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    performRefresh(); //children function of interest
  }, [formState]);

  const performRefresh = () => {
    if (!isEmpty(errors)) {
      const newActiveData = accordionState.map((value, index) => {
        return Object.keys(errors?.workload[0])
          .map((item) => (item === 'computeResource' ? 0 : 1))
          .includes(index);
      });
      setAccordionState(newActiveData);
    }
  };

  const onPayAsYouGoChange = ({ target: { id, checked, tabIndex } }) => {
    if (!checked) {
      setValue(
        `workload[${nestIndex}].computeResource[${tabIndex}].usagePerMonth`,
        730
      );
    }

    setPayAsYouGo({
      ...payAsYouGoState,
      [id]: checked,
    });
  };

  const onChangeInstanceType = ({ target: { id, value, tabIndex } }) => {
    if (value === 'Dedicated') {
      unregister(`workload[${nestIndex}].computeResource[${tabIndex}].memory`);
    }

    setInstanceType({
      ...instanceTypeState,
      [id]: value,
    });
  };

  const onChangeStorageType = ({ target: { id, value, tabIndex } }) => {
    if (value !== 'Block') {
      unregister(`workload[${nestIndex}].storage[${tabIndex}].volumeType`);
    }
    setStorageType({
      ...storageTypeState,
      [id]: value,
    });
  };

  const activeOnClick = (id) => {
    const newActiveData = accordionState.map((value, index) => {
      return index === id ? !value : value;
    });
    setAccordionState(newActiveData);
  };

  const storageType = [
    {
      label: 'Block (Local/SAN)',
      value: 'Block',
    },
    {
      label: 'NAS/File',
      value: 'File',
    },
    {
      label: 'Object/Blob',
      value: 'Object',
    },
    {
      label: 'Backup',
      value: 'Backup',
    },
    {
      label: 'Archive',
      value: 'Archive',
    },
  ];

  const volumeType = [
    {
      label: 'HDD',
      value: 'HDD',
    },
    {
      label: 'SSD',
      value: 'SSD',
    },
  ];

  const capacityUnit = [
    {
      label: 'GB',
      value: 'GB',
    },
    {
      label: 'TB',
      value: 'TB',
    },
  ];

  const hasDuplicateValues = (wid, index, group) => {
    const computeResourceValues = getValues(
      `workload[${nestIndex}].computeResource`
    ).map((item, index) => {
      return {
        ...item,
        id: fields[index].id,
      };
    });

    const storageValues = getValues(`workload[${nestIndex}].storage`).map(
      (item, index) => {
        return {
          ...item,
          id: storageFields[index].id,
        };
      }
    );

    const combinedWorkloadData = [...computeResourceValues, ...storageValues];
    const groupData = group === 'storage' ? storageFields : fields;

    const hasDuplicateEntry = combinedWorkloadData.find(
      (item) => item.id !== groupData[index].id && item.workloadId === wid
    );

    if (hasDuplicateEntry) {
      return false;
    }

    return true;
  };

  const onClickDropdown = (event) => {
    event.preventDefault();
    if (!cloudProviders.length) {
      handleShow();
    }
  };

  return (
    <>
      <div>
        {!fields.length && stateMsgCreateNewWorkload && (
          <Message
            customClasses={'py-2 px-3'}
            data={{
              message: stateMsgCreateNewWorkload,
            }}
          />
        )}
        <Accordion>
          <React.Fragment key={`0_fragment`}>
            <Accordion.Toggle
              as={Card.Header}
              className={`p-1 cursor-pointer`}
              onClick={() => activeOnClick(0)}
            >
              <div className={'mx-2'}>
                {accordionState[0] ? (
                  <ChevronDown className="text-primary" />
                ) : (
                  <ChevronRight className="text-primary" />
                )}
                <span className="ml-2">
                  <CpuFill size={16} />
                  <span className={'ml-1 vertical-align-middle'}>
                    {i18next.t('headerComputeResources')}
                  </span>
                </span>
              </div>
            </Accordion.Toggle>

            <Accordion.Collapse
              className={`${accordionState[0] ? 'collapse show' : ''} m-0`}
            >
              <div className={'m-0 compute-resource-dynamic-row-outer'}>
                {fields.map((item, k) => {
                  return (
                    <div
                      key={item.id}
                      className={
                        'compute-resource-dynamic-row border-bottom mt-3 px-3'
                      }
                    >
                      <Row>
                        <Col xs={10} className={'pr-0'}>
                          <Form.Group
                            controlId="formWorkloadName"
                            className={'required'}
                          >
                            <Form.Control
                              size={'sm'}
                              autoComplete="off"
                              type="text"
                              placeholder={i18next.t('workloadName')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].workloadId`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('workloadName'),
                                    }),
                                  },
                                  validate: {
                                    message: (workloadId) =>
                                      hasDuplicateValues(
                                        workloadId,
                                        k,
                                        'computeResource'
                                      ) ||
                                      i18next.t('common:duplicateEntry', {
                                        label: i18next.t('workloadName'),
                                      }),
                                  },
                                }
                              )}
                              defaultValue={item.workloadId}
                            />
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.workloadId
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        <Col xs={2}>
                          <OverlayTrigger
                            placement="top"
                            overlay={
                              <Tooltip>
                                {i18next.t('addServerWorkload')}
                              </Tooltip>
                            }
                          >
                            <PlusCircleFill
                              className={'cursor-pointer text-primary'}
                              size={24}
                              onClick={() =>
                                append({
                                  workloadId: '',
                                  instanceType: instanceTypeOptions[0]?.value,
                                  operatingSystemFamily:
                                    operatingSystemOptions[0].value,
                                  instanceFamily:
                                    instanceFamilyOptions[0].value,
                                  regionGroup: regionOptions[0].value,
                                  noOfServers: 1,
                                  virtualCpu: 8,
                                  memory: 16,
                                  payAsYouGo: false,
                                  usagePerMonth: 730,
                                })
                              }
                            />
                          </OverlayTrigger>

                          <OverlayTrigger
                            placement="top"
                            overlay={
                              <Tooltip>{i18next.t('common:remove')}</Tooltip>
                            }
                          >
                            <XCircleFill
                              className={
                                'cursor-pointer ml-md-1 ml-lg-2 text-danger'
                              }
                              size={24}
                              onClick={() => remove(k)}
                            />
                          </OverlayTrigger>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={3}>
                          <Form.Group>
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('instanceType')}
                            </Form.Label>
                            <Form.Control
                              tabIndex={k}
                              id={`instanceType${nestIndex}${k}`}
                              size={'sm'}
                              as={'select'}
                              placeholder={i18next.t('instanceType')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].instanceType`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('instanceType'),
                                    }),
                                  },
                                }
                              )}
                              defaultValue={item.instanceType}
                              onChange={onChangeInstanceType}
                              custom
                              onClick={onClickDropdown}
                            >
                              {instanceTypeOptions.length === 0 && (
                                <option value={''}>
                                  {i18next.t('common:pleaseSelect')}
                                </option>
                              )}
                              {instanceTypeOptions.map(
                                (instanceType, instanceTypeIndex) => {
                                  return (
                                    <option key={instanceTypeIndex}>
                                      {instanceType.label}
                                    </option>
                                  );
                                }
                              )}
                            </Form.Control>
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.instanceType
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        <Col md={3}>
                          <Form.Group controlId="formOperatingSystem">
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('operatingSystem')}
                            </Form.Label>
                            <Form.Control
                              size={'sm'}
                              as={'select'}
                              placeholder={i18next.t('operatingSystem')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].operatingSystemFamily`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('operatingSystem'),
                                    }),
                                  },
                                }
                              )}
                              defaultValue={item.operatingSystemFamily}
                              custom
                              onClick={onClickDropdown}
                            >
                              <option value={''}>
                                {i18next.t('common:pleaseSelect')}
                              </option>
                              {operatingSystemOptions.map(
                                (operatingSystem, operatingSystemIndex) => {
                                  return (
                                    <option key={operatingSystemIndex}>
                                      {operatingSystem.label}
                                    </option>
                                  );
                                }
                              )}
                            </Form.Control>
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.operatingSystemFamily
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        <Col md={3}>
                          <Form.Group controlId="formInstanceFamily">
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('instanceFamily')}
                            </Form.Label>
                            <Form.Control
                              size={'sm'}
                              as={'select'}
                              placeholder={i18next.t('instanceFamily')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].instanceFamily`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('instanceFamily'),
                                    }),
                                  },
                                }
                              )}
                              defaultValue={item.instanceFamily}
                              custom
                              onClick={onClickDropdown}
                            >
                              <option value={''}>
                                {i18next.t('common:pleaseSelect')}
                              </option>
                              {instanceFamilyOptions.map(
                                (instanceFamily, instanceFamilyIndex) => {
                                  return (
                                    <option key={instanceFamilyIndex}>
                                      {instanceFamily.label}
                                    </option>
                                  );
                                }
                              )}
                            </Form.Control>
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.instanceFamily
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        <Col md={3}>
                          <Form.Group controlId="formRegion">
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('regionGroup')}
                            </Form.Label>
                            <Form.Control
                              size={'sm'}
                              as={'select'}
                              placeholder={i18next.t('regionGroup')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].regionGroup`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('regionGroup'),
                                    }),
                                  },
                                }
                              )}
                              defaultValue={item.region}
                              custom
                              onClick={onClickDropdown}
                            >
                              <option value={''}>
                                {i18next.t('common:pleaseSelect')}
                              </option>
                              {regionOptions.map((region, regionIndex) => {
                                return (
                                  <option key={regionIndex}>
                                    {region.label}
                                  </option>
                                );
                              })}
                            </Form.Control>
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.regionGroup
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={2}>
                          <Form.Group controlId="formNoOfServers">
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('noOfServers')}
                            </Form.Label>
                            <Form.Control
                              size={'sm'}
                              type={'number'}
                              placeholder={i18next.t('noOfServers')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].noOfServers`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('noOfServers'),
                                    }),
                                  },
                                  max: {
                                    value: 10000000,
                                    message: i18next.t('common:maxValue', {
                                      value: 10000000,
                                    }),
                                  },
                                  min: {
                                    value: 1,
                                    message: i18next.t('common:minValue', {
                                      value: 1,
                                    }),
                                  },
                                }
                              )}
                              defaultValue={
                                item.noOfServers ? item.noOfServers : 1
                              }
                              min={0}
                              max={10000000}
                            />
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.noOfServers
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        <Col md={2}>
                          <Form.Group controlId="formVCPUsPerServer">
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('vCPUsPerServer')}
                            </Form.Label>
                            <Form.Control
                              size={'sm'}
                              type={'number'}
                              placeholder={i18next.t('vCPUsPerServer')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].virtualCpu`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('vCPUsPerServer'),
                                    }),
                                  },
                                  max: {
                                    value: 10000000,
                                    message: i18next.t('common:maxValue', {
                                      value: 10000000,
                                    }),
                                  },
                                  min: {
                                    value: 1,
                                    message: i18next.t('common:minValue', {
                                      value: 1,
                                    }),
                                  },
                                }
                              )}
                              defaultValue={
                                item.virtualCpu ? item.virtualCpu : 8
                              }
                              min={0}
                              max={10000000}
                            />
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.virtualCpu
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        {instanceTypeState[`instanceType${nestIndex}${k}`] !==
                          'Dedicated' && (
                          <Col md={3}>
                            <Form.Group controlId="formMemoryPerServer">
                              <Form.Label className={'required text-black-50'}>
                                {i18next.t('memoryPerServer')}
                                <span
                                  className={
                                    'text-muted elem--text_round_bracket_small_size'
                                  }
                                >
                                  {MemoryUnit.GB}
                                </span>
                              </Form.Label>
                              <Form.Control
                                size={'sm'}
                                type={'number'}
                                placeholder={i18next.t('memoryPerServer')}
                                {...register(
                                  `workload[${nestIndex}].computeResource[${k}].memory`,
                                  {
                                    required: {
                                      value: true,
                                      message: i18next.t(
                                        'common:requiredField',
                                        {
                                          label: i18next.t('memoryPerServer'),
                                        }
                                      ),
                                    },
                                    max: {
                                      value: 100000,
                                      message: i18next.t('common:maxValue', {
                                        value: 100000,
                                      }),
                                    },
                                    min: {
                                      value: 1,
                                      message: i18next.t('common:minValue', {
                                        value: 1,
                                      }),
                                    },
                                  }
                                )}
                                defaultValue={item.memory ? item.memory : 16}
                                min={0}
                                disabled={
                                  instanceTypeState[
                                    `instanceType${nestIndex}${k}`
                                  ] === 'Dedicated'
                                }
                              />
                              <Form.Text>
                                {workload &&
                                  workload[nestIndex]?.computeResource && (
                                    <Message
                                      data={
                                        workload[nestIndex]?.computeResource[k]
                                          ?.memory
                                      }
                                    />
                                  )}
                              </Form.Text>
                            </Form.Group>
                          </Col>
                        )}
                        <Col md={2}>
                          <Form.Group controlId="formPayAsYouGo">
                            <Form.Label className={'text-black-50'}>
                              {i18next.t('payAsYouGo')}
                            </Form.Label>
                            <Form.Check
                              id={`payAsYouGo${nestIndex}${k}`}
                              tabIndex={k}
                              type={'switch'}
                              placeholder={i18next.t('payAsYouGo')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].payAsYouGo`
                              )}
                              defaultValue={
                                item.payAsYouGo
                                  ? item.payAsYouGo
                                  : payAsYouGoState[
                                      `payAsYouGo${nestIndex}${k}`
                                    ]
                              }
                              onChange={onPayAsYouGoChange}
                            />
                          </Form.Group>
                        </Col>
                        <Col md={3}>
                          <Form.Group controlId="formEstimatedHoursPerMonth">
                            <Form.Label className={'text-black-50'}>
                              {i18next.t('estimatedHoursPerMonth')}
                              {payAsYouGoState[`payAsYouGo${nestIndex}${k}`]}
                            </Form.Label>
                            <Form.Control
                              size={'sm'}
                              type={'number'}
                              placeholder={i18next.t('estimatedHoursPerMonth')}
                              {...register(
                                `workload[${nestIndex}].computeResource[${k}].usagePerMonth`,
                                {
                                  required: {
                                    value:
                                      payAsYouGoState[
                                        `payAsYouGo${nestIndex}${k}`
                                      ],
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t(
                                        'estimatedHoursPerMonth'
                                      ),
                                    }),
                                  },
                                  max: {
                                    value: 730,
                                    message: i18next.t('common:maxValue', {
                                      value: 730,
                                    }),
                                  },
                                  min: {
                                    value: 1,
                                    message: i18next.t('common:minValue', {
                                      value: 1,
                                    }),
                                  },
                                }
                              )}
                              defaultValue={
                                item.usagePerMonth ? item.usagePerMonth : 730
                              }
                              min={0}
                              max={730}
                              disabled={
                                !payAsYouGoState[`payAsYouGo${nestIndex}${k}`]
                              }
                            />
                            <Form.Text>
                              {workload &&
                                workload[nestIndex]?.computeResource && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.computeResource[k]
                                        ?.usagePerMonth
                                    }
                                  />
                                )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                      </Row>
                    </div>
                  );
                })}
                <OverlayTrigger
                  placement="top"
                  overlay={<Tooltip>{i18next.t('addServerWorkload')}</Tooltip>}
                >
                  <Button
                    size={'sm'}
                    className={'p-0 text-decoration-none m-3'}
                    variant="link"
                    onClick={() =>
                      append({
                        workloadId: '',
                        instanceType: instanceTypeOptions[0]?.value,
                        operatingSystemFamily: operatingSystemOptions[0].value,
                        instanceFamily: instanceFamilyOptions[0].value,
                        regionGroup: regionOptions[0].value,
                        noOfServers: 1,
                        virtualCpu: 8,
                        memory: 16,
                        payAsYouGo: false,
                        usagePerMonth: 730,
                      })
                    }
                  >
                    <PlusSquareFill size={32} />
                  </Button>
                </OverlayTrigger>
              </div>
            </Accordion.Collapse>

            <Accordion.Toggle
              as={Card.Header}
              className={`p-1 cursor-pointer`}
              onClick={() => activeOnClick(1)}
            >
              <div className={'mx-2'}>
                {accordionState[1] ? (
                  <ChevronDown className="text-primary" />
                ) : (
                  <ChevronRight className="text-primary" />
                )}
                <span className="ml-2">
                  <Server size={16} />
                  <span className={'ml-1 vertical-align-middle'}>
                    {i18next.t('storage')}
                  </span>
                </span>
              </div>
            </Accordion.Toggle>

            <Accordion.Collapse
              className={`${accordionState[1] ? 'collapse show' : ''} m-0`}
            >
              <div>
                {storageFields.map((item, k) => {
                  return (
                    <div
                      key={item.id}
                      className={
                        'compute-resource-dynamic-row border-bottom mt-3 px-3'
                      }
                    >
                      <Row>
                        <Col xs={10} className={'pr-0'}>
                          <Form.Group
                            controlId="formStorageName"
                            className={'required'}
                          >
                            <Form.Control
                              tabIndex={k}
                              size={'sm'}
                              autoComplete="off"
                              type="text"
                              placeholder={i18next.t('workloadName')}
                              {...register(
                                `workload[${nestIndex}].storage[${k}].workloadId`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('workloadName'),
                                    }),
                                  },
                                  // validate: {
                                  //   message: (workloadId) =>
                                  //     isDuplicate(workloadId, 'storage') ||
                                  //     i18next.t('common:duplicateEntry', {
                                  //       label: i18next.t('workloadName'),
                                  //     }),
                                  // },
                                  validate: {
                                    message: (workloadId) =>
                                      hasDuplicateValues(
                                        workloadId,
                                        k,
                                        'storage'
                                      ) ||
                                      i18next.t('common:duplicateEntry', {
                                        label: i18next.t('workloadName'),
                                      }),
                                  },
                                }
                              )}
                              // onBlur={isDuplicate}
                              defaultValue={item.workloadId}
                            />
                            <Form.Text>
                              {workload && workload[nestIndex]?.storage && (
                                <Message
                                  data={
                                    workload[nestIndex]?.storage[k]?.workloadId
                                  }
                                />
                              )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        <Col xs={2}>
                          <OverlayTrigger
                            placement="top"
                            overlay={
                              <Tooltip>{i18next.t('addStorage')}</Tooltip>
                            }
                          >
                            <PlusCircleFill
                              className={'cursor-pointer text-primary'}
                              size={24}
                              onClick={() => {
                                const id = `storageType${nestIndex}${control.fieldsRef.current.workload[nestIndex].storage.length}`;
                                setStorageType({
                                  ...storageTypeState,
                                  [id]: 'Block',
                                });
                                fieldArray.append({
                                  workloadId: '',
                                  storageType: 'Block',
                                  volumeType: 'HDD',
                                  diskSpace: 2,
                                  diskSpaceUnit: 'GB',
                                  regionGroup: regionOptions[0].value,
                                });
                              }}
                            />
                          </OverlayTrigger>

                          <OverlayTrigger
                            placement="top"
                            overlay={
                              <Tooltip>{i18next.t('common:remove')}</Tooltip>
                            }
                          >
                            <XCircleFill
                              className={
                                'cursor-pointer ml-md-1 ml-lg-2 text-danger'
                              }
                              size={24}
                              onClick={() => fieldArray.remove(k)}
                            />
                          </OverlayTrigger>
                        </Col>
                      </Row>
                      <Row>
                        <Col md={3}>
                          <Form.Group>
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('storageType')}
                            </Form.Label>
                            <Form.Control
                              tabIndex={k}
                              id={`storageType${nestIndex}${k}`}
                              size={'sm'}
                              as={'select'}
                              placeholder={i18next.t('storageType')}
                              {...register(
                                `workload[${nestIndex}].storage[${k}].storageType`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('storageType'),
                                    }),
                                  },
                                }
                              )}
                              defaultValue={item.storageType}
                              onChange={onChangeStorageType}
                              custom
                            >
                              {storageType.map((type, typeIndex) => {
                                return (
                                  <option key={typeIndex} value={type.value}>
                                    {type.label}
                                  </option>
                                );
                              })}
                            </Form.Control>
                            <Form.Text>
                              {workload && workload[nestIndex]?.storage && (
                                <Message
                                  data={
                                    workload[nestIndex]?.storage[k]?.storageType
                                  }
                                />
                              )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                        {(storageTypeState[`storageType${nestIndex}${k}`] !==
                          '' &&
                          storageTypeState[`storageType${nestIndex}${k}`]) ===
                          'Block' && (
                          <Col md={3}>
                            <Form.Group controlId="formDiskType">
                              <Form.Label className={'required text-black-50'}>
                                {i18next.t('volumeType')}
                              </Form.Label>
                              <Form.Control
                                size={'sm'}
                                as={'select'}
                                placeholder={i18next.t('volumeType')}
                                {...register(
                                  `workload[${nestIndex}].storage[${k}].volumeType`,
                                  {
                                    required: {
                                      value: true,
                                      message: i18next.t(
                                        'common:requiredField',
                                        {
                                          label: i18next.t('volumeType'),
                                        }
                                      ),
                                    },
                                  }
                                )}
                                defaultValue={item.volumeType}
                                custom
                                disabled={
                                  (storageTypeState[
                                    `storageType${nestIndex}${k}`
                                  ] !== '' &&
                                    storageTypeState[
                                      `storageType${nestIndex}${k}`
                                    ]) !== 'Block'
                                }
                              >
                                {volumeType.map((type, typeIndex) => {
                                  return (
                                    <option key={typeIndex}>
                                      {type.label}
                                    </option>
                                  );
                                })}
                              </Form.Control>
                              <Form.Text>
                                {workload && workload[nestIndex]?.storage && (
                                  <Message
                                    data={
                                      workload[nestIndex]?.storage[k]
                                        ?.volumeType
                                    }
                                  />
                                )}
                              </Form.Text>
                            </Form.Group>
                          </Col>
                        )}
                        <Col md={3}>
                          <Form.Group controlId="formCapacity">
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('capacity')}
                            </Form.Label>
                            <Row className="m-0">
                              <Col className={'p-0'}>
                                <Form.Control
                                  size={'sm'}
                                  type={'number'}
                                  placeholder={i18next.t('diskSpace')}
                                  {...register(
                                    `workload[${nestIndex}].storage[${k}].diskSpace`,
                                    {
                                      required: {
                                        value: true,
                                        message: i18next.t(
                                          'common:requiredField',
                                          {
                                            label: i18next.t('diskSpace'),
                                          }
                                        ),
                                      },
                                      max: {
                                        value: 100000,
                                        message: i18next.t('common:maxValue', {
                                          value: 100000,
                                        }),
                                      },
                                      min: {
                                        value: 1,
                                        message: i18next.t('common:minValue', {
                                          value: 1,
                                        }),
                                      },
                                    }
                                  )}
                                  defaultValue={
                                    item.diskSpace ? item.diskSpace : 2
                                  }
                                  min={0}
                                />
                                <Form.Text>
                                  {workload && workload[nestIndex]?.storage && (
                                    <Message
                                      data={
                                        workload[nestIndex]?.storage[k]
                                          ?.diskSpace
                                      }
                                    />
                                  )}
                                </Form.Text>
                              </Col>
                              <Col className={'p-0 pl-2'}>
                                <Form.Control
                                  size={'sm'}
                                  as={'select'}
                                  placeholder={i18next.t('capacity')}
                                  {...register(
                                    `workload[${nestIndex}].storage[${k}].diskSpaceUnit`,
                                    {
                                      required: {
                                        value: true,
                                        message: i18next.t(
                                          'common:requiredField',
                                          {
                                            label: i18next.t('capacity'),
                                          }
                                        ),
                                      },
                                    }
                                  )}
                                  defaultValue={item.diskSpaceUnit}
                                  custom
                                >
                                  {capacityUnit.map((unit, unitIndex) => {
                                    return (
                                      <option key={unitIndex}>
                                        {unit.label}
                                      </option>
                                    );
                                  })}
                                </Form.Control>
                                <Form.Text>
                                  {workload && workload[nestIndex]?.storage && (
                                    <Message
                                      data={
                                        workload[nestIndex]?.storage[k]
                                          ?.diskSpaceUnit
                                      }
                                    />
                                  )}
                                </Form.Text>
                              </Col>
                            </Row>
                          </Form.Group>
                        </Col>
                        <Col md={3}>
                          <Form.Group controlId="formStorageRegion">
                            <Form.Label className={'required text-black-50'}>
                              {i18next.t('regionGroup')}
                            </Form.Label>
                            <Form.Control
                              size={'sm'}
                              as={'select'}
                              placeholder={i18next.t('regionGroup')}
                              {...register(
                                `workload[${nestIndex}].storage[${k}].regionGroup`,
                                {
                                  required: {
                                    value: true,
                                    message: i18next.t('common:requiredField', {
                                      label: i18next.t('regionGroup'),
                                    }),
                                  },
                                }
                              )}
                              defaultValue={item.region}
                              custom
                              onClick={onClickDropdown}
                            >
                              <option value={''}>
                                {i18next.t('common:pleaseSelect')}
                              </option>
                              {regionOptions.map((region, regionIndex) => {
                                return (
                                  <option key={regionIndex}>
                                    {region.label}
                                  </option>
                                );
                              })}
                            </Form.Control>
                            <Form.Text>
                              {workload && workload[nestIndex]?.storage && (
                                <Message
                                  data={
                                    workload[nestIndex]?.storage[k]?.regionGroup
                                  }
                                />
                              )}
                            </Form.Text>
                          </Form.Group>
                        </Col>
                      </Row>
                    </div>
                  );
                })}
                <OverlayTrigger
                  placement="top"
                  overlay={<Tooltip>{i18next.t('addStorage')}</Tooltip>}
                >
                  <Button
                    size={'sm'}
                    className={'p-0 text-decoration-none m-3'}
                    variant="link"
                    onClick={() => {
                      const id = `storageType${nestIndex}${control.fieldsRef.current.workload[nestIndex].storage.length}`;
                      setStorageType({
                        ...storageTypeState,
                        [id]: 'Block',
                      });

                      fieldArray.append({
                        workloadId: '',
                        storageType: 'Block',
                        volumeType: 'HDD',
                        diskSpace: 2,
                        diskSpaceUnit: 'GB',
                        regionGroup: regionOptions[0].value,
                      });
                    }}
                  >
                    <PlusSquareFill size={32} />
                  </Button>
                </OverlayTrigger>
              </div>
            </Accordion.Collapse>
          </React.Fragment>
        </Accordion>
      </div>

      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title>Warning</Modal.Title>
        </Modal.Header>
        <Modal.Body>{i18next.t('messageSelectAtLeastOneProvide')}</Modal.Body>
        <Modal.Footer>
          <Button variant="primary" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default NestedArray;
