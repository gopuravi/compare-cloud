import React, { useState, useRef, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Tab, Button, Form } from 'react-bootstrap';
import { isEmpty } from '@maze/utilities/Object.utilities';
import Stepper from '@maze/components/Stepper';
import Loader from 'react-loader';
import { toast } from 'react-toastify';
import { errorMessage } from '@maze/utilities/Error.utilities';

// Constants
import { Status } from '@maze/constants';

// Components
import FieldArray from './FieldArray';
import ModifyWorkload from '@maze/components/ModifyWorkload/ModifyWorkload.component';
import CompareOptimize from '@maze/components/CompareOptimize/CompareOptimize.component';

// Services
import CombinedResultsServices from '@maze/shared/services/combined-results/combined-results.services';
import { getCloudProvidersHTTPService } from '@maze/http-get-services/cloud-providers-http.services';
import { getDataByAttributeNameHTTPService } from '@maze/http-get-services/attribute-data-http.services';

// import scss
import './Compare.page.scss';

// For Translation
import i18next from 'i18next';

const Compare = () => {
  const workLoadFormRef = useRef(); // Form Reference
  const defaultPage = 0;
  const totalPages = 2;
  const defaultValues = {
    workload: [
      {
        cloudProviders: [],
        tcoTerm: 3,
        computeResource: [],
        storage: [],
      },
    ],
  };

  const [page, setPage] = useState(defaultPage);
  const [stateCloudProviders, setStateCloudProviders] = useState([]);
  const [stateMsgCreateNewWorkload, setStateMsgCreateNewWorkload] =
    useState('');
  const [stateWorkLoadFormData, setStateWorkLoadFormData] =
    useState(defaultValues);
  const [reloadData, setReloadData] = useState(true);
  const [isLoaded, setIsLoaded] = useState(false);
  const [allAttributeValues, setAllAttributeValues] = useState([]);

  const [formState, checkFormState] = useState(0);
  const [downloadReportState, onDownloadReport] = useState(0);

  const {
    control,
    register,
    handleSubmit,
    formState: { errors },
    unregister,
    getValues,
    setValue,
    setError,
  } = useForm({
    mode: 'all',
    defaultValues,
  });

  useEffect(() => {
    const cloudProviders$ = getCloudProvidersHTTPService();
    const instanceTypeItems$ = getDataByAttributeNameHTTPService({
      attribute: 'instanceType',
      attributeField: 'instanceTypeMapped',
    });
    const operatingSystemItems$ = getDataByAttributeNameHTTPService({
      attribute: 'operatingSystem',
      attributeField: 'operatingSystemFamily',
    });
    const instanceFamilyItems$ = getDataByAttributeNameHTTPService({
      attribute: 'instanceFamily',
      attributeField: 'instanceFamilyMapped',
    });
    const regionItems$ = getDataByAttributeNameHTTPService({
      attribute: 'region',
      attributeField: 'regionGroupMapped',
    });
    const promises = [
      cloudProviders$,
      instanceTypeItems$,
      operatingSystemItems$,
      instanceFamilyItems$,
      regionItems$,
    ];

    Promise.allSettled(promises).then((results) => {
      const [cloudProvider] = results;
      setAllAttributeValues(results);
      if (cloudProvider.status === Status.fulfilled) {
        setStateCloudProviders(cloudProvider.value);
      }
      setErrorResults(results);
      setIsLoaded(true);
    });
  }, []);

  const setErrorResults = (results) => {
    results.forEach((result) => {
      if (result.status === Status.rejected) {
        toast.error(`${errorMessage(result.reason)}`);
      }
    });
  };

  const onSubmit = (data) => {
    CombinedResultsServices.setCombinedResultData(data, stateCloudProviders);
    setStateWorkLoadFormData(data);
  };

  const onClickNext = () => {
    const hasComputeResource =
      control.fieldArrayDefaultValuesRef.current.workload.some(
        (wl) => wl.computeResource.length || wl.storage.length
      );
    if (hasComputeResource) {
      setStateMsgCreateNewWorkload('');
      if (page === 0) {
        setReloadData(true);
        workLoadFormRef.current.dispatchEvent(
          new Event('submit', { cancelable: true, bubbles: true })
        );
        setTimeout(() => {
          if (isEmpty(errors)) {
            setPage(page + 1);
          } else {
            checkFormState((prev) => prev + 1);
          }
        });
      } else {
        setPage(page + 1);
      }
    } else {
      setStateMsgCreateNewWorkload(i18next.t('defineComputeResources'));
    }
  };

  const onClickDownloadReport = () => {
    onDownloadReport((prev) => prev + 1);
  };

  return (
    <div className={'page-compare p-3'}>
      <Loader loaded={isLoaded}>
        <Tab.Container
          className="maze-custom-tabs"
          defaultActiveKey={defaultPage}
          activeKey={page}
        >
          <div className={'p-0 m-0 hight-100 maze-custom-tabs'}>
            <div className="px-0">
              <Stepper
                steps={[
                  {
                    title: i18next.t('titleDefineWorkLoads'),
                    onClick: () => {
                      setPage(0);
                    },
                  },
                  {
                    title: i18next.t('titleModifyWorkLoad'),
                    onClick: () => {
                      setPage(1);
                    },
                  },
                  {
                    title: i18next.t('titleCompareAndOptimize'),
                  },
                ]}
                activeStep={page}
              />
            </div>
            <div className={'p-0 bg-white'}>
              <Tab.Content>
                <Tab.Pane eventKey={0}>
                  <>
                    <Form
                      ref={workLoadFormRef}
                      onSubmit={handleSubmit(onSubmit)}
                    >
                      <FieldArray
                        {...{
                          control,
                          register,
                          errors,
                          allAttributeValues,
                          stateMsgCreateNewWorkload,
                          unregister,
                          getValues,
                          setValue,
                          setError,
                          formState,
                        }}
                      />
                    </Form>
                  </>
                </Tab.Pane>
                {/* TODO: Godwin should optimize here - POST call should be in common(or should be in parent) place, 
                not inside child get back to parent */}
                <Tab.Pane eventKey={1}>
                  <>
                    {page === 1 && (
                      <ModifyWorkload
                        reloadData={reloadData}
                        cloudProviders={stateCloudProviders}
                        definedData={stateWorkLoadFormData}
                      />
                    )}
                  </>
                </Tab.Pane>
                <Tab.Pane eventKey={2}>
                  <>
                    <div className={'pt-3'}>
                      {page === 2 && (
                        <CompareOptimize
                          downloadReportState={downloadReportState}
                        ></CompareOptimize>
                      )}
                    </div>
                  </>
                </Tab.Pane>
                {/* TODO: Godwin should optimize here */}
              </Tab.Content>
              <div className={'p-3 border-top text-right'}>
                {/* Footer Section */}

                {page !== 0 && (
                  <Button
                    variant="light"
                    onClick={() => {
                      setPage(page - 1);
                      setReloadData(false);
                    }}
                  >
                    {i18next.t('common:previous')}
                  </Button>
                )}
                {page === 2 && (
                  <Button
                    variant="primary"
                    className={'ml-3'}
                    onClick={onClickDownloadReport}
                  >
                    {i18next.t('common:downloadReport')}
                  </Button>
                )}
                {page !== totalPages && (
                  <Button
                    variant="primary"
                    className={'ml-3'}
                    onClick={() => onClickNext()}
                  >
                    {i18next.t('common:next')}
                  </Button>
                )}
              </div>
            </div>
          </div>
        </Tab.Container>
      </Loader>
    </div>
  );
};

export default Compare;
