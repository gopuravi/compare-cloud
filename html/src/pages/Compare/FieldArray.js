import React, { useState, useEffect } from 'react';
import { useFieldArray } from 'react-hook-form';
import { Row, Col, Form } from 'react-bootstrap';
import { arrayIntersect } from '@maze/utilities/Array.utilities';

// Child Component
import NestedArray from './NestedFieldArray';
import Message from '@maze/shared-components/Message/Message.component';

// Constants
import { Status } from '@maze/constants';

// For Translation
import i18next from 'i18next';

const Fields = ({
  control,
  register,
  errors,
  allAttributeValues,
  stateMsgCreateNewWorkload,
  unregister,
  getValues,
  setValue,
  setError,
  formState,
}) => {
  const { fields } = useFieldArray({
    control,
    name: 'workload',
  });

  const [selectedCloudProvidersState, setSelectedCloudProviderState] = useState(
    []
  );
  const [cloudProvidersItemsState, setCloudProvidersItemsState] = useState([]);
  const [instanceTypeItemsState, setInstanceTypeItemsState] = useState([]);
  const [operatingSystemItemsState, setOperatingSystemItemsState] = useState(
    []
  );
  const [instanceFamilyItemsState, setInstanceFamilyItemsState] = useState([]);
  const [regionItemsState, setRegionItemsState] = useState([]);

  const [instanceTypeOptionsState, setInstanceTypeOptionsState] = useState([]);
  const [operatingSystemOptionsState, setOperatingSystemOptionsState] =
    useState([]);
  const [instanceFamilyOptionsState, setInstanceFamilyOptionsState] = useState(
    []
  );
  const [regionOptionsState, setRegionOptionsState] = useState([]);
  const [
    cloudProviders,
    instanceTypeItems,
    operatingSystemItems,
    instanceFamilyItems,
    regionItems,
  ] = allAttributeValues;

  useEffect(() => {
    let unmounted = false;

    if (!unmounted) {
      if (instanceTypeItems.status === Status.fulfilled) {
        if (instanceTypeItems.value.length) {
          setInstanceTypeItemsState(instanceTypeItems.value);
        }
      }

      if (operatingSystemItems.status === Status.fulfilled) {
        if (operatingSystemItems.value.length) {
          setOperatingSystemItemsState(operatingSystemItems.value);
        }
      }

      if (instanceFamilyItems.status === Status.fulfilled) {
        if (instanceFamilyItems.value.length) {
          setInstanceFamilyItemsState(instanceFamilyItems.value);
        }
      }

      if (regionItems.status === Status.fulfilled) {
        if (regionItems.value.length) {
          setRegionItemsState(regionItems.value);
        }
      }

      if (cloudProviders.status === Status.fulfilled) {
        if (cloudProviders.value.length) {
          const cloudProviderLists = cloudProviders.value.map(
            ({ name, cloudProviderId }) => ({
              label: name,
              displayName: i18next.t(name),
              value: cloudProviderId,
              checked: true,
            })
          );
          setCloudProvidersItemsState(cloudProviderLists);
          const selectedCloudProvider = cloudProviders.value.map(
            (cp) => cp.cloudProviderId
          );
          setSelectedCloudProviderState(selectedCloudProvider);
        }
      }
    }

    return () => {
      unmounted = true;
    };
  }, [
    cloudProviders.status,
    cloudProviders.value,
    instanceTypeItems.status,
    instanceTypeItems.value,
    operatingSystemItems.status,
    operatingSystemItems.value,
    instanceFamilyItems.status,
    instanceFamilyItems.value,
    regionItems.value,
    regionItems.status,
  ]);

  const onChangeCloudProvider = (event) => {
    const selectedCloudProvider = updateSelectedCloudProvider(event);
    setSelectedCloudProviderState(selectedCloudProvider);
    const updateCloudProviderSelection = cloudProvidersItemsState.map((cp) => {
      if (parseInt(cp.value) === parseInt(event.target.value)) {
        return {
          ...cp,
          checked: event.target.checked,
        };
      }

      return { ...cp };
    });

    setCloudProvidersItemsState(updateCloudProviderSelection);
    updateDropDownValues(selectedCloudProvidersState);
  };

  const updateSelectedCloudProvider = (event) => {
    let selectedCloudProvider = selectedCloudProvidersState;
    const isAvailable = selectedCloudProvider.includes(event.target.value);
    if (event.target.checked) {
      if (!isAvailable) {
        selectedCloudProvider.push(event.target.value);
      }
    } else {
      selectedCloudProvider = selectedCloudProvider.filter(
        (cp) => cp !== event.target.value
      );
    }

    return selectedCloudProvider;
  };

  const updateDropDownValues = (selectedCloudProvider) => {
    const instanceTypes = instanceTypeItemsState;
    const operatingSystems = operatingSystemItemsState;
    const instanceFamily = instanceFamilyItemsState;
    const region = regionItemsState;

    if (selectedCloudProvider.length) {
      // Filter options based on the Selected Cloud Providers
      const instanceTypeOptions = arrayIntersect(
        instanceTypes,
        selectedCloudProvider
      );
      setInstanceTypeOptionsState(instanceTypeOptions);

      const operatingSystemOptions = arrayIntersect(
        operatingSystems,
        selectedCloudProvider
      );
      setOperatingSystemOptionsState(operatingSystemOptions);

      const instanceFamilyOptions = arrayIntersect(
        instanceFamily,
        selectedCloudProvider
      );
      setInstanceFamilyOptionsState(instanceFamilyOptions);

      const regionOptions = arrayIntersect(region, selectedCloudProvider);
      setRegionOptionsState(regionOptions);
    } else {
      setInstanceTypeOptionsState([]);
      setOperatingSystemOptionsState([]);
      setInstanceFamilyOptionsState([]);
      setRegionOptionsState([]);
    }
  };

  useEffect(() => {
    updateDropDownValues(selectedCloudProvidersState);
  }, [setSelectedCloudProviderState, selectedCloudProvidersState]);

  const { workload } = errors;

  return (
    <>
      {fields.map((item, index) => {
        return (
          <div key={item.id}>
            <Row className={'m-0 pt-3'}>
              <Col md={6}>
                <Form.Group>
                  <Form.Label className={'mr-2 required text-black-50'}>
                    {i18next.t('labelCloudServiceProviders')}
                  </Form.Label>
                  {cloudProvidersItemsState.map((cp, cpIndex) => {
                    return (
                      <Form.Check
                        size={'sm'}
                        type={'checkbox'}
                        key={cpIndex}
                        inline
                        id={cp.label}
                        label={cp.displayName}
                        checked={cp.checked}
                        defaultValue={cp.value}
                        {...register(`workload.${index}.cloudProviders`, {
                          required: {
                            value: true,
                            message: i18next.t(
                              'messageSelectAtLeastOneProvide'
                            ),
                          },
                          validate: () => selectedCloudProvidersState.length,
                        })}
                        onClick={onChangeCloudProvider}
                      />
                    );
                  })}
                  {/* <Form.Text>
                    {!cloudProvidersItemsState.some((cp) => cp.checked) && (
                      <Message
                        data={{
                          message: i18next.t('messageSelectAtLeastOneProvide'),
                        }}
                      />
                    )}
                  </Form.Text> */}
                </Form.Group>
              </Col>
              <Col md={6}>
                <Form.Group className={'text-md-right'}>
                  <Form.Label
                    htmlFor="tcoTerm"
                    className={'required text-black-50'}
                  >
                    {i18next.t('labelTCOTermYear')}
                  </Form.Label>
                  <Form.Control
                    size={'sm'}
                    {...register(`workload.${index}.tcoTerm`, {
                      required: {
                        value: true,
                        message: i18next.t('common:requiredField', {
                          label: i18next.t('labelTCOTermYear'),
                        }),
                      },
                      max: {
                        value: 10,
                        message: i18next.t('common:maxValue', {
                          value: 10,
                        }),
                      },
                      min: {
                        value: 1,
                        message: i18next.t('common:minValue', {
                          value: 1,
                        }),
                      },
                    })}
                    className={'ml-3 d-inline w-auto'}
                    type="number"
                    id="tcoTerm"
                    min={1}
                    max={10}
                    defaultValue={item.tcoTerm}
                  />
                  <Form.Text>
                    {workload && <Message data={workload[index].tcoTerm} />}
                  </Form.Text>
                </Form.Group>
              </Col>
            </Row>

            <NestedArray
              nestIndex={index}
              {...{
                control,
                register,
                errors,
                stateMsgCreateNewWorkload,
                unregister,
                getValues,
                setValue,
                setError,
                cloudProviders: selectedCloudProvidersState,
                formState,
              }}
              instanceTypeOptions={instanceTypeOptionsState}
              operatingSystemOptions={operatingSystemOptionsState}
              instanceFamilyOptions={instanceFamilyOptionsState}
              regionOptions={regionOptionsState}
            />
          </div>
        );
      })}
    </>
  );
};

export default Fields;
