// Constants
import { workloadTypes } from '@maze/constants';
class CombinedResultsServices {
  static #combinedResult;

  static setCombinedResultData(data, stateCloudProviders) {
    const workloadData = data.workload[0];
    let combinedResultObj = {};
    const combinedResult = workloadData.cloudProviders.map((item) => {
      for (const dataKey in workloadData) {
        const isValidWorkload = workloadTypes[dataKey] ?? false;
        if (isValidWorkload) {
          combinedResultObj[dataKey] = workloadData[dataKey].map((cr) => ({
            ...cr,
            tcoTerm: workloadData.tcoTerm,
          }));
        }
      }

      return {
        cloudProvider: stateCloudProviders.find(
          (cp) => cp.cloudProviderId === item
        ),
        ...combinedResultObj,
      };
    });

    CombinedResultsServices.#combinedResult = combinedResult;
  }

  static getCombinedResultData() {
    return CombinedResultsServices.#combinedResult;
  }
}
export default CombinedResultsServices;
