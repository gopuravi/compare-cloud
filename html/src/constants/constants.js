export const Condition = {
  CONTAINS: 'CONTAINS',
  EQUALITY: 'EQUALITY',
  GREATER_THAN: 'GREATER_THAN',
  IN: 'IN',
  BETWEEN: 'BETWEEN',
};

export const CustomAttributes = {
  ON_DEMAND: 'On Demand',
  RESERVED: 'Reserved',
};

export const Constants = {
  SELECTED: 'Selected',
  OPTIONAL: 'Optional',
};

export const Sort = {
  DESC: 'desc',
  ASC: 'asc',
};

export const MemoryUnit = {
  GB: 'GB',
  MB: 'MB',
  KB: 'KB',
};

export const SearchTypes = {
  floatArr: 'floatArr',
};

export const workloadTypes = {
  computeResource: 'computeResource',
  storage: 'storage',
};

export const Status = {
  fulfilled: 'fulfilled',
  rejected: 'rejected',
};
