import { axiosApiInstance } from '../HttpInterceptors';

export const getCloudProvidersHTTPService = () => {
  console.log('getCloudProvidersHTTPInterface');
  return new Promise((resolve, reject) => {
    axiosApiInstance
      .get('/cloud-api/data/cloud-provider/all')
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};
