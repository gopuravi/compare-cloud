import { axiosApiInstance } from '../HttpInterceptors';

export const postComputeResourceHTTPService = (requestPayload) => {
  console.log('postComputeResourceHTTPService');
  return new Promise((resolve, reject) => {
    axiosApiInstance
      .post('/cloud-api/compare/computeResource', requestPayload)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};

export const postStorageHTTPService = (requestPayload) => {
  console.log('postStorageHTTPService');
  return new Promise((resolve, reject) => {
    axiosApiInstance
      .post('/cloud-api/storageCompare/storageResource', requestPayload)
      .then((response) => resolve(response.data))
      .catch((error) => reject(error));
  });
};
