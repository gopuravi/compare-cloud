import { Sort } from '@maze/constants';

export const arrayIntersect = (arrayIn, identifierArray) => {
  const arrayOut = arrayIn
    .filter((item) =>
      identifierArray.every((i) => item.cloudProviderIdList.includes(i))
    )
    .map((val) => ({ label: val.name, value: val.name }));

  return arrayOut;
};

export const sortByNumericValue = (items, valueKey) => {
  return items.sort((a, b) => {
    return a[valueKey] - b[valueKey];
  });
};

export const sort_by = function () {
  const fields = [].slice.call(arguments),
    n_fields = fields.length;

  return function (A, B) {
    let a, b, field, key, reverse, result;
    for (let i = 0, l = n_fields; i < l; i++) {
      result = 0;
      field = fields[i];

      key = typeof field === 'string' ? field : field.name;

      a = A[key];
      b = B[key];

      if (typeof field.primer !== 'undefined') {
        a = field.primer(a);
        b = field.primer(b);
      }

      reverse = field.reverse ? -1 : 1;

      if (a < b) result = reverse * -1;
      if (a > b) result = reverse * 1;
      if (result !== 0) break;
    }
    return result;
  };
};
