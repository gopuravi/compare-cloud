const express = require('express');

const ComputeResourceResponseMockData = require('./data/mock-compute-resource-response');
const storageResponseMockData = require('./data/mock-storage-response.json');
const ComputeResourcePostMockRouter = express.Router();

ComputeResourcePostMockRouter.post(
  '/cloud-api/compare/computeResource',
  (req, res) => {
    console.log(
      '============================= Compute Resource - POST =================================='
    );
    console.log('request method :::', req.method);
    console.log('request url :::', req.url);
    console.log('request params :::', req.params);
    console.log('request params :::', req.body);
    console.log(
      '============================= Compute Resource - POST =================================='
    );
    res.send(ComputeResourceResponseMockData);
  }
);

ComputeResourcePostMockRouter.post(
  '/cloud-api/storageCompare/storageResource',
  (req, res) => {
    console.log(
      '============================= Storage - POST =================================='
    );
    console.log('request method :::', req.method);
    console.log('request url :::', req.url);
    console.log('request params :::', req.params);
    console.log('request params :::', req.body);
    console.log(
      '============================= Storage - POST =================================='
    );
    res.send(storageResponseMockData);
  }
);

module.exports = ComputeResourcePostMockRouter;
