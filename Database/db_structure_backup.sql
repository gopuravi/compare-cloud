/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 8.0.17 : Database - test
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `application_properties` */

DROP TABLE IF EXISTS `application_properties`;

CREATE TABLE `application_properties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(20) NOT NULL,
  `value` varchar(50) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `application_properties_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `cloud_provider_mst` */

DROP TABLE IF EXISTS `cloud_provider_mst`;

CREATE TABLE `cloud_provider_mst` (
  `cloud_provider_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `description` varchar(100) NOT NULL,
  `active` tinyint(1) DEFAULT '0',
  `display_order` int(11) DEFAULT NULL,
  `created_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(40) NOT NULL DEFAULT 'admin',
  `updated_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `updated_by` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `file_structure_mst` */

DROP TABLE IF EXISTS `file_structure_mst`;

CREATE TABLE `file_structure_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `file_structure` char(100) NOT NULL,
  `file_structure_mapped` char(100) NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `file_structure_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_structure_mst_id`),
  KEY `file_structure_mst_ibfk_1` (`cloud_provider_id`),
  CONSTRAINT `file_structure_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `instance_family_mst` */

DROP TABLE IF EXISTS `instance_family_mst`;

CREATE TABLE `instance_family_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `instance_family` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `instance_family_mapped` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `instance_family_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`instance_family_mst_id`),
  UNIQUE KEY `instance_type_mst_unique` (`instance_family`,`cloud_provider_id`),
  KEY `cloud_provider_id` (`cloud_provider_id`),
  CONSTRAINT `instance_family_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `instance_mst` */

DROP TABLE IF EXISTS `instance_mst`;

CREATE TABLE `instance_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `instance_name` char(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `instance_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`instance_mst_id`),
  UNIQUE KEY `operating_system_mst_unique` (`cloud_provider_id`,`instance_name`),
  CONSTRAINT `instance_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1775 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `instance_type_mst` */

DROP TABLE IF EXISTS `instance_type_mst`;

CREATE TABLE `instance_type_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `instance_type` char(30) NOT NULL,
  `instance_type_mapped` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `instance_type_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`instance_type_mst_id`),
  UNIQUE KEY `instance_type_mst_unique` (`instance_type`,`cloud_provider_id`),
  KEY `cloud_provider_id` (`cloud_provider_id`),
  CONSTRAINT `instance_type_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `lease_term_mst` */

DROP TABLE IF EXISTS `lease_term_mst`;

CREATE TABLE `lease_term_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `lease_term` char(30) NOT NULL,
  `lease_term_mapped` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `lease_term_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`lease_term_mst_id`),
  UNIQUE KEY `lease_term_mst_unique` (`cloud_provider_id`,`lease_term`),
  CONSTRAINT `lease_term_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `offering_class_mst` */

DROP TABLE IF EXISTS `offering_class_mst`;

CREATE TABLE `offering_class_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `offering_class` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `offering_class_mapped` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `offering_class_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`offering_class_mst_id`),
  UNIQUE KEY `offering_class_mst_unique` (`cloud_provider_id`,`offering_class`),
  CONSTRAINT `offering_class_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `operating_system_mst` */

DROP TABLE IF EXISTS `operating_system_mst`;

CREATE TABLE `operating_system_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `operating_system` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `operating_system_mapped` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `operating_system_family` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `operating_system_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`operating_system_mst_id`),
  UNIQUE KEY `operating_system_mst_unique` (`cloud_provider_id`,`operating_system`),
  CONSTRAINT `operating_system_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `payment_option_mst` */

DROP TABLE IF EXISTS `payment_option_mst`;

CREATE TABLE `payment_option_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `payment_option` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `payment_option_mapped` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `payment_option_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`payment_option_mst_id`),
  UNIQUE KEY `payment_option_mst_unique` (`cloud_provider_id`,`payment_option`),
  CONSTRAINT `payment_option_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `processor_mst` */

DROP TABLE IF EXISTS `processor_mst`;

CREATE TABLE `processor_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `processor` char(100) NOT NULL,
  `processor_family_mapped` char(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `processor_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`processor_mst_id`),
  UNIQUE KEY `operating_system_mst_unique` (`cloud_provider_id`,`processor`),
  CONSTRAINT `processor_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `redundancy_mst` */

DROP TABLE IF EXISTS `redundancy_mst`;

CREATE TABLE `redundancy_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `redundancy` char(5) NOT NULL,
  `redundancy_mapped` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `redundancy_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`redundancy_mst_id`),
  KEY `redundancy_mst_ibfk_1` (`cloud_provider_id`),
  CONSTRAINT `redundancy_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `region_mst` */

DROP TABLE IF EXISTS `region_mst`;

CREATE TABLE `region_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `region` char(30) NOT NULL,
  `region_group_mapped` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `location_mapped` char(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `region_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`region_mst_id`),
  UNIQUE KEY `region_xrf_region_unique` (`region`,`cloud_provider_id`),
  KEY `cloud_provider_id` (`cloud_provider_id`),
  CONSTRAINT `region_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `server_attributes` */

DROP TABLE IF EXISTS `server_attributes`;

CREATE TABLE `server_attributes` (
  `cloud_provider_id` int(2) NOT NULL,
  `operating_system_mst_id` int(11) NOT NULL,
  `region_mst_id` int(11) NOT NULL,
  `instance_mst_id` int(11) NOT NULL,
  `instance_type_mst_id` int(11) NOT NULL,
  `instance_family_mst_id` int(11) NOT NULL,
  `processor_mst_id` int(11) NOT NULL,
  `virtual_cpu` int(11) NOT NULL,
  `memory` float NOT NULL,
  `default_storage` float DEFAULT NULL,
  `server_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`server_attributes_id`),
  UNIQUE KEY `server_attributes_uk1` (`cloud_provider_id`,`operating_system_mst_id`,`region_mst_id`,`instance_mst_id`),
  KEY `operating_system_mst_id` (`operating_system_mst_id`),
  KEY `region_mst_id` (`region_mst_id`),
  KEY `instance_mst_id` (`instance_mst_id`),
  KEY `instance_type_mst_id` (`instance_type_mst_id`),
  KEY `instance_family_mst_id` (`instance_family_mst_id`),
  KEY `processor_mst_id` (`processor_mst_id`),
  CONSTRAINT `server_attributes_cp_fk1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`),
  CONSTRAINT `server_attributes_if_fk6` FOREIGN KEY (`instance_family_mst_id`) REFERENCES `instance_family_mst` (`instance_family_mst_id`),
  CONSTRAINT `server_attributes_in_fk4` FOREIGN KEY (`instance_mst_id`) REFERENCES `instance_mst` (`instance_mst_id`),
  CONSTRAINT `server_attributes_it_fk5` FOREIGN KEY (`instance_type_mst_id`) REFERENCES `instance_type_mst` (`instance_type_mst_id`),
  CONSTRAINT `server_attributes_os_fk2` FOREIGN KEY (`operating_system_mst_id`) REFERENCES `operating_system_mst` (`operating_system_mst_id`),
  CONSTRAINT `server_attributes_pr_fk7` FOREIGN KEY (`processor_mst_id`) REFERENCES `processor_mst` (`processor_mst_id`),
  CONSTRAINT `server_attributes_re_fk3` FOREIGN KEY (`region_mst_id`) REFERENCES `region_mst` (`region_mst_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `server_attributes_stg` */

DROP TABLE IF EXISTS `server_attributes_stg`;

CREATE TABLE `server_attributes_stg` (
  `cloud_provider_id` int(2) NOT NULL,
  `operating_system` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `region` char(30) NOT NULL,
  `instance_name` char(50) NOT NULL,
  `virtual_cpu` int(11) NOT NULL,
  `memory` float NOT NULL,
  `default_storage` char(30) NOT NULL,
  `instance_type` char(10) NOT NULL,
  `instance_family` char(50) NOT NULL,
  `processor` char(50) DEFAULT NULL,
  `operating_system_mst_id` int(11) DEFAULT NULL,
  `region_mst_id` int(11) DEFAULT NULL,
  `instance_mst_id` int(11) DEFAULT NULL,
  `instance_type_mst_id` int(11) DEFAULT NULL,
  `instance_family_mst_id` int(11) DEFAULT NULL,
  `processor_mst_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`server_attributes_id`),
  UNIQUE KEY `server_attributes_uk1` (`cloud_provider_id`,`operating_system`,`region`,`instance_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1928647 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `server_attributes_stg_google` */

DROP TABLE IF EXISTS `server_attributes_stg_google`;

CREATE TABLE `server_attributes_stg_google` (
  `cloud_provider_id` int(2) NOT NULL,
  `operating_system` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `region` char(30) NOT NULL,
  `instance_name` char(50) NOT NULL,
  `virtual_cpu` int(11) NOT NULL,
  `memory` float NOT NULL,
  `default_storage` char(30) NOT NULL,
  `instance_type` char(10) NOT NULL,
  `instance_family` char(50) NOT NULL,
  `processor` char(50) DEFAULT NULL,
  `operating_system_mst_id` int(11) DEFAULT NULL,
  `region_mst_id` int(11) DEFAULT NULL,
  `instance_mst_id` int(11) DEFAULT NULL,
  `instance_type_mst_id` int(11) DEFAULT NULL,
  `instance_family_mst_id` int(11) DEFAULT NULL,
  `processor_mst_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server_attributes_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `server_pricing` */

DROP TABLE IF EXISTS `server_pricing`;

CREATE TABLE `server_pricing` (
  `server_attributes_id` int(11) NOT NULL,
  `lease_term_mst_id` int(11) NOT NULL,
  `offering_class_mst_id` int(11) NOT NULL,
  `payment_option_mst_id` int(11) NOT NULL,
  `hourly_price` float NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server_pricing_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`server_pricing_id`),
  UNIQUE KEY `server_pricing_uk1` (`server_attributes_id`,`lease_term_mst_id`,`offering_class_mst_id`,`payment_option_mst_id`),
  KEY `lease_term_mst_id` (`lease_term_mst_id`),
  KEY `offering_class_mst_id` (`offering_class_mst_id`),
  KEY `payment_option_mst_id` (`payment_option_mst_id`),
  CONSTRAINT `server_pricing_lt_fk1` FOREIGN KEY (`lease_term_mst_id`) REFERENCES `lease_term_mst` (`lease_term_mst_id`),
  CONSTRAINT `server_pricing_oc_fk2` FOREIGN KEY (`offering_class_mst_id`) REFERENCES `offering_class_mst` (`offering_class_mst_id`),
  CONSTRAINT `server_pricing_po_fk3` FOREIGN KEY (`payment_option_mst_id`) REFERENCES `payment_option_mst` (`payment_option_mst_id`),
  CONSTRAINT `server_pricing_sa_fk4` FOREIGN KEY (`server_attributes_id`) REFERENCES `server_attributes` (`server_attributes_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `server_pricing_stg` */

DROP TABLE IF EXISTS `server_pricing_stg`;

CREATE TABLE `server_pricing_stg` (
  `cloud_provider_id` int(2) NOT NULL,
  `operating_system` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `region` char(30) NOT NULL,
  `instance_name` char(50) NOT NULL,
  `lease_term` char(10) NOT NULL,
  `offering_class` char(30) NOT NULL,
  `payment_option` char(30) NOT NULL,
  `hourly_price` float NOT NULL,
  `server_attributes_id` int(11) DEFAULT NULL,
  `lease_term_mst_id` int(11) DEFAULT NULL,
  `offering_class_mst_id` int(11) DEFAULT NULL,
  `payment_option_mst_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server_pricing_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`server_pricing_id`),
  UNIQUE KEY `server_pricing_uk1` (`cloud_provider_id`,`operating_system`,`region`,`instance_name`,`lease_term`,`offering_class`,`payment_option`)
) ENGINE=InnoDB AUTO_INCREMENT=1855682 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `server_pricing_stg_google` */

DROP TABLE IF EXISTS `server_pricing_stg_google`;

CREATE TABLE `server_pricing_stg_google` (
  `cloud_provider_id` int(2) NOT NULL,
  `operating_system` char(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `region` char(30) NOT NULL,
  `instance_name` char(50) NOT NULL,
  `lease_term` char(10) NOT NULL,
  `offering_class` char(30) NOT NULL,
  `payment_option` char(30) NOT NULL,
  `hourly_price` float NOT NULL,
  `server_attributes_id` int(11) DEFAULT NULL,
  `lease_term_mst_id` int(11) DEFAULT NULL,
  `offering_class_mst_id` int(11) DEFAULT NULL,
  `payment_option_mst_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `server_pricing_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `storage_attributes` */

DROP TABLE IF EXISTS `storage_attributes`;

CREATE TABLE `storage_attributes` (
  `cloud_provider_id` int(11) NOT NULL,
  `region_mst_id` int(11) NOT NULL,
  `storage_type_mst_id` int(11) NOT NULL,
  `volume_type_mst_id` int(11) NOT NULL,
  `storage_mst_id` int(11) NOT NULL,
  `redundancy_mst_id` int(11) NOT NULL,
  `file_structure_mst_id` int(11) NOT NULL,
  `storage_attributes_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`storage_attributes_id`),
  UNIQUE KEY `storage_attributes_uk1` (`cloud_provider_id`,`region_mst_id`,`storage_type_mst_id`,`volume_type_mst_id`,`storage_mst_id`,`redundancy_mst_id`,`file_structure_mst_id`),
  KEY `region_mst_id` (`region_mst_id`),
  KEY `storage_type_mst_id` (`storage_type_mst_id`),
  KEY `volume_type_mst_id` (`volume_type_mst_id`),
  KEY `storage_mst_id` (`storage_mst_id`),
  KEY `redundancy_mst_id` (`redundancy_mst_id`),
  KEY `file_structure_mst_id` (`file_structure_mst_id`),
  CONSTRAINT `storage_attributes_cp_fk1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`),
  CONSTRAINT `storage_attributes_fs_fk7` FOREIGN KEY (`file_structure_mst_id`) REFERENCES `file_structure_mst` (`file_structure_mst_id`),
  CONSTRAINT `storage_attributes_re_fk2` FOREIGN KEY (`region_mst_id`) REFERENCES `region_mst` (`region_mst_id`),
  CONSTRAINT `storage_attributes_re_fk6` FOREIGN KEY (`redundancy_mst_id`) REFERENCES `redundancy_mst` (`redundancy_mst_id`),
  CONSTRAINT `storage_attributes_sn_fk5` FOREIGN KEY (`storage_mst_id`) REFERENCES `storage_mst` (`storage_mst_id`),
  CONSTRAINT `storage_attributes_st_fk3` FOREIGN KEY (`storage_type_mst_id`) REFERENCES `storage_type_mst` (`storage_type_mst_id`),
  CONSTRAINT `storage_attributes_vt_fk4` FOREIGN KEY (`volume_type_mst_id`) REFERENCES `volume_type_mst` (`volume_type_mst_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5245 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `storage_mst` */

DROP TABLE IF EXISTS `storage_mst`;

CREATE TABLE `storage_mst` (
  `cloud_provider_id` int(2) NOT NULL,
  `storage_name` char(100) NOT NULL,
  `storage_name_mapped` char(100) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `storage_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`storage_mst_id`),
  KEY `storage_mst_ibfk_1` (`cloud_provider_id`),
  CONSTRAINT `storage_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=191 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `storage_pricing` */

DROP TABLE IF EXISTS `storage_pricing`;

CREATE TABLE `storage_pricing` (
  `storage_attributes_id` int(11) NOT NULL,
  `lease_term_mst_id` int(11) NOT NULL,
  `disk_space` int(10) DEFAULT NULL,
  `disk_space_uom` char(10) DEFAULT NULL,
  `min_iops` int(10) DEFAULT NULL,
  `max_iops` int(10) DEFAULT NULL,
  `min_throughput` int(10) DEFAULT NULL,
  `max_throughput` int(10) DEFAULT NULL,
  `min_tier` int(10) DEFAULT NULL,
  `min_tier_uom` char(10) DEFAULT NULL,
  `max_tier` int(10) DEFAULT NULL,
  `max_tier_uom` char(10) DEFAULT NULL,
  `storage_price` float DEFAULT NULL,
  `iops_price` float DEFAULT NULL,
  `throughput_price` float DEFAULT NULL,
  `storage_pricing_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`storage_pricing_id`),
  UNIQUE KEY `storage_pricing_uk1` (`storage_attributes_id`,`lease_term_mst_id`,`disk_space`,`disk_space_uom`,`min_iops`,`max_iops`,`min_throughput`,`max_throughput`,`min_tier`,`min_tier_uom`,`max_tier`,`max_tier_uom`),
  KEY `lease_term_mst_id` (`lease_term_mst_id`),
  CONSTRAINT `storage_pricing_lt_fk1` FOREIGN KEY (`lease_term_mst_id`) REFERENCES `lease_term_mst` (`lease_term_mst_id`),
  CONSTRAINT `storage_pricing_sa_fk2` FOREIGN KEY (`storage_attributes_id`) REFERENCES `storage_attributes` (`storage_attributes_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9341 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `storage_pricing_stg` */

DROP TABLE IF EXISTS `storage_pricing_stg`;

CREATE TABLE `storage_pricing_stg` (
  `cloud_provider_id` int(2) NOT NULL,
  `region` char(30) NOT NULL,
  `storage_type` char(30) NOT NULL,
  `volume_type` char(30) NOT NULL,
  `storage_name` char(100) NOT NULL,
  `redundancy` char(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'NA',
  `file_structure` char(100) NOT NULL DEFAULT 'NA',
  `lease_term` char(10) NOT NULL,
  `disk_space` int(10) DEFAULT NULL,
  `disk_space_uom` char(10) DEFAULT NULL,
  `min_iops` int(10) DEFAULT NULL,
  `max_iops` int(10) DEFAULT NULL,
  `min_throughput` int(10) DEFAULT NULL,
  `max_throughput` int(10) DEFAULT NULL,
  `min_tier` int(10) DEFAULT NULL,
  `min_tier_uom` char(10) DEFAULT NULL,
  `max_tier` int(10) DEFAULT NULL,
  `max_tier_uom` char(10) DEFAULT NULL,
  `storage_price` float DEFAULT NULL,
  `iops_price` float DEFAULT NULL,
  `throughput_price` float DEFAULT NULL,
  `region_mst_id` int(11) DEFAULT NULL,
  `storage_type_mst_id` int(11) DEFAULT NULL,
  `volume_type_mst_id` int(11) DEFAULT NULL,
  `storage_mst_id` int(11) DEFAULT NULL,
  `redundancy_mst_id` int(11) DEFAULT NULL,
  `file_structure_mst_id` int(11) DEFAULT NULL,
  `lease_term_mst_id` int(11) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `storage_type_mst` */

DROP TABLE IF EXISTS `storage_type_mst`;

CREATE TABLE `storage_type_mst` (
  `cloud_provider_id` int(11) NOT NULL,
  `storage_type` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `storage_type_mapped` char(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `storage_type_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`storage_type_mst_id`),
  KEY `storage_type_mst_ibfk_1` (`cloud_provider_id`),
  CONSTRAINT `storage_type_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*Table structure for table `volume_type_mst` */

DROP TABLE IF EXISTS `volume_type_mst`;

CREATE TABLE `volume_type_mst` (
  `cloud_provider_id` int(11) NOT NULL,
  `volume_type` char(30) NOT NULL,
  `volume_type_mapped` char(30) NOT NULL DEFAULT 'DEFAULT',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `volume_type_mst_id` int(11) NOT NULL AUTO_INCREMENT,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`volume_type_mst_id`),
  KEY `volume_type_mst_ibfk_1` (`cloud_provider_id`),
  CONSTRAINT `volume_type_mst_ibfk_1` FOREIGN KEY (`cloud_provider_id`) REFERENCES `cloud_provider_mst` (`cloud_provider_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
