import React from 'react';
import { render } from '@testing-library/react';

import Help from './Help';

describe('Help', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Help />);
    expect(baseElement).toBeTruthy();
  });
});
