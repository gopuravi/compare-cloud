import React, { Component } from 'react';
import i18next from 'i18next';
import { Container } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import { LinkContainer } from 'react-router-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

import './Home.scss';

export interface HomeProps {}

class Home extends Component<HomeProps> {
  render() {
    return (
      <Container fluid>
        <div className="windowCenter">
          <LinkContainer to="/compare">
            <Button variant="primary" size="sm">
              {i18next.t('common:getStarted')}
              <FontAwesomeIcon className="mar-lt-10" icon={faChevronRight} />
            </Button>
          </LinkContainer>
        </div>
      </Container>
    );
  }
}

export default Home;
