import {
  GET_CLOUD_PROVIDERS_LIST_REQUEST,
  GET_CLOUD_PROVIDERS_LIST_RESPONSE,
  GET_CLOUD_PROVIDERS_LIST_ERRORS
} from './../constants/types';
import {getCloudProvidersHTTPService} from '@react-client/shared';

const cloudProvidersRequest = () => {
  // console.log('cloudProvidersRequest called');
  return {
    type: GET_CLOUD_PROVIDERS_LIST_REQUEST,
    data: [],
    errors: {},
    loading: true
  }
}

const getCloudProvidersResponse = (data) => {
  // console.log('getCloudProvidersResponse called :::', data);
  return {
    type: GET_CLOUD_PROVIDERS_LIST_RESPONSE,
    data,
    errors: {},
    loading: false
  }
}

const getCloudProvidersError = (error) => {
  // console.log('getCloudProvidersError called :::', error);
  return {
    type: GET_CLOUD_PROVIDERS_LIST_ERRORS,
    data: [],
    errors: error,
    loading: false
  }
}

export const getCloudProviders = () => (dispatch) => {
  // console.log('getCloudProviders called');
  dispatch(cloudProvidersRequest())
  getCloudProvidersHTTPService()
    .then((response) => {
      dispatch(getCloudProvidersResponse(response.data));
    })
    .catch((error) => {
      dispatch(getCloudProvidersError(error))
    })
}
