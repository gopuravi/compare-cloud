import {
  GET_OPERATING_SYSTEM_LIST_REQUEST,
  GET_OPERATING_SYSTEM_LIST_RESPONSE,
  GET_OPERATING_SYSTEM_LIST_ERRORS
} from './../constants/types';
import {getDataByAttributeNameHTTPService} from '@react-client/shared';

const operatingSystemRequest = () => {
  // console.log('operatingSystemRequest called');
  return {
    type: GET_OPERATING_SYSTEM_LIST_REQUEST,
    data: [],
    errors: {},
    loading: true
  }
}

const getOperatingSystemResponse = (data) => {
  // console.log('getOperatingSystemResponse called :::', data);
  return {
    type: GET_OPERATING_SYSTEM_LIST_RESPONSE,
    data,
    errors: {},
    loading: false
  }
}

const getOperatingSystemError = (error) => {
  // console.log('getOperatingSystemError called :::', error);
  return {
    type: GET_OPERATING_SYSTEM_LIST_ERRORS,
    data: [],
    errors: error,
    loading: false
  }
}

export const getOperatingSystem = (operatingSystemContext) => (dispatch) => {
  // console.log('getCloudProviders called');
  dispatch(operatingSystemRequest())
  getDataByAttributeNameHTTPService(operatingSystemContext)
    .then((response) => {
      dispatch(getOperatingSystemResponse(response.data));
    })
    .catch((error) => {
      dispatch(getOperatingSystemError(error))
    })
}
