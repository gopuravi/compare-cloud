import {
    GET_INSTANCE_TYPE_LIST_REQUEST,
    GET_INSTANCE_TYPE_LIST_RESPONSE,
    GET_INSTANCE_TYPE_LIST_ERRORS
} from './../constants/types';

const initialState = {
    data: [],
    errors: {} || '',
    loading: false
};

export const instanceTypeReducer = ( state = initialState, action ) => {
    // console.log('instanceTypeReducer called ::', state, action);
    switch( action.type ) {
        case GET_INSTANCE_TYPE_LIST_REQUEST: 
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        case GET_INSTANCE_TYPE_LIST_RESPONSE:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        case GET_INSTANCE_TYPE_LIST_ERRORS:
        return {
            ...state,
            data: action.data,
            errors: action.errors,
            loading: action.loading
        };
        default:
            return state;
    }
}