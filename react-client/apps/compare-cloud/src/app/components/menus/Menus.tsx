import React from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import { Link } from 'react-router-dom';
import i18next from 'i18next';
import './Menus.scss';

export interface MenusProps {}

const Menus = (props: MenusProps) => {
  return (
    <>
      <Navbar sticky="top" bg="light" expand="md" variant="light">
        <Navbar.Brand>
          <Link to="/">
            {i18next.t('logoHeader').toUpperCase()}
          </Link>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="compare-cloud-navbar-nav" />
        <Navbar.Collapse id="compare-cloud-navbar-nav">
          <Nav className="mr-auto">
            <LinkContainer to="/product">
              <NavItem className="nav-link">{i18next.t('common:product')}</NavItem>
            </LinkContainer>
            <LinkContainer to="/services">
              <NavItem className="nav-link">{i18next.t('common:services')}</NavItem>
            </LinkContainer>
            <LinkContainer to="/help">
              <NavItem className="nav-link">{i18next.t('common:help')}</NavItem>
            </LinkContainer>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default Menus;
