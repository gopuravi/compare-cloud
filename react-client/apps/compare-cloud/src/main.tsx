import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'

import store from './app/redux/store';
import './i18n';
import 'antd/dist/antd.css';

import App from './app/app';

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
