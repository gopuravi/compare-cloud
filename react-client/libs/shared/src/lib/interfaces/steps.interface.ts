import { ReactElement } from "react";

export interface StepsInterface {
    title: string;
    content: () => ReactElement
}