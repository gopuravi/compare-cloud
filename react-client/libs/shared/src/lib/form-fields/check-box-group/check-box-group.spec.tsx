import React from 'react';
import { render } from '@testing-library/react';

import {CheckBoxGroup} from './check-box-group';

describe('CheckBoxGroup', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CheckBoxGroup />);
    expect(baseElement).toBeTruthy();
  });
});
